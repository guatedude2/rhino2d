#include <QCommandLineParser>
#include <QDir>
#include <QTemporaryFile>
#include <QDebug>
#include "rcc.h"
#define VERSION 1.0

QT_BEGIN_NAMESPACE

void dumpRecursive(const QDir &dir, const QString &rootPath, QTextStream &out){
    QFileInfoList entries = dir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    foreach (const QFileInfo &entry, entries) {
        if (entry.isDir()) {
            dumpRecursive(entry.filePath(), rootPath, out);
        } else {
            out << QString("<file alias=\"%1\">").arg(entry.filePath().replace(QRegExp("^"+rootPath+"[/]?"), "")) << entry.filePath() << QLatin1String("</file>\n");
        }
    }
}

bool createPackage(const QString &rootPath, const QString outFilename){

    RCCResourceLibrary library;
//    if (parser.isSet(rootOption)) {
//        library.setResourceRoot(QDir::cleanPath(parser.value(rootOption)));
//        if (library.resourceRoot().isEmpty()
//                || library.resourceRoot().at(0) != QLatin1Char('/'))
//            errorMsg = QLatin1String("Root must start with a /");
//    }

    library.setFormat(RCCResourceLibrary::Binary);

    QTemporaryFile file;
    if (!file.open()) {
        fprintf(stderr, "Unable to create resource file %s\n", qPrintable(file.errorString()));
        return 1;
    }

//----OUTPUT ONLY----
//    QFile file;
//    QIODevice::OpenMode mode = QIODevice::WriteOnly;
//    file.open(stdout, mode);

    const QString absolutePath(QDir(rootPath).absolutePath());
    QTextStream stream(&file);

    stream << QLatin1String("<!DOCTYPE RCC><RCC version=\"1.0\">\n<qresource>\n");
    dumpRecursive(QDir(absolutePath), absolutePath, stream);
    stream << QLatin1String("</qresource>\n</RCC>\n");

    file.close();

    QFile errorDevice;
    errorDevice.open(stderr, QIODevice::WriteOnly|QIODevice::Text);

    if (library.verbose()) errorDevice.write("Qt resource compiler\n");

    library.setInputFiles(QStringList() << file.fileName());

    if (!library.readFiles(false, errorDevice))
        return 1;

    // open output
    QFile out;
    QIODevice::OpenMode mode = QIODevice::WriteOnly;
    if (library.format() == RCCResourceLibrary::C_Code) mode |= QIODevice::Text;

    out.setFileName(outFilename);
    if (!out.open(mode)) {
        const QString msg = QString::fromUtf8("Unable to open %1 for writing: %2\n").arg(outFilename).arg(out.errorString());
        errorDevice.write(msg.toUtf8());
        return 1;
    }

    return library.output(out, errorDevice) ? 0 : 1;
}

Q_CORE_EXPORT extern QBasicAtomicInt qt_qhash_seed; // from qhash.cpp

QT_END_NAMESPACE


int main(int argc, char *argv[]){

    if (!qEnvironmentVariableIsEmpty("QT_RCC_TEST") && !qt_qhash_seed.testAndSetRelaxed(-1, 0))
        qFatal("Cannot force QHash seed for testing as requested");

    QString errorMsg;
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationVersion(QString::fromLatin1(QT_VERSION_STR));

    QCommandLineParser parser;
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
    parser.setApplicationDescription(QStringLiteral("Rhino2d packager %1").arg(VERSION));
    parser.addHelpOption();
    parser.addVersionOption();

    //parameters
    parser.addPositionalArgument(QStringLiteral("path"), QStringLiteral("project root path"));
    //parser.addPositionalArgument(QStringLiteral("output_file"), QStringLiteral("output resource file"));
    QCommandLineOption outputOption(QStringList() << QStringLiteral("o") << QStringLiteral("output"));
    outputOption.setDescription(QStringLiteral("Write output to <path> rather than stdout."));
    outputOption.setValueName(QStringLiteral("path"));
    parser.addOption(outputOption);

    parser.process(app);

    //set variables from parametrs
    const QString path = parser.positionalArguments().first();
    const QString outputPath = (parser.isSet(outputOption) ? parser.value(outputOption) : QDir::currentPath());
    const QString outputFilename = QDir(path).dirName() +".rcc";

    //error checking
    if (path.isEmpty()) errorMsg = QStringLiteral("No project path specified.");
    if (outputPath.isEmpty()) errorMsg = QStringLiteral("Output path is required.");

    if (!errorMsg.isEmpty()) {
        fprintf(stderr, "%s: %s\n", argv[0], qPrintable(errorMsg));
        parser.showHelp(1);
        return 1;
    }

    // rcc -binary myresource.qrc -o myresource.rcc
    return QT_PREPEND_NAMESPACE(createPackage)(path, QDir(outputPath).absoluteFilePath(outputFilename));
}
