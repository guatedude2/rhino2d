#-------------------------------------------------
#
# Project created by QtCreator 2014-05-04T21:12:23
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = packager
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    main.cpp \
    rcc.cpp

HEADERS += \
    rcc.h
