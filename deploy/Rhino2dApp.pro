#TEMPLATE = app
QT += quick qml declarative xml #sensors svg xml
TARGET = Rhino2dApp
MOC_DIR = .moc
OBJECTS_DIR = .obj


android{
    QT += androidextras

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-sources
    OTHER_FILES += \
        android-sources/AndroidManifest.xml

    qmlplugins.files = \
        ../output/android/Rhino2d/*.so
    qmlplugins.path = /libs/armeabi-v7a
    INSTALLS += qmlplugins
}else{
    message("x86 not supported")
}

SOURCES += main.cpp


RESOURCES += \
    lib.qrc \
    package.qrc

#HEADERS +=

#PRE_TARGETDEPS = ../src/Rhino2d
#INCLUDEPATH += ../src/Rhino2d

#Rhino2d.target = ../src/Rhino2d
#Rhino2d.commands = cd ../src/Rhino2d && make
#Rhino2d.depends = ../src/Rhino2d/Makefile
#QMAKE_EXTRA_TARGETS += Rhino2d

#ANDROID_EXTRA_LIBS = ../src/Rhino2d/lib/libRhino2d.so
