#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQuick>
#include <QQmlEngine>
#include <QQmlExtensionPlugin>

#include <QDebug>
#include <QtGlobal>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    qDebug() << app.applicationDirPath();
    engine.addImportPath("qrc:/imports/");
    engine.load(QUrl("qrc:/main.qml"));

    return app.exec();
}
