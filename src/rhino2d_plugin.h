#ifndef RHINO2D_PLUGIN_H
#define RHINO2D_PLUGIN_H

#define Q_RHINO_VERSION "Rhino2d 1.0b"
#include <QQmlExtensionPlugin>

class Rhino2dPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
    void initializeEngine(QQmlEngine *engine, const char *uri);
    QQmlEngine *engine();

private:
    bool m_engineSet;
    QQmlEngine *m_engine;
};

#endif // RHINO2D_PLUGIN_H

