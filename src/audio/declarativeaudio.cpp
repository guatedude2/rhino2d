#include "declarativeaudio.h"
#include "../core/declarativegamewindow.h"
#include <QDebug>
#include <qmath.h>
#include <QQmlInfo>

//TODO: make errors more friendly
#define ERRCHECK(RESULT) if (RESULT != FMOD_OK){ qErrnoWarning(QString("FMOD error! (%1) %2").arg(RESULT).arg(FMOD_ErrorString(RESULT)).toLatin1()); return; }
#define ERRCHECK2(RESULT, RETURN) if (RESULT != FMOD_OK){ qErrnoWarning(QString("FMOD error! (%1) %2").arg(RESULT).arg(FMOD_ErrorString(RESULT)).toLatin1()); return RETURN; }

/** DeclarativeAudio class **/
DeclarativeAudio::DeclarativeAudio(QObject *parent) :
    QObject(parent),
    m_musicVolume(1.0),
    m_audioDeviceReady(false)
{
    unsigned int version;
    int numDrivers;
    FMOD_SPEAKERMODE speakerMode;
    FMOD_CAPS caps;
    char deviceName[256];
    FMOD_RESULT result;

    result = FMOD_System_Create(&m_system);
    ERRCHECK(result)

    result = FMOD_System_GetVersion(m_system, &version);
    ERRCHECK(result)

    if (version < FMOD_VERSION){
        qErrnoWarning("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
        return;
    }

    // Get number of sound cards
    result = FMOD_System_GetNumDrivers(m_system, &numDrivers);
    ERRCHECK(result);

    // No sound cards (disable sound)
    if (numDrivers == 0){
        result = FMOD_System_SetOutput(m_system, FMOD_OUTPUTTYPE_NOSOUND);
        ERRCHECK(result);
    }else{
        result = FMOD_System_GetDriverCaps(m_system, 0, &caps, 0, &speakerMode);
        ERRCHECK(result);

        result = FMOD_System_SetSpeakerMode(m_system, speakerMode);
        ERRCHECK(result);

        // Increase buffer size if user has Acceleration slider set to off
        if (caps & FMOD_CAPS_HARDWARE_EMULATED)
        {
            result = FMOD_System_SetDSPBufferSize(m_system, 1024, 10);
            ERRCHECK(result);
        }

        // Get name of driver
        result = FMOD_System_GetDriverInfo(m_system, 0, deviceName, 256, 0);
        ERRCHECK(result);

        // SigmaTel sound devices crackle for some reason if the format is PCM 16-bit. PCM floating point output seems to solve it.
        if (strstr(deviceName, "SigmaTel")){
            result = FMOD_System_SetSoftwareFormat(m_system, 48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR);
            ERRCHECK(result);
        }
        m_audioDeviceReady = true;
    }

    result = FMOD_System_Init(m_system, MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0);
    // If the selected speaker mode isn't supported by this sound card, switch it back to stereo
    if (result == FMOD_ERR_OUTPUT_CREATEBUFFER){
        result = FMOD_System_SetSpeakerMode(m_system, FMOD_SPEAKERMODE_STEREO);
        ERRCHECK(result);
        result = FMOD_System_Init(m_system, MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0);
    }
    ERRCHECK(result)

    //FMOD_System_CreateDSPByType(m_system, FMOD_DSP_TYPE_PITCHSHIFT, &pitch);
    //ERRCHECK(result)


}

DeclarativeAudio::~DeclarativeAudio(){
    if (m_system){
        FMOD_RESULT result;
        result = FMOD_System_Close(m_system);
        ERRCHECK(result)
        result = FMOD_System_Release(m_system);
        ERRCHECK(result)
    }
}

qreal DeclarativeAudio::musicVolume() const{ return m_musicVolume; }
void DeclarativeAudio::setMusicVolume(const qreal volume){
    if (volume != m_musicVolume){
        m_musicVolume = volume;
        if (!m_currentMusic.isEmpty()){
            AudioSoundLoader *audioSound = m_audioMusic.value(m_currentMusic, 0);
            if (audioSound) audioSound->setVolume(m_musicVolume);
        }
        emit musicVolumeChanged();
    }
}

bool DeclarativeAudio::loadSound(const QUrl &source, const QString &id){
    if (m_audioDeviceReady && !m_audioSound.contains(id)){
        AudioSoundLoader *audioSound = new AudioSoundLoader(source, false, m_system, this);
        m_audioSound.insert(id, audioSound);
        return true;
    }
    return false;
}

bool DeclarativeAudio::loadMusic(const QUrl &source, const QString &id){
    if (m_audioDeviceReady && !m_audioMusic.contains(id)){
        AudioSoundLoader *audioSound = new AudioSoundLoader(source, true, m_system, this);
        m_audioMusic.insert(id, audioSound);
        return true;
    }
    return false;
}

void DeclarativeAudio::playMusic(const QString &id, const qreal volume){
    if (!m_audioPaused){
        AudioSoundLoader *audioSound;

        if (!m_currentMusic.isEmpty()){
            audioSound = m_audioMusic.value(m_currentMusic, 0);
            if (audioSound) audioSound->stop();
        }

        if (m_audioMusic.contains(id)){
            AudioSoundLoader *audioSound = m_audioMusic.value(id);
            audioSound->setRepeat(true);
            audioSound->setVolume(volume*m_musicVolume);
            audioSound->play();
            m_currentMusic = id;
        }
    }
}

void DeclarativeAudio::pauseMusic(){
    if (!m_audioPaused && !m_currentMusic.isEmpty()){
        AudioSoundLoader *audioSound = m_audioMusic.value(m_currentMusic, 0);
        if (audioSound) audioSound->pause();
    }
}

void DeclarativeAudio::resumeMusic(){
    if (!m_audioPaused && !m_currentMusic.isEmpty()){
        AudioSoundLoader *audioSound = m_audioMusic.value(m_currentMusic, 0);
        if (audioSound) audioSound->play();
    }
}

void DeclarativeAudio::stopMusic(){
    if (!m_audioPaused && !m_currentMusic.isEmpty()){
        AudioSoundLoader *audioSound = m_audioMusic.value(m_currentMusic, 0);
        if (audioSound) audioSound->stop();
    }
}

void DeclarativeAudio::playSound(const QString &id, const qreal volume, const bool loop, const qreal rate, QJSValue callback){
    if (!m_audioPaused && m_audioSound.contains(id)){
        AudioSoundLoader *audioSound = m_audioSound.value(id);
        audioSound->setRepeat(loop);
        audioSound->setRate(rate);
        audioSound->setVolume(volume);
        audioSound->play();
    }
}

void DeclarativeAudio::pauseSound(const QString &id){
    if (!m_audioPaused && m_audioSound.contains(id)){
        AudioSoundLoader *audioSound = m_audioSound.value(id);
        audioSound->pause();
    }
}

void DeclarativeAudio::resumeSound(const QString &id){
    if (!m_audioPaused && m_audioSound.contains(id)){
        AudioSoundLoader *audioSound = m_audioSound.value(id);
        audioSound->play();
    }
}

void DeclarativeAudio::stopSound(const QString &id){
    if (!m_audioPaused && m_audioSound.contains(id)){
        AudioSoundLoader *audioSound = m_audioSound.value(id);
        audioSound->stop();
    }
}

void DeclarativeAudio::pauseAll(){
    foreach(AudioSoundLoader *audioSound, m_audioSound){
        audioSound->pause();
    }
    foreach(AudioSoundLoader *audioSound, m_audioMusic){
        audioSound->pause();
    }
    m_audioPaused = true;
}

void DeclarativeAudio::resumeAll(){
    foreach(AudioSoundLoader *audioSound, m_audioSound){
        audioSound->resume();
    }
    foreach(AudioSoundLoader *audioSound, m_audioMusic){
        audioSound->resume();
    }
    m_audioPaused = false;
}

void DeclarativeAudio::updateStates(){
    /*if (channel){
        FMOD_SOUND *currentsound = 0;

        result = FMOD_Channel_IsPlaying(channel, &playing);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
        {
            ERRCHECK(result);
        }

        result = FMOD_Channel_GetPaused(channel, &paused);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
        {
            ERRCHECK(result);
        }

        result = FMOD_Channel_GetPosition(channel, &ms, FMOD_TIMEUNIT_MS);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
        {
            ERRCHECK(result);
        }

        FMOD_Channel_GetCurrentSound(channel, &currentsound);
        if (currentsound)
        {
            result = FMOD_Sound_GetLength(currentsound, &lenms, FMOD_TIMEUNIT_MS);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            {
                ERRCHECK(result);
            }
        }
    }

    result = FMOD_Sound_GetLength(sound1, &lenms, FMOD_TIMEUNIT_MS);
    if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
    {
        ERRCHECK(result);
    }

    FMOD_System_GetChannelsPlaying(system, &channelsplaying);

    printf("\rTime %02d:%02d:%02d/%02d:%02d:%02d : %s : Channels Playing %2d", ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100, lenms / 1000 / 60, lenms / 1000 % 60, lenms / 10 % 100, paused ? "Paused " : playing ? "Playing" : "Stopped", channelsplaying);
*/

    FMOD_System_Update(m_system);
}

/** AudioSoundLoader class **/
AudioSoundLoader::AudioSoundLoader(QUrl source, bool isStream, FMOD_SYSTEM *system, QObject *parent) :
    QObject(parent),
    m_system(system),
    m_sound(0),
    m_channel(0),
    m_rate(1.0),
    m_isStream(isStream),
    m_volume(1.0),
    m_isReady(false),
    m_queuePlay(false)
{
    m_source = DeclarativeGameWindow::instance()->config()->baseUrl().resolved(source);
    if (m_source.scheme()=="file"){
        QFile file(m_source.toLocalFile());
        if (file.open(QFile::ReadOnly)){
            m_rawData = file.readAll();
            file.close();
            load();
        }else{
            qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load audio file " << m_source.toLocalFile();
        }
    }else{
        m_resource = new CoreRemoteResource(m_source, this);
        connect(m_resource, SIGNAL(complete()), this, SLOT(resourceLoaded()));
        connect(m_resource, SIGNAL(error(QNetworkReply::NetworkError,QString)), this, SLOT(resourceError(QNetworkReply::NetworkError,QString)));
    }
}

AudioSoundLoader::~AudioSoundLoader(){
    if (m_sound && !m_isStream){
        FMOD_RESULT result = FMOD_Sound_Release(m_sound);
        ERRCHECK(result)
    }
}

FMOD_SOUND *AudioSoundLoader::sound(){ return m_sound; }
FMOD_CHANNEL *AudioSoundLoader::channel(){ return m_channel; }
QPointF AudioSoundLoader::position(){ return m_position; }


void AudioSoundLoader::play(){
    if (!m_isReady){ m_queuePlay = true; return; }
    checkChannelStatus();
    if (!m_sound) return;
    FMOD_BOOL isPlaying = 0;
    //TODO: fix multichannel support
    FMOD_RESULT result;// = (m_channel ? FMOD_Channel_IsPlaying(m_channel, &isPlaying) : FMOD_OK);
    //ERRCHECK(result);
    if (!isPlaying){
        result = FMOD_System_PlaySound(m_system, FMOD_CHANNEL_FREE, m_sound, 0, &m_channel);
        ERRCHECK(result);

        setRepeat(m_repeat, true);
        setVolume(m_volume, true);
        setRate(m_rate, true);

        FMOD_System_Update(m_system);
    }else{
        resume();
    }
    m_queuePlay = false;
}

void AudioSoundLoader::pause(){
    if (!m_isReady){ m_queuePlay = false; return; }
    checkChannelStatus();
    if (!m_channel) return;
    FMOD_RESULT result = FMOD_Channel_SetPaused(m_channel, true);
    ERRCHECK(result)
}

void AudioSoundLoader::resume(){
    if (!m_isReady) return;
    checkChannelStatus();
    if (!m_channel) return;
    FMOD_RESULT result = FMOD_Channel_SetPaused(m_channel, false);
    ERRCHECK(result)
}

void AudioSoundLoader::stop(){
    if (!m_isReady){ m_queuePlay = false; return; }
    checkChannelStatus();
    if (!m_channel) return;
    FMOD_RESULT result = FMOD_Channel_Stop(m_channel);
    ERRCHECK(result)
    if (m_isStream){
        FMOD_Sound_Release(m_sound);
        m_sound = 0;
        load();
    }
    m_channel = 0;
}

bool AudioSoundLoader::repeat(){ return m_repeat; }
void AudioSoundLoader::setRepeat(bool repeat, bool force){
    checkChannelStatus();
    if (m_repeat != repeat || force){
        m_repeat = repeat;
        if (m_sound){
            FMOD_RESULT result;
            result = FMOD_Sound_SetMode(m_sound, (repeat ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF));
            ERRCHECK(result);
            result = FMOD_Sound_SetLoopCount(m_sound, -1);
            ERRCHECK(result);
        }
    }
}

qreal AudioSoundLoader::volume(){ return m_volume; }
void AudioSoundLoader::setVolume(qreal volume, bool force){
    checkChannelStatus();
    if (m_volume != volume || force){
        m_volume = volume;
        if (m_channel){
            FMOD_RESULT result = FMOD_Channel_SetVolume(m_channel, fmin(fmax(m_volume, 0.0f), 1.0f));
            ERRCHECK(result)
        }
    }
}

qreal AudioSoundLoader::rate(){ return m_rate; }
void AudioSoundLoader::setRate(qreal rate, bool force){
    checkChannelStatus();
    if (m_rate != rate || force){
        FMOD_RESULT result;
        m_rate = rate;
        if (m_channel){
            float freq = 0;
            result = FMOD_Channel_GetFrequency(m_channel, &freq);
            ERRCHECK(result);

            result = FMOD_Channel_SetFrequency(m_channel, fmin(fmax(rate, 0.1f), 1.0f) * freq);
            ERRCHECK(result);
        }
    }
}

void AudioSoundLoader::resourceLoaded(){
    m_rawData = m_resource->data();
    load();
}

void AudioSoundLoader::resourceError(QNetworkReply::NetworkError status, QString error){
    qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load audio file " << m_source.toLocalFile() << "#"<< status << ": " << error;
}


void AudioSoundLoader::load(){
    FMOD_RESULT result;
    FMOD_CREATESOUNDEXINFO exInfo;
    memset(&exInfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    exInfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    exInfo.length = m_rawData.size();
    if (!m_isStream){
        result = FMOD_System_CreateSound(m_system, m_rawData.constData(), FMOD_DEFAULT | FMOD_HARDWARE | FMOD_NONBLOCKING | FMOD_OPENMEMORY, &exInfo, &m_sound);
    }else{
        result = FMOD_System_CreateStream(m_system, m_rawData.constData(), FMOD_DEFAULT | FMOD_HARDWARE | FMOD_NONBLOCKING | FMOD_OPENMEMORY, &exInfo, &m_sound);
    }
    if (result != FMOD_OK){
        qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load audio file " << m_source.toLocalFile() << ": "<< FMOD_ErrorString(result);
        return;
    }
    m_isReady = true;
    if (m_queuePlay) play();
}

void AudioSoundLoader::checkChannelStatus(){
    FMOD_BOOL isPlaying = 0;
    if (m_channel) FMOD_Channel_IsPlaying(m_channel, &isPlaying);
    if (!isPlaying) m_channel = 0;
}
