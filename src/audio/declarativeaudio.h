#ifndef DECLARATIVEAUDIO_H
#define DECLARATIVEAUDIO_H

#define MAX_AUDIO_CHANNELS 100

#include <QtPlugin>
#include "../core/declarativecore.h"

#include <fmod/macx/inc/fmod.h>
#include <fmod/macx/inc/fmod_errors.h>

#include <QFile>
#include <QObject>
#include <QPointF>
#include <QJSValue>
#include <QUrl>

class AudioSoundLoader;
class DeclarativeAudio : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal musicVolume READ musicVolume WRITE setMusicVolume NOTIFY musicVolumeChanged)
public:
    explicit DeclarativeAudio(QObject *parent = 0);
    ~DeclarativeAudio();

    qreal musicVolume() const;
    void setMusicVolume(const qreal volume);

    bool loadSound(const QUrl &source, const QString &id);
    bool loadMusic(const QUrl &source, const QString &id);

    Q_INVOKABLE void playMusic(const QString &id, const qreal volume=1.0);
    Q_INVOKABLE void pauseMusic();
    Q_INVOKABLE void resumeMusic();
    Q_INVOKABLE void stopMusic();

    Q_INVOKABLE void playSound(const QString &id, const qreal volume=1.0, const bool loop=false, const qreal rate=1.0, QJSValue callback=QJSValue());
    Q_INVOKABLE void pauseSound(const QString &id);
    Q_INVOKABLE void resumeSound(const QString &id);
    Q_INVOKABLE void stopSound(const QString &id);

    void pauseAll();
    void resumeAll();
    //Q_INVOKABLE void stopAll();

    void updateStates();

signals:
    void musicVolumeChanged();

private:

    FMOD_SYSTEM *m_system;
    QHash<QString, AudioSoundLoader *> m_audioSound, m_audioMusic;
    QString m_currentMusic;
    qreal m_musicVolume;
    bool m_audioDeviceReady;
    bool m_audioPaused;

};


class AudioSoundLoader : public QObject{
    Q_OBJECT
public:
    AudioSoundLoader(QUrl source, bool isStream, FMOD_SYSTEM *system, QObject *parent=0);
    ~AudioSoundLoader();

    FMOD_SOUND *sound();
    FMOD_CHANNEL *channel();
    QPointF position();

    void play();
    void pause();
    void resume();
    void stop();

    bool repeat();
    void setRepeat(bool repeat, bool force=false);

    qreal volume();
    void setVolume(qreal volume, bool force=false);

    qreal rate();
    void setRate(qreal rate, bool force=false);

protected slots:
    void resourceLoaded();
    void resourceError(QNetworkReply::NetworkError status, QString error);

protected:
    void load();
    void checkChannelStatus();


private:
    CoreRemoteResource *m_resource;
    FMOD_SYSTEM *m_system;
    FMOD_SOUND *m_sound;
    FMOD_CHANNEL *m_channel;
    QPointF m_position;
    qreal m_rate;
    QUrl m_source;
    bool m_isStream;
    qreal m_volume;
    bool m_repeat;
    bool m_isResource;
    QByteArray m_rawData;
    bool m_isReady;
    bool m_queuePlay;
};

#endif // DECLARATIVEAUDIO_H
