TEMPLATE = lib
TARGET = Rhino2d
QT += qml quick declarative xml
CONFIG += qt plugin valgrind #TODO: remove valgrind

android{
    ARCH = android
    DESTDIR = $$PWD/../output/android/Rhino2d
}else{
    ARCH = x86
    DESTDIR = $$PWD/../output/x86/Rhino2d
}
#TARGET = $$DESTDIR
QMAKE_POST_LINK += $$QMAKE_COPY $$replace($$list($$quote($$PWD/qmldir) $$DESTDIR), /, $$QMAKE_DIR_SEP);

MOC_DIR = .moc/$$ARCH
OBJECTS_DIR = .obj/$$ARCH
RCC_DIR = .rcc/$$ARCH


#TARGET = $$PWD/lib/Rhino2d

uri = Rhino2d


INCLUDEPATH += libs

include(libs/Box2D/box2d.pri)

# Input
SOURCES += \
    rhino2d_plugin.cpp \
    audio/declarativeaudio.cpp \
    core/declarativegamewindow.cpp \
    core/declarativeloader.cpp \
    core/declarativesystem.cpp \
    core/declarativestage.cpp \
    core/declarativetween.cpp \
    core/declarativetimer.cpp \
    core/declarativepool.cpp \
    drawable/declarativescene.cpp \
    drawable/declarativesprite.cpp \
    drawable/declarativetilingsprite.cpp \
    drawable/declarativemovieclip.cpp \
    drawable/declarativeemitter.cpp \
    drawable/declarativecontainer.cpp \
    drawable/declarativetext.cpp \
    physics/declarativeshape.cpp \
    physics/declarativebody.cpp \
    physics/declarativestaticbody.cpp \
    physics/declarativeworld.cpp \
    drawable/declarativebitmaptext.cpp \
    core/declarativestorage.cpp \
    core/declarativeconfig.cpp \
    core/declarativecore.cpp \
    core/declarativekeyboard.cpp

HEADERS += \
    rhino2d_plugin.h \
    audio/declarativeaudio.h \
    core/qdeclarativeevents_p_p.h \
    core/declarativegamewindow.h \
    core/declarativeloader.h \
    core/declarativesystem.h \
    core/declarativestage.h \
    core/declarativetween.h \
    core/declarativetimer.h \
    core/declarativepool.h \
    drawable/declarativescene.h \
    drawable/declarativesprite.h \
    drawable/declarativetilingsprite.h \
    drawable/declarativemovieclip.h \
    drawable/declarativeemitter.h \
    drawable/declarativecontainer.h \
    drawable/declarativetext.h \
    physics/declarativeshape.h \
    physics/declarativebody.h \
    physics/declarativestaticbody.h \
    physics/declarativeworld.h \
    drawable/declarativebitmaptext.h \
    core/declarativestorage.h \
    core/declarativeconfig.h \
    core/declarativecore.h \
    core/declarativekeyboard.h

OTHER_FILES = qmldir \
    rhino2d.qmltypes \
    qml/DefaultLoader.qml \
    qml/TweenLibrary.js

RESOURCES += \
    qml/qml.qrc

android {
    message("WARNING using android armeabi-v7a");
    LIBS += -L$$PWD/libs/fmod/android/lib/armeabi-v7a -lfmodex
    QMAKE_POST_LINK += $$QMAKE_COPY $$replace($$list($$quote($$PWD/libs/fmod/android/lib/armeabi-v7a/*) $$DESTDIR), /, $$QMAKE_DIR_SEP);
} else: macx {
    LIBS += -L$$PWD/libs/fmod/macx/lib -lfmodex
    QMAKE_POST_LINK += $$QMAKE_COPY $$replace($$list($$quote($$PWD/libs/fmod/macx/lib/*) $$DESTDIR), /, $$QMAKE_DIR_SEP);
} else: win {
    error("No fmod support for windows")
} else: ios {
    error("No fmod support for windows")
}
