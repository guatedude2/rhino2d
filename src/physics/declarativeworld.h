#ifndef DECLARATIVEWORLD_H
#define DECLARATIVEWORLD_H

#define PIXELS_PER_METER 32.0f
#define MAX_CIRCULAR_RAYCAST 512.0f //256 - ok 128
#define EXPLOSION_MAX_PARTICLES 128

#include "declarativestaticbody.h"
#include "declarativebody.h"
#include "declarativeshape.h"

#include <QJSEngine>
#include <QQuickPaintedItem>
#include <QPolygonF>
#include <QLineF>
#include <Box2D.h>

class DeclarativeDebugDraw;
class ContactListener : public b2ContactListener{
public:

    struct ContactEvent{
        enum Type { BeginContact, EndContact };
        Type type;
        DeclarativeStaticBody *bodyA;
        DeclarativeStaticBody *bodyB;
    };
    void BeginContact(b2Contact *contact);
    void EndContact(b2Contact *contact);

    void removeEvent(int index) { mEvents.removeAt(index); }
    void clearEvents() { mEvents.clear(); }
    const QList<ContactEvent> &events() { return mEvents; }

private:
    QList<ContactEvent> mEvents;
};

class DeclarativeWorld : public QObject, public QQmlParserStatus{
    Q_OBJECT
    Q_ENUMS(CollisionAccuracy BodyDetection)
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QPointF gravity READ gravity WRITE setGravity NOTIFY gravityChanged)
    Q_PROPERTY(CollisionAccuracy collisionAccuracy READ collisionAccuracy WRITE setCollisionAccuracy NOTIFY collisionAccuracyChanged)
    Q_PROPERTY(bool debugDraw READ debugDraw WRITE setDebugDraw NOTIFY debugDrawChanged)
    Q_PROPERTY(QQmlListProperty<QObject> bodies READ bodiesListProperty)
public:
    explicit DeclarativeWorld(QObject *parent = 0);
    ~DeclarativeWorld();

    enum CollisionAccuracy{ DefaultAccuracy = 1, LowAccuracy = 0, MediumAccuracy=1, HighAccuracy=2, VeryHighAccuracy=3 };
    enum BodyDetection{ StaticAndDynamicBodies, DynamicBodiesOnly, StaticBodiesOnly };

    QPointF gravity();
    void setGravity(QPointF gravity);
    CollisionAccuracy collisionAccuracy();
    void setCollisionAccuracy(CollisionAccuracy accuracy);
    bool debugDraw();
    void setDebugDraw(bool debugDraw);

    QList<QObject *> bodies();
    QQmlListProperty<QObject> bodiesListProperty();

    void addBody(DeclarativeStaticBody *body);
    void removeBody(DeclarativeStaticBody *body);

    void sceneUpdate();

    bool isComponentComplete();

    b2Body *createBody(DeclarativeStaticBody *body);

    void applyGlobalImpulse(b2Body *excludeBody, qreal force, DeclarativeBody::ExplosionType type, QPointF position, qreal radius);
    void applyGlobalForce(b2Body *excludeBody, qreal force, DeclarativeBody::ExplosionType type, QPointF position, qreal radius);

    Q_INVOKABLE QVariantMap rayCastTest(const qreal startX, const qreal startY, const qreal endX, const qreal endY);
    Q_INVOKABLE QVariantList aabbTest(qreal x1, qreal y1, qreal x2, qreal y2);
    Q_INVOKABLE DeclarativeStaticBody *pointTest(qreal x, qreal y);
    Q_INVOKABLE bool containsPoint(DeclarativeStaticBody *body, qreal x, qreal y);
    Q_INVOKABLE DeclarativeStaticBody *closestBody(qreal x, qreal y, qreal radius, BodyDetection bodyTypes=StaticAndDynamicBodies);

    static DeclarativeWorld *instance();

    void componentComplete();

    static QPointF toQPointF(const b2Vec2 &vec, const float scaleRatio){
        return QPointF(vec.x * scaleRatio, -vec.y * scaleRatio);
    }

    static DeclarativeStaticBody *toBody(b2Fixture * fixture){
        return static_cast<DeclarativeStaticBody*>(fixture->GetUserData());
    }


signals:
    void gravityChanged();
    void collisionAccuracyChanged();
    void debugDrawChanged();

protected:
    void classBegin(){}
    void applyBlastImpulse(b2Body* body, b2Vec2 blastCenter, b2Vec2 applyPoint, float blastPower);
    void applyBlastForce(b2Body* body, b2Vec2 blastCenter, b2Vec2 applyPoint, float blastPower);

private:
    struct BodyContact{ DeclarativeStaticBody *bodyA, *bodyB; };
    bool m_ready;
    CollisionAccuracy m_collisionAccuracy;
    bool m_debugDraw;
    QList<QObject *> m_bodies;
    b2World *m_world;
    QPointF m_gravity;
    DeclarativeDebugDraw *m_debugDrawItem;
    ContactListener *m_contactListener;

    static DeclarativeWorld *m_instance;
};

class QueryCallback : public b2QueryCallback{
public:
    std::vector<b2Body*> m_bodies;

    bool ReportFixture(b2Fixture* fixture) {
        m_bodies.push_back( fixture->GetBody() );
        return true;//keep going to find all fixtures in the query area
    }
};

class RayCastCallback : public b2RayCastCallback
{
public:
    b2Body* m_body;
    b2Vec2 m_point;
    b2Vec2 m_normal;
    float32 m_fraction;


    RayCastCallback() { m_body = NULL; }

    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction){
        m_body = fixture->GetBody();
        m_point = point;
        m_normal = normal;
        m_fraction = fraction;
        return fraction;
    }
};


class DeclarativeDebugDraw : public QQuickPaintedItem{
    Q_OBJECT
public:
    explicit DeclarativeDebugDraw(b2World * world, QQuickItem *parent = 0);
    void paint(QPainter *painter);

protected:
    static QColor toQColor(const b2Color &color, const int alpha=255){
        return QColor(color.r * 255, color.g * 255, color.b * 255, alpha);
    }

    static QPolygonF toQPolygonF(const b2Vec2 *vertices, int32 vertexCount, const float scaleRatio){
        QPolygonF polygon;
        polygon.reserve(vertexCount);

        for (int i = 0; i < vertexCount; ++i)
            polygon.append(DeclarativeWorld::toQPointF(vertices[i], scaleRatio));

        return polygon;
    }

    class DebugDraw : public b2Draw{
    public:
        DebugDraw(QPainter *painter, b2World *world);

        void draw();

        void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
        void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
        void DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color);
        void DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color);
        void DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color);
        void DrawTransform(const b2Transform &){}

    private:
        QPainter *m_p;
        b2World *m_world;
    };
private:
    b2World *m_world;
};

#endif // DECLARATIVEWORLD_H
