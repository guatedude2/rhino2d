#include "declarativeworld.h"
#include "declarativestaticbody.h"


/** DeclarativeStaticBody class **/
DeclarativeStaticBody::DeclarativeStaticBody(QQuickItem *parent) :
    QQuickItem(parent),
    m_body(0),
    m_shape(0),
    m_world(0),
    m_anchorPoint(QPointF(0.5, 0.5)),
    m_collidesAgainst(0), m_collisionGroup(0),
    m_bounciness(0),
    m_friction(0.2),
    m_sensor(false),
    m_mass(0)
{
    m_bodyDef.type = b2_staticBody;
}

DeclarativeStaticBody::~DeclarativeStaticBody(){
    if (m_body) m_world->removeBody(this);
    m_body = 0;
}

DeclarativeWorld *DeclarativeStaticBody::world(){ return m_world; }
void DeclarativeStaticBody::setWorld(DeclarativeWorld *world){
    if (world != m_world){
        if (m_world && m_body){
            m_world->removeBody(this);
            m_shape->destroyFixture();
        }
        m_body = 0;
        m_world = world;
        if (m_world && isComponentComplete()){
            m_body = m_world->createBody(this);
            m_body->SetUserData(this);
            m_shape->createFixture(this);
        }
        emit worldChanged();
    }
}

qreal DeclarativeStaticBody::x() const{ return QQuickItem::x() + m_anchorPoint.x()*width(); }
void DeclarativeStaticBody::setX(const qreal x){
    if (x != this->x()){
        QQuickItem::setX(x - m_anchorPoint.x()*width());
        if (isComponentComplete()) updateBody();
        emit xChanged();
    }
}

qreal DeclarativeStaticBody::y() const{ return QQuickItem::y() + m_anchorPoint.y()*height(); }
void DeclarativeStaticBody::setY(const qreal y){
    if (y != this->y()){
        QQuickItem::setY(y - m_anchorPoint.y()*height());
        if (isComponentComplete()) updateBody();
        emit yChanged();
    }
}

QPointF DeclarativeStaticBody::anchorPoint() const{ return m_anchorPoint; }
void DeclarativeStaticBody::setAnchorPoint(const QPointF anchor){
    if (anchor != m_anchorPoint){
        if (isComponentComplete()) QQuickItem::setPosition(QPointF(this->x()-anchor.x()*width(), this->y()-anchor.y()*height()));
        m_anchorPoint = anchor;
        QQuickItem::update();
        emit anchorPointChanged();
    }
}

void DeclarativeStaticBody::setEnabled(bool enabled){
    if (enabled != isEnabled()){
        QQuickItem::setEnabled(enabled);
        if (isComponentComplete()) updateBody();
        emit enabledChanged();
    }
}

void DeclarativeStaticBody::setRotation(const qreal rotation){
    if (rotation != this->rotation()){
        QQuickItem::setRotation(rotation);
        if (isComponentComplete()) updateBody();
        emit rotationChanged();
    }
}

bool DeclarativeStaticBody::fixedRotation() const{ return m_bodyDef.fixedRotation; }
void DeclarativeStaticBody::setFixedRotation(const bool fixedRotation){
    if (fixedRotation != m_bodyDef.fixedRotation){
        m_bodyDef.fixedRotation = fixedRotation;
        if (isComponentComplete() && body()) body()->SetFixedRotation(fixedRotation);
    }
}

QObject *DeclarativeStaticBody::shape(){ return m_shape; }
void DeclarativeStaticBody::setShape(QObject *shape){
    if (shape != m_shape && shape->inherits("DeclarativeShape")){
        DeclarativeShape *oldShape = m_shape;
        m_shape = static_cast<DeclarativeShape *>(shape);
        if (isComponentComplete()){
            if (m_shape){
//                m_shape->setCollideAgainst(m_collidesAgainst);
//                m_shape->setCollisionGroup(m_collisionGroup);
                if (oldShape){
                    m_shape->setDensity(oldShape->density());
                    m_shape->setRestitution(oldShape->restitution());
                    m_shape->setFriction(oldShape->friction());
                    m_shape->setSensor(oldShape->isSensor());
                }
                m_shape->createFixture(this);
            }
        }
        emit shapeChanged();
    }
}

int DeclarativeStaticBody::collideAgainst(){ return m_collidesAgainst; }
void DeclarativeStaticBody::setCollideAgainst(int groupId){
    if (m_collidesAgainst != groupId){
        m_collidesAgainst = groupId;
//        if (isComponentComplete()){
//            if (m_shape){
//                m_shape->setCollideAgainst(groupId);
//            }
//        }
        emit collideAgainstChanged();
    }
}

int DeclarativeStaticBody::collisionGroup(){ return m_collisionGroup; }
void DeclarativeStaticBody::setCollisionGroup(int groupId){
    if (m_collisionGroup != groupId){
        m_collisionGroup = groupId;
        if (isComponentComplete()){
            if (m_shape){
                m_shape->setCollisionGroup(groupId);
            }
        }
        emit collisionGroupChanged();
    }
}
qreal DeclarativeStaticBody::bounciness() const{ return m_bounciness; }
void DeclarativeStaticBody::setBounciness(const qreal bounciness){
    if (m_bounciness != bounciness){
        m_bounciness = bounciness;
        if (isComponentComplete()){
            if (m_shape){
                m_shape->setRestitution(bounciness);
            }
        }
        emit bouncinessChanged();
    }
}

qreal DeclarativeStaticBody::friction() const{ return m_friction; }
void DeclarativeStaticBody::setFriction(const qreal friction){
    if (m_friction != friction){
        m_friction = friction;
        if (isComponentComplete()){
            if (m_shape){
                m_shape->setFriction(friction);
            }
        }
        emit bouncinessChanged();
    }
}

bool DeclarativeStaticBody::sensor() const{ return m_sensor; }
void DeclarativeStaticBody::setSensor(const bool sensor){
    if (m_sensor != sensor){
        m_sensor = sensor;
        if (isComponentComplete()){
            if (m_shape){
                m_shape->setSensor(sensor);
            }
        }
        emit bouncinessChanged();
    }
}

qreal DeclarativeStaticBody::mass() const{ return m_mass; }
void DeclarativeStaticBody::setMass(const qreal mass){
    if (mass != m_mass){
        m_mass = mass;
        def()->type = (m_mass>0 ? b2_dynamicBody : b2_kinematicBody);
        if (isComponentComplete()){
            if (m_body && shape()){
                static_cast<DeclarativeShape *>(shape())->setDensity(m_mass);
                m_body->SetType(def()->type);
            }
            updateBody();
        }
    }
}


b2BodyDef *DeclarativeStaticBody::def(){ return &m_bodyDef; }

b2Body *DeclarativeStaticBody::body(){ return m_body; }

void DeclarativeStaticBody::worldUpdate(){
    QPointF pos = DeclarativeWorld::toQPointF(m_body->GetPosition(), PIXELS_PER_METER);
    qreal angle = -(m_body->GetAngle() * 180.0) / b2_pi;
    if (pos.x() != this->x()){
        QQuickItem::setX(pos.x() - m_anchorPoint.x()*width());
        emit xChanged();
    }
    if (pos.y() != this->y()){
        QQuickItem::setY(pos.y() - m_anchorPoint.y()*height());
        emit yChanged();
    }
    if (angle != this->rotation()){
        QQuickItem::setRotation(angle);
        emit rotationChanged();
    }
    emit update();
}

void DeclarativeStaticBody::contact(const bool contact, DeclarativeStaticBody *body){
    if (contact){
        emit collided(body);
    }else{
        emit afterCollide(body);
    }
}

void DeclarativeStaticBody::initialize(){
    if (m_world && m_world->isComponentComplete()){
        if (!m_body){
            m_body = m_world->createBody(this);
            m_body->SetUserData(this);
        }
        //TODO need to test if this is needed
        if (m_shape) {
            // //m_shape->setCollideAgainst(m_collidesAgainst);
            m_shape->setDensity(m_mass);
            m_shape->setCollisionGroup(m_collisionGroup);
            m_shape->setRestitution(m_bounciness);
            m_shape->setFriction(m_friction);
            m_shape->setSensor(m_sensor);
            m_shape->createFixture(this);
            QPointF pos(QQuickItem::position());
            updateShapeBounds();
            QQuickItem::setPosition(QPointF(pos.x()-m_anchorPoint.x()*width(), pos.y()-m_anchorPoint.y()*height()));
        }
        updateBody();
    }
}

void DeclarativeStaticBody::updateShapeBounds(){
    if (m_shape && isComponentComplete()){
        setImplicitWidth(m_shape->boundingRect().width());
        setImplicitHeight(m_shape->boundingRect().height());
        QQuickItem::setPosition(QPointF(this->x()-m_anchorPoint.x()*width(), this->y()-m_anchorPoint.y()*height()));
    }
}

void DeclarativeStaticBody::componentComplete(){
    QQuickItem::componentComplete();    
    if (DeclarativeWorld::instance()) setWorld(DeclarativeWorld::instance());
    m_bodyDef.position.Set(this->x() / PIXELS_PER_METER, -this->y() / PIXELS_PER_METER);
    m_bodyDef.angle =-(this->rotation() * (2 * b2_pi)) / 360.0;
    m_bodyDef.allowSleep = true;
    if (m_world) m_world->addBody(this);
}

void DeclarativeStaticBody::updateBody(){
    if (m_body){
        m_bodyDef.active = isEnabled();
        m_bodyDef.angle = -(this->rotation() * (2 * b2_pi)) / 360.0;
        m_bodyDef.position.Set(this->x() / PIXELS_PER_METER, -this->y() / PIXELS_PER_METER);
        m_body->SetTransform(m_bodyDef.position, m_bodyDef.angle);
        m_body->SetActive(m_bodyDef.active);
    }
}
