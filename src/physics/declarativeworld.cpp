#include "declarativeworld.h"
#include "../core/declarativegamewindow.h"

/** ContactListener class**/
void ContactListener::BeginContact(b2Contact *contact){
    ContactEvent event;
    event.type = ContactEvent::BeginContact;
    event.bodyA = DeclarativeWorld::toBody(contact->GetFixtureA());
    event.bodyB = DeclarativeWorld::toBody(contact->GetFixtureB());
    mEvents.append(event);
}

void ContactListener::EndContact(b2Contact *contact){
    ContactEvent event;
    event.type = ContactEvent::EndContact;
    event.bodyA = DeclarativeWorld::toBody(contact->GetFixtureA());
    event.bodyB = DeclarativeWorld::toBody(contact->GetFixtureB());
    mEvents.append(event);
}

/** DeclarativeWorld class **/
DeclarativeWorld *DeclarativeWorld::m_instance = 0;
DeclarativeWorld::DeclarativeWorld(QObject *parent) :
    QObject(parent),
    m_ready(false),
    m_collisionAccuracy(DefaultAccuracy),
    m_debugDraw(false),
    m_world(0),
    m_gravity(0.0, 10.0),
    m_debugDrawItem(0)
{
    m_contactListener = new ContactListener;
    m_instance = this;
}

DeclarativeWorld::~DeclarativeWorld(){
    foreach(QObject *body, m_bodies){
        m_world->DestroyBody(static_cast<DeclarativeStaticBody *>(body)->body());
    }
    m_bodies.clear();
    delete m_contactListener;
}

QPointF DeclarativeWorld::gravity(){ return m_gravity; }
void DeclarativeWorld::setGravity(QPointF gravity){
    if (m_gravity != gravity){
        m_gravity = gravity;
        if (m_world) m_world->SetGravity(b2Vec2(gravity.x(), -gravity.y()));

        emit gravityChanged();
    }
}

DeclarativeWorld::CollisionAccuracy DeclarativeWorld::collisionAccuracy(){ return m_collisionAccuracy; }
void DeclarativeWorld::setCollisionAccuracy(DeclarativeWorld::CollisionAccuracy accuracy){
    if (accuracy != m_collisionAccuracy){
        m_collisionAccuracy = accuracy;
        emit collisionAccuracyChanged();
    }
}

bool DeclarativeWorld::debugDraw(){ return m_debugDraw; }
void DeclarativeWorld::setDebugDraw(bool debugDraw){
    if (debugDraw != m_debugDraw){
        m_debugDraw = debugDraw;
        if (isComponentComplete()){
            if (m_debugDraw && DeclarativeGameWindow::instance() && DeclarativeGameWindow::instance()->scene()){
                m_debugDrawItem = new DeclarativeDebugDraw(m_world, DeclarativeGameWindow::instance()->scene());
            }else if (!m_debugDraw && m_debugDrawItem){
                m_debugDrawItem->deleteLater();
                m_debugDrawItem = 0;
            }
        }
        emit debugDrawChanged();
    }
}

QList<QObject *> DeclarativeWorld::bodies(){ return m_bodies; }

QQmlListProperty<QObject> DeclarativeWorld::bodiesListProperty(){
    return QQmlListProperty<QObject>(static_cast<QObject *>(this), m_bodies); //TODO: change to non-prototype version
}


void DeclarativeWorld::addBody(DeclarativeStaticBody *body){
    m_bodies.append(body);
    if (isComponentComplete()) body->initialize();
}

void DeclarativeWorld::removeBody(DeclarativeStaticBody *body){
    if (m_bodies.contains(body)){
        m_bodies.removeOne(body);
        m_world->DestroyBody(body->body());
    }
}

b2Body *DeclarativeWorld::createBody(DeclarativeStaticBody *body){
    if (isComponentComplete()){
        return m_world->CreateBody(body->def());
    }
    return 0;
}

void DeclarativeWorld::applyGlobalImpulse(b2Body *excludeBody, qreal force, DeclarativeBody::ExplosionType type, QPointF position, qreal radius){
    if (type==DeclarativeBody::RaycastExplosion){
        float32 numRays = MAX_CIRCULAR_RAYCAST;
        b2Vec2 center = b2Vec2(position.x(), position.y());
        for (float32 i = 0; i < numRays; i++) {
            float32 angle = (i / numRays) * 360.0f * DEG_TO_RAD;
            b2Vec2 rayDir( sinf(angle), cosf(angle) );
            b2Vec2 rayEnd = center + radius * rayDir;

            //check what this ray hits
            RayCastCallback callback;
            m_world->RayCast(&callback, center, rayEnd);
            if ( callback.m_body ) {
                if (callback.m_body!=excludeBody) applyBlastImpulse(callback.m_body, center, callback.m_point, (force / numRays));
            }
        }
    }else{
        QueryCallback queryCallback; //see "World querying topic"
        b2AABB aabb;
        b2Vec2 center(position.x(), position.y());
        aabb.lowerBound = center - b2Vec2( radius, radius );
        aabb.upperBound = center + b2Vec2( radius, radius );
        m_world->QueryAABB( &queryCallback, aabb );

        //check which of these bodies have their center of mass within the blast radius
        for (int i = (int)queryCallback.m_bodies.size()-1; i >=0; --i) {
            b2Body* body = queryCallback.m_bodies[i];
            b2Vec2 bodyCom = body->GetWorldCenter();

            //ignore bodies outside the blast range
            if ( (bodyCom - center).Length() >= radius ) continue;

            if (body!=excludeBody) applyBlastImpulse(body, center, bodyCom, force );
        }
    }
}

void DeclarativeWorld::applyGlobalForce(b2Body *excludeBody, qreal force, DeclarativeBody::ExplosionType type, QPointF position, qreal radius){
    if (type==DeclarativeBody::RaycastExplosion){
        float32 numRays = MAX_CIRCULAR_RAYCAST;
        b2Vec2 center = b2Vec2(position.x(), position.y());
        for (float32 i = 0; i < numRays; i++) {
            float32 angle = (i / numRays) * 360.0f * DEG_TO_RAD;
            b2Vec2 rayDir( sinf(angle), cosf(angle) );
            b2Vec2 rayEnd = center + radius * rayDir;

            //check what this ray hits
            RayCastCallback callback;
            m_world->RayCast(&callback, center, rayEnd);
            if ( callback.m_body ) {
                if (callback.m_body!=excludeBody) applyBlastForce(callback.m_body, center, callback.m_point, (force / numRays));
            }
        }
    }else{
        QueryCallback queryCallback; //see "World querying topic"
        b2AABB aabb;
        b2Vec2 center(position.x(), position.y());
        aabb.lowerBound = center - b2Vec2( radius, radius );
        aabb.upperBound = center + b2Vec2( radius, radius );
        m_world->QueryAABB( &queryCallback, aabb );

        //check which of these bodies have their center of mass within the blast radius
        for (int i = (int)queryCallback.m_bodies.size()-1; i >=0; --i) {
            b2Body* body = queryCallback.m_bodies[i];
            b2Vec2 bodyCom = body->GetWorldCenter();

            //ignore bodies outside the blast range
            if ( (bodyCom - center).Length() >= radius ) continue;

            if (body!=excludeBody) applyBlastForce(body, center, bodyCom, force );
        }
    }

}

QVariantMap DeclarativeWorld::rayCastTest(const qreal startX, const qreal startY, const qreal endX, const qreal endY){
    QVariantMap result, normal, point;
    RayCastCallback callback;
    m_world->RayCast(&callback, b2Vec2(startX / PIXELS_PER_METER, startY / PIXELS_PER_METER), b2Vec2(endX / PIXELS_PER_METER, -endY / PIXELS_PER_METER));
    if (callback.m_body){
        QPoint p(callback.m_point.x * PIXELS_PER_METER, -callback.m_point.y * PIXELS_PER_METER);
        normal.insert("x", callback.m_normal.x * PIXELS_PER_METER);
        normal.insert("y", -callback.m_normal.y * PIXELS_PER_METER);
        result.insert("normal", normal);
        point.insert("x", p.x());
        point.insert("y", p.y());
        result.insert("point", point);
        result.insert("distance", QLineF(startX, startY, p.x(), p.y()).length());
        result.insert("body",  QVariant::fromValue(static_cast<QObject *>((DeclarativeStaticBody*)callback.m_body->GetUserData())));
    }
    return result;
}

QVariantList DeclarativeWorld::aabbTest(qreal x1, qreal y1, qreal x2, qreal y2){
    QVariantList result;
    QueryCallback queryCallback;
    b2AABB aabb;
    aabb.lowerBound.Set(fmin(x1 / PIXELS_PER_METER, x2 / PIXELS_PER_METER), fmin(-y1 / PIXELS_PER_METER, -y2 / PIXELS_PER_METER));
    aabb.upperBound.Set(fmax(x1 / PIXELS_PER_METER, x2 / PIXELS_PER_METER), fmax(-y1 / PIXELS_PER_METER, -y2 / PIXELS_PER_METER));
    m_world->QueryAABB( &queryCallback, aabb );
    for (int i = (int)queryCallback.m_bodies.size()-1; i >=0; --i) {
        b2Body* body = queryCallback.m_bodies[i];
        result.append(QVariant::fromValue(static_cast<QObject *>((DeclarativeStaticBody *)body->GetUserData())));
    }
    return result;
}

DeclarativeStaticBody *DeclarativeWorld::pointTest(qreal x, qreal y){
    QueryCallback queryCallback;
    b2AABB aabb;
    b2Body *closestBody = 0;
    float32 closestDistance=std::numeric_limits<float32>::max();
    b2Vec2 pos(x / PIXELS_PER_METER, -y / PIXELS_PER_METER);
    aabb.lowerBound.Set(pos.x-0.001, pos.y-0.001);
    aabb.upperBound.Set(pos.x+0.001, pos.y+0.001);
    m_world->QueryAABB( &queryCallback, aabb );
    for (int i = (int)queryCallback.m_bodies.size()-1; i >=0; --i) {
        b2Body* body = queryCallback.m_bodies[i];
        b2Vec2 bodyCom = body->GetWorldCenter();
        float32 distance = (bodyCom - pos).Length();

        if (distance<0.8 && distance < closestDistance){
            closestBody = body;
            closestDistance = distance;
        }
    }
    return (closestBody ? (DeclarativeStaticBody *)closestBody->GetUserData() : 0);
}

bool DeclarativeWorld::containsPoint(DeclarativeStaticBody *body, qreal x, qreal y){
    QueryCallback queryCallback;
    b2AABB aabb;
    b2Body *closestBody = 0;
    float32 closestDistance=std::numeric_limits<float32>::max();
    b2Vec2 pos(x / PIXELS_PER_METER, -y / PIXELS_PER_METER);
    aabb.lowerBound.Set(pos.x-0.001, pos.y-0.001);
    aabb.upperBound.Set(pos.x+0.001, pos.y+0.001);
    m_world->QueryAABB( &queryCallback, aabb );
    for (int i = (int)queryCallback.m_bodies.size()-1; i >=0; --i) {
        b2Body* body = queryCallback.m_bodies[i];
        b2Vec2 bodyCom = body->GetWorldCenter();
        float32 distance = (bodyCom - pos).Length();

        if (distance<0.8 && distance < closestDistance){
            closestBody = body;
            closestDistance = distance;
        }
    }
    return (closestBody &&  (DeclarativeStaticBody *)closestBody->GetUserData()==body);
}

DeclarativeStaticBody *DeclarativeWorld::closestBody(qreal x, qreal y, qreal radius, BodyDetection bodyTypes){
    DeclarativeStaticBody *closestBody = 0;
    float32 closestDistance = std::numeric_limits<float32>::max();
    float32 radiusDistance = radius / PIXELS_PER_METER;
    b2Vec2 p1(x / PIXELS_PER_METER, -y / PIXELS_PER_METER);
    float32 numRays = MAX_CIRCULAR_RAYCAST;
    bool checkStatic = (bodyTypes==StaticAndDynamicBodies || bodyTypes==StaticBodiesOnly);
    bool checkDynamic = (bodyTypes==StaticAndDynamicBodies || bodyTypes==DynamicBodiesOnly);
    for (float32 i = 0; i < numRays; i++) {
        float32 angle = (i / numRays) * 360.0f * DEG_TO_RAD;
        b2Vec2 rayDir( sinf(angle), cosf(angle) );
        b2Vec2 rayEnd = p1 + radiusDistance * rayDir;

        //check what this ray hits
        RayCastCallback callback;
        m_world->RayCast(&callback, p1, rayEnd);
        if ( callback.m_body ) {
            bool isStatic = (callback.m_body->GetType()==b2_staticBody);
            if ((isStatic && checkStatic) || (!isStatic && checkDynamic)){
                float32 distance = (p1 - callback.m_point).Length();
                if (distance< closestDistance){
                    closestBody =  (DeclarativeStaticBody *)callback.m_body->GetUserData();
                    closestDistance = distance;
                }
            }
        }
    }

    return closestBody;
}

DeclarativeWorld *DeclarativeWorld::instance(){ return DeclarativeWorld::m_instance; }

void DeclarativeWorld::sceneUpdate(){
    if (!m_ready) return;
    if (DeclarativeGameWindow::instance() && DeclarativeGameWindow::instance()->system()){
        float32 timeStep = 1.0f / 60.0f;
        int32 posItirations = 6, velItirations = 6;

        switch (m_collisionAccuracy){
            case VeryHighAccuracy: velItirations = 20; posItirations = 20; break;
            case HighAccuracy: velItirations = 10; posItirations = 10; break;
            case MediumAccuracy: velItirations = 8; posItirations = 8; break;
            default: break;
        }

        m_world->Step(timeStep, velItirations, posItirations);
        foreach(QObject *body, bodies()){
            static_cast<DeclarativeStaticBody *>(body)->worldUpdate();
        }

        foreach (const ContactListener::ContactEvent &event, m_contactListener->events()) {
            switch (event.type) {
            case ContactListener::ContactEvent::BeginContact:                
                if (event.bodyA->collideAgainst()==event.bodyB->collisionGroup() && bodies().contains(event.bodyA)) event.bodyA->contact(true, bodies().contains(event.bodyB) ? event.bodyB : 0) ;
                if (event.bodyB->collideAgainst()==event.bodyA->collisionGroup() && bodies().contains(event.bodyB)) event.bodyB->contact(true, bodies().contains(event.bodyA) ? event.bodyA : 0);
                break;
            case ContactListener::ContactEvent::EndContact:
                if (event.bodyA->collideAgainst()==event.bodyB->collisionGroup() && bodies().contains(event.bodyA)) event.bodyA->contact(false, bodies().contains(event.bodyA) ? event.bodyB : 0);
                if (event.bodyB->collideAgainst()==event.bodyA->collisionGroup() && bodies().contains(event.bodyB)) event.bodyB->contact(false, bodies().contains(event.bodyB) ? event.bodyA : 0);
                break;
            }
        }
        m_contactListener->clearEvents();

        // Emit signals for the current state of the contacts
//        b2Contact *contact = m_world->GetContactList();
//        while (contact) {
//            Box2DFixture *bodyA = DeclarativeWorld::toBody(contact->GetFixtureA());
//            Box2DFixture *bodyB = DeclarativeWorld::toBody(contact->GetFixtureB());

//            bodyA->emitContactChanged(bodyB);
//            bodyB->emitContactChanged(fixtureA);

//            contact = contact->GetNext();
//        }
    }
    if (m_debugDrawItem) m_debugDrawItem->update();
}

bool DeclarativeWorld::isComponentComplete(){ return m_ready; }
void DeclarativeWorld::componentComplete(){
    m_ready = true;
    m_world = new b2World(b2Vec2(m_gravity.x(), -m_gravity.y()));
    m_world->SetContactListener(m_contactListener);
    if (m_debugDraw && DeclarativeGameWindow::instance() && DeclarativeGameWindow::instance()->scene()){
        m_debugDrawItem = new DeclarativeDebugDraw(m_world, DeclarativeGameWindow::instance()->scene());
    }

    foreach(QObject *body, bodies()){
        static_cast<DeclarativeStaticBody *>(body)->initialize();
    }
}

void DeclarativeWorld::applyBlastImpulse(b2Body *body, b2Vec2 blastCenter, b2Vec2 applyPoint, float blastPower){
      b2Vec2 blastDir = applyPoint - blastCenter;
      float distance = blastDir.Normalize();
      //ignore bodies exactly at the blast point - blast direction is undefined
      if ( distance <= 0.01 )
          return;
      float invDistance = 1 / distance;
      float impulseMag = blastPower * invDistance * invDistance;
      body->ApplyLinearImpulse( impulseMag * blastDir, applyPoint, true);
}

void DeclarativeWorld::applyBlastForce(b2Body *body, b2Vec2 blastCenter, b2Vec2 applyPoint, float blastPower){
    b2Vec2 blastDir = applyPoint - blastCenter;
    float distance = blastDir.Normalize();
    //ignore bodies exactly at the blast point - blast direction is undefined
    if ( distance <= 0.01 )
        return;
    float invDistance = 1 / distance;
    float impulseMag = blastPower * invDistance * invDistance;
    body->ApplyForce( impulseMag * blastDir, applyPoint, true);
}




/** DeclarativeDebugDraw class **/
DeclarativeDebugDraw::DeclarativeDebugDraw(b2World *world, QQuickItem *parent) :
    QQuickPaintedItem(parent), m_world(world)
{
    setZ(100000);
    setRenderTarget(FramebufferObject);
    QVariant itemParent = QQmlProperty::read(this, "parent");
    QQmlProperty::write(this, "anchors.fill", itemParent);
}

void DeclarativeDebugDraw::paint(QPainter *painter){
    if (!m_world) return;

    // Darken the view to make the debug draw stand out more
    painter->fillRect(0, 0, width(), height(), QColor(0, 0, 0, 128));
    DebugDraw debugDraw(painter, m_world);
    debugDraw.draw();
}

DeclarativeDebugDraw::DebugDraw::DebugDraw(QPainter *painter, b2World *world) :
    m_p(painter), m_world(world)
{
    SetFlags(e_shapeBit | e_jointBit | e_aabbBit | e_pairBit | e_centerOfMassBit);
}

void DeclarativeDebugDraw::DebugDraw::draw(){
    m_world->SetDebugDraw(this);
    m_world->DrawDebugData();
    m_world->SetDebugDraw(0);
}

void DeclarativeDebugDraw::DebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color){
    m_p->setPen(toQColor(color, 200));
    m_p->setBrush(Qt::NoBrush);
    m_p->drawPolygon(toQPolygonF(vertices, vertexCount, PIXELS_PER_METER));
}

void DeclarativeDebugDraw::DebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color){
    m_p->setPen(Qt::NoPen);
    m_p->setBrush(toQColor(color, 200));
    m_p->drawPolygon(toQPolygonF(vertices, vertexCount, PIXELS_PER_METER));
}

void DeclarativeDebugDraw::DebugDraw::DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color){
    const float scaleRatio = PIXELS_PER_METER;

    m_p->setPen(toQColor(color, 200));
    m_p->setBrush(Qt::NoBrush);
    m_p->drawEllipse(DeclarativeWorld::toQPointF(center, scaleRatio), radius * scaleRatio, radius * scaleRatio);
}

void DeclarativeDebugDraw::DebugDraw::DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color){
    Q_UNUSED(axis)

    const float scaleRatio = PIXELS_PER_METER;

    m_p->setPen(Qt::NoPen);
    m_p->setBrush(toQColor(color, 200));
    m_p->drawEllipse(DeclarativeWorld::toQPointF(center, scaleRatio), radius * scaleRatio, radius * scaleRatio);
}

void DeclarativeDebugDraw::DebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color){
    m_p->setPen(toQColor(color, 200));
    m_p->drawLine(DeclarativeWorld::toQPointF(p1, PIXELS_PER_METER), DeclarativeWorld::toQPointF(p2, PIXELS_PER_METER));
}

