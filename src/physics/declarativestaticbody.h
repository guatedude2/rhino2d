#ifndef DECLARATIVESTATICBODY_H
#define DECLARATIVESTATICBODY_H

#include <QQuickItem>
#include <Box2D.h>

//TODO:
// box2d bullet mode automatically after a max vel
class DeclarativeWorld;
class DeclarativeShape;
class DeclarativeStaticBody : public QQuickItem{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(QPointF anchorPoint READ anchorPoint WRITE setAnchorPoint NOTIFY anchorPointChanged)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(qreal width READ width NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height NOTIFY heightChanged)
    Q_PROPERTY(qreal implicitWidth READ implicitWidth NOTIFY implicitWidthChanged)
    Q_PROPERTY(qreal implicitHeight READ implicitHeight WRITE setImplicitHeight NOTIFY implicitHeightChanged)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(bool fixedRotation READ fixedRotation WRITE setFixedRotation NOTIFY fixedRotationChanged)
    Q_PROPERTY(QObject *shape READ shape WRITE setShape NOTIFY shapeChanged)
    Q_PROPERTY(int collideAgainst READ collideAgainst WRITE setCollideAgainst NOTIFY collideAgainstChanged)
    Q_PROPERTY(int collisionGroup READ collisionGroup WRITE setCollisionGroup NOTIFY collisionGroupChanged)
    Q_PROPERTY(qreal bounciness READ bounciness WRITE setBounciness NOTIFY bouncinessChanged)
    Q_PROPERTY(qreal friction READ friction WRITE setFriction NOTIFY frictionChanged)
    Q_PROPERTY(bool sensor READ sensor WRITE setSensor NOTIFY sensorChanged)
public:
    explicit DeclarativeStaticBody(QQuickItem *parent = 0);
    ~DeclarativeStaticBody();

    DeclarativeWorld *world();
    void setWorld(DeclarativeWorld *world);
    qreal x() const;
    void setX(const qreal x);
    qreal y() const;
    void setY(const qreal y);
    QPointF anchorPoint() const;
    void setAnchorPoint(const QPointF anchor);
    void setEnabled(bool enabled);
    void setRotation(const qreal rotation);
    bool fixedRotation() const;
    void setFixedRotation(const bool fixedRotation);
    QObject *shape();
    void setShape(QObject * shape);
    int collideAgainst();
    void setCollideAgainst(int groupId);
    int collisionGroup();
    void setCollisionGroup(int groupId);
    qreal bounciness() const;
    void setBounciness(const qreal bounciness);
    qreal friction() const;
    void setFriction(const qreal friction);
    bool sensor() const;
    void setSensor(const bool sensor);
    qreal mass() const;
    void setMass(const qreal mass);

    b2BodyDef *def();
    b2Body *body();
    virtual void worldUpdate();
    void contact(const bool contact, DeclarativeStaticBody *body);
    void initialize();
    void updateShapeBounds();

signals:
    void worldChanged();
    void xChanged();
    void yChanged();
    void anchorPointChanged();
    void enabledChanged();
    void widthChanged();
    void heightChanged();
    void implicitWidthChanged();
    void implicitHeightChanged();
    void rotationChanged();
    void fixedRotationChanged();
    void shapeChanged();
    void collided(DeclarativeStaticBody * body);
    void afterCollide(DeclarativeStaticBody * body);
    void collideAgainstChanged();
    void collisionGroupChanged();
    void bouncinessChanged();
    void frictionChanged();
    void sensorChanged();
    void update();

protected:
    void classBegin(){}
    void componentComplete();
    void updateBody();

private:
    bool m_ready;
    bool m_contact;
    b2Body *m_body;
    b2BodyDef m_bodyDef;
    DeclarativeShape *m_shape;
    DeclarativeWorld *m_world;
    QPointF m_anchorPoint;
    int m_collidesAgainst;
    int m_collisionGroup;
    qreal m_bounciness;
    qreal m_friction;
    bool m_sensor;
    qreal m_mass;

};

#endif // DECLARATIVESTATICBODY_H
