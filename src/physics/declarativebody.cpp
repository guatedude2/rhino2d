#include "declarativeworld.h"
#include "declarativebody.h"


/** DeclarativeBody class **/
DeclarativeBody::DeclarativeBody(QQuickItem *parent) :
    DeclarativeStaticBody(parent)
{
    def()->type = b2_kinematicBody;
}

qreal DeclarativeBody::mass() const{ return DeclarativeStaticBody::mass(); }

void DeclarativeBody::setMass(const qreal mass){
    if (mass != DeclarativeStaticBody::mass()){
        DeclarativeStaticBody::setMass(mass);
        emit massChanged();
    }
}


QPointF DeclarativeBody::linearVelocity() const{ return m_linearVelocity; }
void DeclarativeBody::setLinearVelocity(const QPointF velocity){
    if (m_linearVelocity != velocity){
        m_linearVelocity = velocity;
        def()->linearVelocity = b2Vec2(m_linearVelocity.x() / PIXELS_PER_METER , -m_linearVelocity.y() / PIXELS_PER_METER);
        if (isComponentComplete()){
            if (body()){
                body()->SetLinearVelocity(def()->linearVelocity);
            }
        }
        emit linearVelocityChanged();
    }
}

qreal DeclarativeBody::angularVelocity() const{ return m_angularVelocity; }
void DeclarativeBody::setAngularVelocity(const qreal angularVelocity){
    if (m_angularVelocity != angularVelocity){
        m_angularVelocity = angularVelocity;
        def()->angularVelocity = m_angularVelocity / PIXELS_PER_METER;
        if (isComponentComplete()){
            if (body()){
                body()->SetAngularVelocity(def()->angularVelocity);
            }
        }
        emit angularVelocityChanged();
    }
}

qreal DeclarativeBody::linearDamping() const{ return m_linearDamping; }
void DeclarativeBody::setLinearDamping(const qreal linearDamping){
    if (m_linearDamping != linearDamping){
        m_linearDamping = linearDamping;
        def()->linearDamping = m_linearDamping / PIXELS_PER_METER;
        if (isComponentComplete()){
            if (body()){
                body()->SetLinearDamping(def()->linearDamping);
            }
        }
        emit linearDampingChanged();
    }
}

qreal DeclarativeBody::angularDamping() const{ return m_angularDamping; }
void DeclarativeBody::setAngularDamping(const qreal angularDamping){
    if (m_angularDamping != angularDamping){
        m_angularDamping = angularDamping;
        def()->angularDamping = m_angularDamping / PIXELS_PER_METER;
        if (isComponentComplete()){
            if (body()){
                body()->SetAngularDamping(def()->angularDamping);
            }
        }
        emit angularDampingChanged();
    }
}

void DeclarativeBody::addForce(const QPointF &force, const QPointF &position, DeclarativeBody::ForceMode mode){
    if (!world()) return;
    if (mode==Impulse){
        body()->ApplyLinearImpulse(b2Vec2(force.x(), force.y()), b2Vec2(position.x(), position.y()), true);
    }else{
        body()->ApplyForce(b2Vec2(force.x(), force.y()), b2Vec2(position.x(), position.y()), true);
    }
}

void DeclarativeBody::addTorque(qreal torque, DeclarativeBody::ForceMode mode){
    if (!world()) return;
    if (mode==Impulse){
        body()->ApplyAngularImpulse(torque, true);
    }else{
        body()->ApplyTorque(torque, true);
    }
}

void DeclarativeBody::addExplosionForce(qreal force, qreal radius, QPointF position, ExplosionType type, DeclarativeBody::ForceMode mode){
    if (!world()) return;
    if (mode==Impulse){
        world()->applyGlobalImpulse(body(), force, type, QPoint(body()->GetPosition().x + position.x(), body()->GetPosition().y + position.y()), radius);
    }else{
        world()->applyGlobalForce(body(), force, type, QPoint(body()->GetPosition().x + position.x(), body()->GetPosition().y + position.y()), radius);
    }
}

void DeclarativeBody::worldUpdate(){
    DeclarativeStaticBody::worldUpdate();
    QPointF velocity = DeclarativeWorld::toQPointF(body()->GetLinearVelocity(), PIXELS_PER_METER);
    if (velocity != m_linearVelocity){
        m_linearVelocity = velocity;
        emit linearVelocityChanged();
    }
    qreal angularVelocity = body()->GetAngularDamping() / PIXELS_PER_METER;
    if (angularVelocity != m_angularVelocity){
        m_angularVelocity = angularVelocity;
        emit angularVelocityChanged();
    }
}

void DeclarativeBody::componentComplete(){
    def()->linearVelocity = b2Vec2(m_linearVelocity.x() / PIXELS_PER_METER, -m_linearVelocity.y() / PIXELS_PER_METER);
    DeclarativeStaticBody::componentComplete();
}
