#ifndef DECLARATIVEBOX2D_H
#define DECLARATIVEBOX2D_H

#include <QQuickItem>
#include <Box2D.h>

#include "declarativebase.h"

//TODO:
//    Maybe move collisions to b2ContactListener
class DeclarativeShape;
class DeclarativeDebugDraw;
class DeclarativeStaticBody;

class ContactListener : public b2ContactListener{
public:

    struct ContactEvent{
        enum Type { BeginContact, EndContact };
        Type type;
        DeclarativeStaticBody *bodyA;
        DeclarativeStaticBody *bodyB;
    };
    void BeginContact(b2Contact *contact);
    void EndContact(b2Contact *contact);

    void removeEvent(int index) { mEvents.removeAt(index); }
    void clearEvents() { mEvents.clear(); }
    const QList<ContactEvent> &events() { return mEvents; }

private:
    QList<ContactEvent> mEvents;
};

class DeclarativeWorld : public QObject, public QQmlParserStatus{
    Q_OBJECT
    Q_ENUMS(CollisionAccuracy)
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QPointF gravity READ gravity WRITE setGravity NOTIFY gravityChanged)
    Q_PROPERTY(CollisionAccuracy collisionAccuracy READ collisionAccuracy WRITE setCollisionAccuracy NOTIFY collisionAccuracyChanged)
    Q_PROPERTY(bool debugDraw READ debugDraw WRITE setDebugDraw NOTIFY debugDrawChanged)
    Q_PROPERTY(QQmlListProperty<QObject> bodies READ bodiesListProperty)
public:
    explicit DeclarativeWorld(QObject *parent = 0);
    ~DeclarativeWorld();

    enum CollisionAccuracy{ DefaultAccuracy = 1, LowAccuracy = 0, MediumAccuracy=1, HighAccuracy=2, VeryHighAccuracy=3 };

    QPointF gravity();
    void setGravity(QPointF gravity);
    CollisionAccuracy collisionAccuracy();
    void setCollisionAccuracy(CollisionAccuracy accuracy);
    bool debugDraw();
    void setDebugDraw(bool debugDraw);

    QList<QObject *> bodies();
    QQmlListProperty<QObject> bodiesListProperty();

    void addBody(DeclarativeStaticBody *body);
    void removeBody(DeclarativeStaticBody *body);

    void sceneUpdate();

    bool isComponentComplete();

    b2Body *createBody(DeclarativeStaticBody *body);

signals:
    void gravityChanged();
    void collisionAccuracyChanged();
    void debugDrawChanged();

protected:
    void classBegin(){}
    void componentComplete();

private:
    struct BodyContact{ DeclarativeStaticBody *bodyA, *bodyB; };
    bool m_ready;
    CollisionAccuracy m_collisionAccuracy;
    bool m_debugDraw;
    b2World *m_world;
    QList<QObject *> m_bodies;
    QPointF m_gravity;
    DeclarativeDebugDraw *m_debugDrawItem;
    ContactListener *m_contactListener;
};


//TODO:
// box2d bullet mode automatically after a max vel
// box2d active when enabled/disabled
class DeclarativeStaticBody : public QQuickItem{
    Q_OBJECT
    Q_PROPERTY(DeclarativeWorld *world READ world WRITE setWorld NOTIFY worldChanged)
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(QPointF anchorPoint READ anchorPoint WRITE setAnchorPoint NOTIFY anchorPointChanged)
    Q_PROPERTY(qreal width READ width NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height NOTIFY heightChanged)
    Q_PROPERTY(qreal implicitWidth READ implicitWidth NOTIFY implicitWidthChanged)
    Q_PROPERTY(qreal implicitHeight READ implicitHeight WRITE setImplicitHeight NOTIFY implicitHeightChanged)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(bool fixedRotation READ fixedRotation WRITE setFixedRotation NOTIFY fixedRotationChanged)
    Q_PROPERTY(QObject *shape READ shape WRITE setShape NOTIFY shapeChanged)
    Q_PROPERTY(int collideAgainst READ collideAgainst WRITE setCollideAgainst NOTIFY collideAgainstChanged)
    Q_PROPERTY(int collisionGroup READ collisionGroup WRITE setCollisionGroup NOTIFY collisionGroupChanged)
    Q_PROPERTY(qreal bounciness READ bounciness WRITE setBounciness NOTIFY bouncinessChanged)
    Q_PROPERTY(qreal friction READ friction WRITE setFriction NOTIFY frictionChanged)
    Q_PROPERTY(bool sensor READ sensor WRITE setSensor NOTIFY sensorChanged)
    Q_ENUMS(Group)
    Q_FLAGS(Groups)
public:
    explicit DeclarativeStaticBody(QQuickItem *parent = 0);
    ~DeclarativeStaticBody();

    DeclarativeWorld *world();
    void setWorld(DeclarativeWorld *world);
    qreal x() const;
    void setX(const qreal x);
    qreal y() const;
    void setY(const qreal y);
    QPointF anchorPoint() const;
    void setAnchorPoint(const QPointF anchor);
    void setRotation(const qreal rotation);
    bool fixedRotation() const;
    void setFixedRotation(const bool fixedRotation);
    QObject *shape();
    void setShape(QObject * shape);
    int collideAgainst();
    void setCollideAgainst(int groupId);
    int collisionGroup();
    void setCollisionGroup(int groupId);
    qreal bounciness() const;
    void setBounciness(const qreal bounciness);
    qreal friction() const;
    void setFriction(const qreal friction);
    bool sensor() const;
    void setSensor(const bool sensor);

    b2BodyDef *def();
    b2Body *body();
    virtual void worldUpdate();
    void contact(const bool contact, DeclarativeStaticBody *body);
    void initialize();
    void updateShapeBounds();

signals:
    void worldChanged();
    void xChanged();
    void yChanged();
    void anchorPointChanged();
    void widthChanged();
    void heightChanged();
    void implicitWidthChanged();
    void implicitHeightChanged();
    void rotationChanged();
    void fixedRotationChanged();
    void shapeChanged();
    void collided(DeclarativeStaticBody * body);
    void afterCollide(DeclarativeStaticBody * body);
    void collideAgainstChanged();
    void collisionGroupChanged();
    void bouncinessChanged();
    void frictionChanged();
    void sensorChanged();

protected:
    void classBegin(){}
    void componentComplete();
    void updateBody();

private:
    bool m_ready;
    bool m_contact;
    b2Body *m_body;
    b2BodyDef m_bodyDef;
    DeclarativeShape *m_shape;
    DeclarativeWorld *m_world;
    QPointF m_anchorPoint;
    int m_collidesAgainst;
    int m_collisionGroup;
    qreal m_bounciness;
    qreal m_friction;
    bool m_sensor;

};
//Q_DECLARE_OPERATORS_FOR_FLAGS(DeclarativeStaticBody::Groups)


class DeclarativeBody : public DeclarativeStaticBody{
    Q_OBJECT
    Q_PROPERTY(qreal mass READ mass WRITE setMass NOTIFY massChanged)
    Q_PROPERTY(QPointF linearVelocity READ linearVelocity WRITE setLinearVelocity NOTIFY linearVelocityChanged)
    //Q_PROPERTY(qreal angularVelocity READ angularVelocity WRITE setAngularVelocity NOTIFY angularVelocityChanged)
    //Q_PROPERTY(qreal linearDamping READ linearDamping WRITE setLinearDamping NOTIFY linearDampingChanged)
    //Q_PROPERTY(qreal angularDamping READ angularDamping WRITE setAngularDamping NOTIFY angularDampingChanged)

public:
    explicit DeclarativeBody(QQuickItem *parent = 0);

    qreal mass() const;
    void setMass(const qreal mass);
    QPointF linearVelocity() const;
    void setLinearVelocity(const QPointF velocity);


    //Q_INVOKABLE void applyForce(const QPointF &force,const QPointF &point=QPointF(0,0));
    //Q_INVOKABLE void applyTorque(qreal torque);
    //Q_INVOKABLE void applyLinearImpulse(const QPointF &impulse, const QPointF &point=QPointF(0,0));

    void worldUpdate();

signals:
    void massChanged();
    void linearVelocityChanged();

protected:
    void componentComplete();

private:
    qreal m_mass;
    QPointF m_linearVelocity;

};


class DeclarativeShape : public QObject{
    Q_OBJECT
public:
    explicit DeclarativeShape(QObject * parent = 0);
    ~DeclarativeShape();

    qreal friction();
    void setFriction(qreal friction);
    qreal restitution();
    void setRestitution(qreal restitution);
    qreal density();
    void setDensity(qreal density);
    bool isSensor();
    void setSensor(bool sensor);
    int collisionGroup();
    void setCollisionGroup(int groupId);
//    uint16 collideAgainst();
//    void setCollideAgainst(uint16 groupId);
//    uint16 collisionGroup();
//    void setCollisionGroup(uint16 groupId);

    QRectF boundingRect();

    virtual b2Shape *createShape(){ return 0; }
    void createFixture(DeclarativeStaticBody *body);
    void destroyFixture();
    void applyShape();

protected:
    void setBoundingRect(QRectF boundRect);

private:
    DeclarativeStaticBody *m_body;
    b2FixtureDef m_fixtureDef;
    b2Fixture *m_fixture;
    QRectF m_boundRect;
};


class DeclarativeRectangleShape : public DeclarativeShape{
    Q_OBJECT
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
public:
    DeclarativeRectangleShape(QObject * parent = 0);
    qreal width();
    void setWidth(qreal width);
    qreal height();
    void setHeight(qreal height);

    b2Shape *createShape();

signals:
    void widthChanged();
    void heightChanged();

protected:
    QSizeF m_shape;
};


class DeclarativeDebugDraw : public QQuickPaintedItem{
    Q_OBJECT
public:
    explicit DeclarativeDebugDraw(b2World * world, QQuickItem *parent = 0);
    void paint(QPainter *painter);

protected:

    class DebugDraw : public b2Draw{
    public:
        DebugDraw(QPainter *painter, b2World *world);

        void draw();

        void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
        void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
        void DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color);
        void DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color);
        void DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color);
        void DrawTransform(const b2Transform &){}

    private:
        QPainter *m_p;
        b2World *m_world;
    };
private:
    b2World *m_world;
};


#endif // DECLARATIVEBOX2D_H
