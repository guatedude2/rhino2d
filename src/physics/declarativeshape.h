#ifndef DECLARATIVESHAPE_H
#define DECLARATIVESHAPE_H

#include <QQuickItem>
#include <Box2D.h>

class DeclarativeStaticBody;
class DeclarativeShape : public QObject{
    Q_OBJECT
public:
    explicit DeclarativeShape(QObject * parent = 0);
    ~DeclarativeShape();

    qreal friction();
    void setFriction(qreal friction);
    qreal restitution();
    void setRestitution(qreal restitution);
    qreal density();
    void setDensity(qreal density);
    bool isSensor();
    void setSensor(bool sensor);
    int collisionGroup();
    void setCollisionGroup(int groupId);
//    uint16 collideAgainst();
//    void setCollideAgainst(uint16 groupId);
//    uint16 collisionGroup();
//    void setCollisionGroup(uint16 groupId);

    QRectF boundingRect();

    virtual QList<b2Shape *> createShapes(){ return QList<b2Shape *>(); }
    void createFixture(DeclarativeStaticBody *body);
    void destroyFixture();
    void applyShape();
    DeclarativeStaticBody *body();

protected:
    void setBoundingRect(QRectF boundRect);

private:
    DeclarativeStaticBody *m_body;
    b2FixtureDef m_fixtureDef;
    QList<b2Fixture *> m_fixtures;
    QRectF m_boundRect;
};


class DeclarativeRectangleShape : public DeclarativeShape{
    Q_OBJECT
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
public:
    DeclarativeRectangleShape(QObject * parent = 0);
    qreal width();
    void setWidth(qreal width);
    qreal height();
    void setHeight(qreal height);

    QList<b2Shape *> createShapes();

signals:
    void widthChanged();
    void heightChanged();

protected:
    QSizeF m_shape;
};


class DeclarativeCircleShape : public DeclarativeShape{
    Q_OBJECT
    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
public:
    DeclarativeCircleShape(QObject * parent = 0);
    qreal radius();
    void setRadius(qreal radius);

    QList<b2Shape *> createShapes();

signals:
    void radiusChanged();

protected:
    qreal m_radius;
};


class DeclarativeEllipseShape : public DeclarativeShape{
    Q_OBJECT
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
public:
    DeclarativeEllipseShape(QObject * parent = 0);
    qreal width();
    void setWidth(qreal width);
    qreal height();
    void setHeight(qreal height);

    QList<b2Shape *> createShapes();

signals:
    void widthChanged();
    void heightChanged();

protected:
    QSizeF m_shape;
};


class DeclarativePolygonPoint : public QObject{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX)
    Q_PROPERTY(qreal y READ y WRITE setY)
public:
    qreal x() const{ return m_x; }
    void setX(const qreal x){ m_x = x; }
    qreal y() const{ return m_y; }
    void setY(const qreal y){ m_y = y; }

    QPointF toPointF(){ return QPointF(m_x, m_y); }
private:
    qreal m_x, m_y;
};


class DeclarativePolygonShape : public DeclarativeShape{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<DeclarativePolygonPoint> points READ points)
public:
    DeclarativePolygonShape(QObject * parent = 0);
    QQmlListProperty<DeclarativePolygonPoint> points();

    QList<b2Shape *> createShapes();

signals:
    void pointsChanged();

protected:

    QList<DeclarativePolygonPoint *> m_points;
};

#endif // DECLARATIVESHAPE_H
