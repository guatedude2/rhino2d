#ifndef DECLARATIVEBODY_H
#define DECLARATIVEBODY_H

#include "declarativestaticbody.h"
#include <QQuickItem>
#include <Box2D.h>

class DeclarativeBody : public DeclarativeStaticBody{
    Q_OBJECT
    Q_ENUMS(ForceMode ExplosionType)
    Q_PROPERTY(qreal mass READ mass WRITE setMass NOTIFY massChanged)
    Q_PROPERTY(QPointF linearVelocity READ linearVelocity WRITE setLinearVelocity NOTIFY linearVelocityChanged)
    Q_PROPERTY(qreal angularVelocity READ angularVelocity WRITE setAngularVelocity NOTIFY angularVelocityChanged)
    Q_PROPERTY(qreal linearDamping READ linearDamping WRITE setLinearDamping NOTIFY linearDampingChanged)
    Q_PROPERTY(qreal angularDamping READ angularDamping WRITE setAngularDamping NOTIFY angularDampingChanged)

public:
    explicit DeclarativeBody(QQuickItem *parent = 0);
    enum ForceMode{ Force, Impulse };
    enum ExplosionType{ ProximityExplosion, RaycastExplosion };

    qreal mass() const;
    void setMass(const qreal mass);
    QPointF linearVelocity() const;
    void setLinearVelocity(const QPointF velocity);
    qreal angularVelocity() const;
    void setAngularVelocity(const qreal angularVelocity);
    qreal linearDamping() const;
    void setLinearDamping(const qreal linearDamping);
    qreal angularDamping() const;
    void setAngularDamping(const qreal angularDamping);


    Q_INVOKABLE void addForce(const QPointF &force,const QPointF &position=QPointF(0,0), ForceMode mode=Force);
    Q_INVOKABLE void addTorque(qreal torque, ForceMode mode=Force);
    Q_INVOKABLE void addExplosionForce(qreal force, qreal radius=0, QPointF position=QPointF(0,0), ExplosionType type=ProximityExplosion, ForceMode mode=Impulse);

    void worldUpdate();

signals:
    void massChanged();
    void linearVelocityChanged();
    void angularVelocityChanged();
    void linearDampingChanged();
    void angularDampingChanged();

protected:
    void componentComplete();

private:
    QPointF m_linearVelocity;
    qreal m_angularVelocity, m_linearDamping, m_angularDamping;

};

#endif // DECLARATIVEBODY_H
