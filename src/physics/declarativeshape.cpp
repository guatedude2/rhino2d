#include "declarativeworld.h"
#include "declarativeshape.h"


/** DeclarativeShape class **/
DeclarativeShape::DeclarativeShape(QObject *parent) :
    QObject(parent),
    m_body(0)
{
}

DeclarativeShape::~DeclarativeShape(){
    //Delete fixture or does box2d handle this?
    /*if (m_fixture){
        delete m_fixture;
    }*/
}

qreal DeclarativeShape::friction(){ return m_fixtureDef.friction; }
void DeclarativeShape::setFriction(qreal friction){
    if (m_fixtureDef.friction != friction){
        m_fixtureDef.friction = friction;
        foreach(b2Fixture *fixture, m_fixtures){
            fixture->SetFriction(friction);
        }
    }
}

qreal DeclarativeShape::restitution(){ return m_fixtureDef.restitution; }
void DeclarativeShape::setRestitution(qreal restitution){
    if (m_fixtureDef.restitution != restitution){
        m_fixtureDef.restitution = restitution;
        foreach(b2Fixture *fixture, m_fixtures){
            fixture->SetRestitution(restitution);
        }
    }
}

qreal DeclarativeShape::density(){ return m_fixtureDef.density; }
void DeclarativeShape::setDensity(qreal density){
    if (m_fixtureDef.density != density){
        m_fixtureDef.density = density;
        foreach(b2Fixture *fixture, m_fixtures){
            fixture->SetDensity(density);
        }
    }
}

bool DeclarativeShape::isSensor(){ return m_fixtureDef.isSensor; }
void DeclarativeShape::setSensor(bool sensor){
    if (m_fixtureDef.isSensor != sensor){
        m_fixtureDef.isSensor = sensor;
        foreach(b2Fixture *fixture, m_fixtures){
            fixture->SetSensor(sensor);
        }
    }
}

int DeclarativeShape::collisionGroup(){ return m_fixtureDef.filter.groupIndex; }
void DeclarativeShape::setCollisionGroup(int groupId){
    if (m_fixtureDef.filter.groupIndex != groupId){
        m_fixtureDef.filter.groupIndex = groupId;
        foreach(b2Fixture *fixture, m_fixtures){
            b2Filter filter = fixture->GetFilterData();
            filter.groupIndex = groupId;
            fixture->SetFilterData(filter);
        }
    }
}

//uint16 DeclarativeShape::collideAgainst(){ return m_fixtureDef.filter.maskBits; }
//void DeclarativeShape::setCollideAgainst(uint16 groupId){
//    //groupId = qMax(1, groupId);
//    if (m_fixtureDef.filter.maskBits != groupId){
//        m_fixtureDef.filter.maskBits = groupId;
//        if(m_fixture){
//            b2Filter filter = m_fixture->GetFilterData();
//            filter.maskBits = groupId;
//            m_fixture->SetFilterData(filter);
//        }
//    }
//}

//uint16 DeclarativeShape::collisionGroup(){ return m_fixtureDef.filter.categoryBits; }
//void DeclarativeShape::setCollisionGroup(uint16 groupId){
//    //groupId = qMax(1, groupId);
//    if (m_fixtureDef.filter.categoryBits != groupId){
//        m_fixtureDef.filter.categoryBits = groupId;
//        if(m_fixture){
//            b2Filter filter = m_fixture->GetFilterData();
//            filter.categoryBits = groupId;
//            m_fixture->SetFilterData(filter);
//        }
//    }
//}

QRectF DeclarativeShape::boundingRect(){ return m_boundRect; }
void DeclarativeShape::setBoundingRect(QRectF boundRect){
    m_boundRect = boundRect;
}

void DeclarativeShape::createFixture(DeclarativeStaticBody *body){
    m_body = body;
    applyShape();
}

void DeclarativeShape::destroyFixture(){
    if(m_fixtures.count()>0 && m_body && m_body->body()){
        foreach(b2Fixture *fixture, m_fixtures){
            m_body->body()->DestroyFixture(fixture);
        }
    }
    m_fixtures.clear();
}

void DeclarativeShape::applyShape(){
    if (m_body && m_body->body()){
        QList<b2Shape *> shapes = createShapes();
        if (shapes.count()>0){
            destroyFixture();
            foreach(b2Shape *shape, shapes){
                m_fixtureDef.shape = shape;
                b2Fixture *fixture = m_body->body()->CreateFixture(&m_fixtureDef);
                fixture->SetUserData(m_body);
                m_fixtures.append(fixture);
                delete shape;
            }
        }
        m_body->updateShapeBounds();
    }
}

DeclarativeStaticBody *DeclarativeShape::body(){ return m_body;}


/** DeclarativeRectangleShape class **/
DeclarativeRectangleShape::DeclarativeRectangleShape(QObject *parent) :
    DeclarativeShape(parent)
{
}

qreal DeclarativeRectangleShape::width(){ return m_shape.width(); }
void DeclarativeRectangleShape::setWidth(qreal width){
    if (width != m_shape.width()){
        m_shape.setWidth(width);
        applyShape();
        emit widthChanged();
    }
}

qreal DeclarativeRectangleShape::height(){ return m_shape.height(); }
void DeclarativeRectangleShape::setHeight(qreal height){
    if (height != m_shape.height()){
        m_shape.setHeight(height);
        applyShape();
        emit heightChanged();
    }
}

QList<b2Shape *> DeclarativeRectangleShape::createShapes(){
    QList<b2Shape *> shapes;
    if (!m_shape.isEmpty() && body()){
        b2Vec2 * vertices = new b2Vec2[4];
        QRectF rect(0, 0, fmax(this->width()*body()->scale(), 2.0f) / PIXELS_PER_METER, fmax(this->height()*body()->scale(), 2.0f) / PIXELS_PER_METER);
        rect.translate(QPointF(-rect.width()/2.0f, rect.height()/2.0f));
        vertices[0].Set(rect.x(), rect.y());
        vertices[1].Set(rect.x() , rect.y() - rect.height());
        vertices[2].Set(rect.x() + rect.width(), rect.y() - rect.height());
        vertices[3].Set(rect.x() + rect.width() , rect.y() );
        b2PolygonShape *shape = new b2PolygonShape;
        shape->Set(vertices, 4);
        setBoundingRect(QRectF(QPointF(0,0), m_shape));
        shapes.append(shape);
    }
    return shapes;
}


/** DeclarativeCircleShape class **/
DeclarativeCircleShape::DeclarativeCircleShape(QObject *parent) :
    DeclarativeShape(parent),
    m_radius(0)
{
}

qreal DeclarativeCircleShape::radius(){ return m_radius; }
void DeclarativeCircleShape::setRadius(qreal radius){
    if (radius != m_radius){
        m_radius = radius;
        applyShape();
        emit radiusChanged();
    }
}

QList<b2Shape *> DeclarativeCircleShape::createShapes(){
    QList<b2Shape *> shapes;
    if (m_radius>0 && body()){
        b2CircleShape *shape = new b2CircleShape;
        shape->m_p.Set(0, 0);
        shape->m_radius = m_radius / PIXELS_PER_METER;
        setBoundingRect(QRectF(0,0, m_radius*2.0f, m_radius*2.0f));
        shapes.append(shape);
    }
    return shapes;
}


/** DeclarativeEllipseShape class **/
DeclarativeEllipseShape::DeclarativeEllipseShape(QObject *parent) :
    DeclarativeShape(parent)
{

}

qreal DeclarativeEllipseShape::width(){ return m_shape.width(); }
void DeclarativeEllipseShape::setWidth(qreal width){
    if (width != m_shape.width()){
        m_shape.setWidth(width);
        applyShape();
        emit widthChanged();
    }
}

qreal DeclarativeEllipseShape::height(){ return m_shape.height(); }
void DeclarativeEllipseShape::setHeight(qreal height){
    if (height != m_shape.height()){
        m_shape.setHeight(height);
        applyShape();
        emit heightChanged();
    }
}

QList<b2Shape *> DeclarativeEllipseShape::createShapes(){
    QList<b2Shape *> shapes;
    if (!m_shape.isEmpty() && body()){
        b2Vec2 *verticesTop = new b2Vec2[8];
        b2Vec2 *verticesBottom = new b2Vec2[8];
        qreal width = fmax(this->width()*body()->scale(), 2.0f) / PIXELS_PER_METER;
        qreal height = fmax(this->height()*body()->scale(), 2.0f) / PIXELS_PER_METER;
        for(int i=0; i<8; i++){
            verticesTop[i].Set(cosf(i*M_PI / 7.0f)*width, sin(i*M_PI / 7.0f)*height);
            verticesBottom[i].Set(cosf(M_PI - i*M_PI / 7.0f)*width, -sin(M_PI - i*M_PI / 7.0f)*height);
        }
        b2PolygonShape *shapeTop = new b2PolygonShape;
        shapeTop->Set(verticesTop, 8);
        shapes.append(shapeTop);
        b2PolygonShape *shapeBottom = new b2PolygonShape;
        shapeBottom->Set(verticesBottom, 8);
        shapes.append(shapeBottom);
        setBoundingRect(QRectF(QPointF(0,0), m_shape));

    }
    return shapes;
}


/** DeclarativePolygonShape class **/
DeclarativePolygonShape::DeclarativePolygonShape(QObject *parent) :
    DeclarativeShape(parent)
{
}

QQmlListProperty<DeclarativePolygonPoint> DeclarativePolygonShape::points(){
    return QQmlListProperty<DeclarativePolygonPoint>(this, m_points); //TODO: change to non-prototype version
}

QList<b2Shape *> DeclarativePolygonShape::createShapes(){
    QList<b2Shape *> shapes;
    QPolygonF polygon;
    for(int i=m_points.count()-1; i>=0; --i){
        polygon.append(m_points.at(i)->toPointF());
    }

    if (m_points.count()>1 && body()){
        QPointF center(polygon.boundingRect().center().x(), polygon.boundingRect().center().y());
        polygon.append(polygon.at(0));
        for(int i=1; i<polygon.count(); i++){
            b2Vec2 *vertices = new b2Vec2[3];
            vertices[0].Set(center.x() / PIXELS_PER_METER, -center.y() / PIXELS_PER_METER);
            vertices[1].Set(polygon.at(i).x() / PIXELS_PER_METER, -polygon.at(i).y() / PIXELS_PER_METER);
            vertices[2].Set(polygon.at(i-1).x() / PIXELS_PER_METER, -polygon.at(i-1).y() / PIXELS_PER_METER);
            b2PolygonShape *triangle = new b2PolygonShape;
            triangle->Set(vertices, 3);
            shapes.append(triangle);
        }
        setBoundingRect(QRectF(QPointF(0,0), polygon.boundingRect().size()));

    }
    return shapes;
}
