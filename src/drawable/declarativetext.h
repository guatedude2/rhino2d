#ifndef DECLARATIVETEXT_H
#define DECLARATIVETEXT_H

#include "../core/declarativecore.h"
#include <QTextDocument>
#include <QQuickItem>

class DeclarativeStyle;
class DeclarativeText : public CoreSprite
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(DeclarativeStyle *style READ style)
public:
    explicit DeclarativeText(QQuickItem *parent = 0);

    QString text() const;
    void setText(const QString &text);
    DeclarativeStyle *style();
signals:
    void textChanged();

protected:
    void paint(QPainter *painter);
    void componentComplete();

protected slots:
    void updateSize();

private:
    QString m_text;
    DeclarativeStyle *m_style;
    QTextDocument m_doc;
};


class DeclarativeStyle : public QObject
{
    Q_OBJECT
    Q_ENUMS(Alignment)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QColor fill READ fill WRITE setFill NOTIFY fillChanged)
    Q_PROPERTY(Alignment align READ align WRITE setAlign NOTIFY alignChanged)
    Q_PROPERTY(QColor stroke READ stroke WRITE setStroke NOTIFY strokeChanged)
    Q_PROPERTY(int strokeThickness READ strokeThickness WRITE setStrokeThickness NOTIFY strokeThicknessChanged)
    Q_PROPERTY(bool wordWrap READ wordWrap WRITE setWordWrap NOTIFY wordWrapChanged)
public:
    explicit DeclarativeStyle(QObject *parent = 0);
    enum Alignment{ Left = 0x0001, Center = 0x0004, Right = 0x0002, Justify = 0x0008 };

    QFont font() const;
    void setFont(const QFont font);
    QColor fill() const;
    void setFill(const QColor color);
    Alignment align() const;
    void setAlign(const Alignment align);
    QColor stroke() const;
    void setStroke(const QColor color);
    int strokeThickness() const;
    void setStrokeThickness(const int thickness);
    bool wordWrap() const;
    void setWordWrap(const bool wordWrap);

signals:
    void fontChanged();
    void fillChanged();
    void alignChanged();
    void strokeChanged();
    void strokeThicknessChanged();
    void wordWrapChanged();

    void styleChanged();

private:
    QFont m_font;
    QColor m_fill;
    Alignment m_align;
    QColor m_stroke;
    int m_strokeThickness;
    bool m_wordWrap;

};

#endif // DECLARATIVETEXT_H
