#include "declarativescene.h"
#include "../core/declarativegamewindow.h"


DeclarativeScene::DeclarativeScene(QQuickItem *parent) :
    QQuickItem(parent),
    m_stage(0),
    m_backgroundColor(Qt::black),
    m_world(0)
{
    setAcceptHoverEvents(true);
    setAcceptedMouseButtons(Qt::AllButtons);
    setFlag(QQuickItem::ItemClipsChildrenToShape);
    if (DeclarativeGameWindow::instance()){
        if (DeclarativeGameWindow::instance()->system() &&
            DeclarativeGameWindow::instance()->system()->stage()) m_stage = DeclarativeGameWindow::instance()->system()->stage();

        foreach(QObject *child, m_stage->children()){
            if (child!=this) child->deleteLater();
        }
        DeclarativeGameWindow::instance()->setScene(this);
    }
    for (int i=0; i<MAX_TOUCHPOINTS; i++) m_touchPoints.append(new QDeclarativeTouchPoint(true));

    setWorld(new DeclarativeWorld(this));
    world()->componentComplete();
}

DeclarativeScene::~DeclarativeScene(){
    m_objects.clear();
    qDeleteAll(m_touchPoints);
}

void DeclarativeScene::componentComplete(){
    QQuickItem::componentComplete();
    QVariant itemParent = QQmlProperty::read(this, "parent");
    QQmlProperty::write(this, "anchors.fill", itemParent);

    if (DeclarativeGameWindow::instance() && DeclarativeGameWindow::instance()->system()){
        DeclarativeGameWindow::instance()->system()->stage()->setBackgroundColor(m_backgroundColor);
    }
    forceActiveFocus();
}

QColor DeclarativeScene::backgroundColor(){ return m_backgroundColor; }
void DeclarativeScene::setBackgroundColor(const QColor color){
    if (m_backgroundColor != color){
        m_backgroundColor = color;
        DeclarativeGameWindow::instance()->system()->stage()->setBackgroundColor(m_backgroundColor);
        emit backgroundColorChanged();
    }
}

DeclarativeWorld *DeclarativeScene::world(){ return m_world; }
void DeclarativeScene::setWorld(DeclarativeWorld *world){
    if (world != m_world){
        m_world = world;
        emit worldChanged();
    }
}

QQmlListProperty<QDeclarativeTouchPoint> DeclarativeScene::touchPoints(){
    return QQmlListProperty<QDeclarativeTouchPoint>(this, m_touchPoints); //TODO: change to non-prototype version
}


void DeclarativeScene::run(){
    update();
}

void DeclarativeScene::pause() {
    if (DeclarativeGameWindow::instance()) DeclarativeGameWindow::instance()->audio()->pauseAll();
}

void DeclarativeScene::resume() {
    if (DeclarativeGameWindow::instance()) DeclarativeGameWindow::instance()->audio()->resumeAll();
}

void DeclarativeScene::update(){
    if(m_world) m_world->sceneUpdate();
    foreach(DeclarativeTimer *timer, m_timers){
        if(timer->time() >= 0) {
            if (timer->callback().isCallable()) timer->callback().callWithInstance(timer->callback().engine()->newQObject(timer));//.call();
            if(timer->repeat()) timer->reset();
            else m_timers.removeOne(timer);
        }
    }
    if (DeclarativeGameWindow::instance()->tweenEngine()) DeclarativeGameWindow::instance()->tweenEngine()->update();
    foreach(CoreDisplayObject *object, m_objects){
        emit object->sceneUpdate();
    }
}

void DeclarativeScene::keyPressEvent(QKeyEvent *e){
    QDeclarativeKeyEvent ke(e->type(), e->key(), e->modifiers(), e->text(), e->isAutoRepeat(), e->count());
    if (DeclarativeGameWindow::instance()) DeclarativeGameWindow::instance()->keyboard()->handleKeyDown(&ke);
    emit keyDown(&ke);
    //TOOD: need to handle android volume here
}
void DeclarativeScene::keyReleaseEvent(QKeyEvent *e){
    QDeclarativeKeyEvent ke(e->type(), e->key(), e->modifiers(), e->text(), e->isAutoRepeat(), e->count());
    if (DeclarativeGameWindow::instance()) DeclarativeGameWindow::instance()->keyboard()->handleKeyUp(&ke);
    emit keyUp(&ke);
    //TOOD: need to handle android volume here
}

void DeclarativeScene::mousePressEvent(QMouseEvent *e){
    QDeclarativeMouseEvent me(e->pos(), e->pos(), e->button(), e->buttons(), e->modifiers(), false, false);
    emit mouseDown(&me);

    m_swipeStartTime = QDateTime::currentMSecsSinceEpoch();
    m_swipeStartPos = QPointF(e->screenPos());
}

void DeclarativeScene::mouseReleaseEvent(QMouseEvent *e){
    QDeclarativeMouseEvent mePress(e->pos(), e->pos(), e->button(), e->buttons(), e->modifiers(), false, true);
    emit mouseUp(&mePress);

    if (boundingRect().contains(e->pos())){
        QDeclarativeMouseEvent meClick(e->pos(), e->pos(), e->button(), e->buttons(), e->modifiers(), true, true);
        emit click(&meClick);
    }
}

void DeclarativeScene::mouseMoveEvent(QMouseEvent *e){
    QDeclarativeMouseEvent me(e->pos(), e->pos(), e->button(), e->buttons(), e->modifiers(), false, true);
    emit mouseMove(&me);

    if (!m_swipeStartTime) return;
    qreal angle = QLineF(m_swipeStartPos, e->screenPos()).angle() * DEG_TO_RAD;
    if (e->screenPos().x() - m_swipeStartPos.x() >= MIN_SWIPEDIST) handleSwipe(SwipeRight, angle);
    else if (e->screenPos().x() - m_swipeStartPos.x() <= -MIN_SWIPEDIST) handleSwipe(SwipeLeft, angle);
    else if (e->screenPos().y() - m_swipeStartPos.y() >= MIN_SWIPEDIST) handleSwipe(SwipeDown, angle);
    else if (e->screenPos().y() - m_swipeStartPos.y() <= -MIN_SWIPEDIST) handleSwipe(SwipeUp, angle);
}

void DeclarativeScene::hoverMoveEvent(QHoverEvent *e){
    QDeclarativeMouseEvent me(e->pos(), e->pos(), Qt::NoButton, Qt::NoButton, e->modifiers(), false, false);
    emit mouseMove(&me);
}

void DeclarativeScene::handleSwipe(SwipeDirection dir, qreal angle) {
    qint64 time = QDateTime::currentMSecsSinceEpoch() - m_swipeStartTime;
    m_swipeStartTime = 0;
    if (time <= MAX_SWIPETIME) emit swipe(dir, angle);
}

void DeclarativeScene::touchEvent(QTouchEvent *e){
    switch (e->type()){
        case QEvent::TouchBegin:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd: {
            bool ended = false;
            bool moved = false;
            bool started = false;

            clearTouchLists();
            QList<QTouchEvent::TouchPoint> touchPoints = e->touchPoints();
            int numTouchPoints = touchPoints.count();
            //always remove released touches, and make sure we handle all releases before adds.
            foreach (const QTouchEvent::TouchPoint &p, touchPoints) {
                Qt::TouchPointState touchPointState = p.state();
                int id = p.id();
                if (touchPointState & Qt::TouchPointReleased) {
                    QDeclarativeTouchPoint *dtp = static_cast<QDeclarativeTouchPoint*>(m_activeTouchPoints.value(id));
                    if (!dtp) continue;
                    updateTouchPoint(dtp, &p);
                    dtp->setPressed(false);
                    m_touchReleased.append(dtp);
                    m_activeTouchPoints.remove(id);
                    ended = true;
                }
            }
            if (numTouchPoints <= MAX_TOUCHPOINTS) {
                foreach (const QTouchEvent::TouchPoint &p, touchPoints) {
                    Qt::TouchPointState touchPointState = p.state();
                    int id = p.id();
                    if (touchPointState & Qt::TouchPointReleased) {
                        //handled above
                    }else if (!m_activeTouchPoints.contains(id)) {
                        //could be pressed, moved, or stationary
                        addTouchPoint(&p);
                        started = true;
                    } else if (touchPointState & Qt::TouchPointMoved) {
                        QDeclarativeTouchPoint* dtp = static_cast<QDeclarativeTouchPoint*>(m_activeTouchPoints[id]);
                        Q_ASSERT(dtp);
                        m_touchMoved.append(dtp);
                        updateTouchPoint(dtp,&p);
                        moved = true;
                    } else {
                        QDeclarativeTouchPoint* dtp = static_cast<QDeclarativeTouchPoint*>(m_activeTouchPoints[id]);
                        Q_ASSERT(dtp);
                        updateTouchPoint(dtp,&p);
                    }
                }

                if (ended) emit touchEnd(m_touchReleased);
                if (moved) emit touchMove(m_touchMoved);
                if (started) emit touchStart(m_touchPressed);
                if (ended || moved || started) emit touchPointsChanged();

                if (e->type() == QEvent::TouchEnd) ungrab();
            }
            if (numTouchPoints==1) e->setAccepted(false); //allow mouse events
            break;
        }
        default:
            QQuickItem::touchEvent(e);
            break;
    }
}

void DeclarativeScene::clearTouchLists(){
    foreach (QObject *obj, m_touchReleased) {
        QDeclarativeTouchPoint *dtp = static_cast<QDeclarativeTouchPoint*>(obj);
        if (!dtp->isQmlDefined())
            delete dtp;
        else
            dtp->setInUse(false);
    }
    m_touchReleased.clear();
    m_touchPressed.clear();
    m_touchMoved.clear();
}

void DeclarativeScene::addTouchPoint(const QTouchEvent::TouchPoint *p){
    QDeclarativeTouchPoint *dtp = 0;
    foreach (QDeclarativeTouchPoint* tp, m_touchPoints) {
        if (!tp->inUse()) {
            tp->setInUse(true);
            dtp = tp;
            break;
        }
    }

    if (dtp == 0) dtp = new QDeclarativeTouchPoint(false);
    dtp->setPointId(p->id());
    updateTouchPoint(dtp,p);
    dtp->setPressed(true);
    m_activeTouchPoints.insert(p->id(), dtp);
    m_touchPressed.append(dtp);
}

void DeclarativeScene::updateTouchPoint(QDeclarativeTouchPoint *dtp, const QTouchEvent::TouchPoint *p){
    dtp->setX(p->pos().x());
    dtp->setY(p->pos().y());
    dtp->setPressure(p->pressure());
    dtp->setVelocity(p->velocity());
    dtp->setArea(p->rect());
    dtp->setStartX(p->startPos().x());
    dtp->setStartY(p->startPos().y());
    dtp->setPreviousX(p->lastPos().x());
    dtp->setPreviousY(p->lastPos().y());
    dtp->setSceneX(p->scenePos().x());
    dtp->setSceneY(p->scenePos().y());
}

void DeclarativeScene::ungrab(){
    if (m_activeTouchPoints.count()) {
        foreach (QObject *obj, m_activeTouchPoints)
            static_cast<QDeclarativeTouchPoint*>(obj)->setPressed(false);
        clearTouchLists();
        foreach (QObject *obj, m_activeTouchPoints) {
            QDeclarativeTouchPoint *dtp = static_cast<QDeclarativeTouchPoint*>(obj);
            if (!dtp->isQmlDefined())
                delete dtp;
            else
                dtp->setInUse(false);
        }
        m_activeTouchPoints.clear();
    }
}


void DeclarativeScene::addObject(CoreDisplayObject *object) {
    if(object) m_objects.append(object);
}


void DeclarativeScene::removeObject(CoreDisplayObject *object) {
    m_objects.removeOne(object);
}

DeclarativeTimer *DeclarativeScene::addTimer(const int time, const QJSValue &callback, const bool repeat){
    DeclarativeTimer *timer = new DeclarativeTimer(time, this);
    timer->setCallback(callback);
    timer->setRepeat(repeat);
    m_timers.append(timer);
    return timer;
}

void DeclarativeScene::removeTimer(DeclarativeTimer *timer, const bool doCallback){
    if (m_timers.contains(timer)){
        if (!doCallback) timer->setCallback(QJSValue());
        timer->setRepeat(false);
        timer->set(0);
    }
}
