#include "declarativetext.h"
#include <QPainterPathStroker>
#include <QFont>
#include <QBrush>
#include <QPen>
#include <QTextCharFormat>
#include <QTextCursor>
#include <QTextOption>

/** DeclarativeText class **/
DeclarativeText::DeclarativeText(QQuickItem *parent) :
    CoreSprite(parent),
    m_style(new DeclarativeStyle(this))
{
}

QString DeclarativeText::text() const{ return m_text; }
void DeclarativeText::setText(const QString &text){
    if (text != m_text){
        m_text = text;
        emit textChanged();
        updateSize();
    }
}

DeclarativeStyle *DeclarativeText::style(){ return m_style; }

void DeclarativeText::paint(QPainter *painter){
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);

    if (m_style->strokeThickness()>0){
        QTextDocument *doc = m_doc.clone();
        QTextCharFormat format;
        format.setTextOutline(QPen(m_style->stroke(), m_style->strokeThickness()*2.0));
        format.setForeground(m_style->stroke());

        QTextCursor cursor(doc);
        cursor.select(QTextCursor::Document);
        cursor.mergeCharFormat(format);
        doc->drawContents(painter);
    }

    QTextDocument *doc = m_doc.clone();
    QTextCharFormat format;
    format.setForeground(m_style->fill());

    QTextCursor cursor(doc);
    cursor.select(QTextCursor::Document);
    cursor.mergeCharFormat(format);
    doc->drawContents(painter);
}

void DeclarativeText::componentComplete(){
    CoreSprite::componentComplete();
    connect(m_style, SIGNAL(styleChanged()), this, SLOT(updateSize()));
    updateSize();
}

void DeclarativeText::updateSize(){
    if (!isComponentComplete()) return;
    QTextOption options;
    options.setWrapMode(m_style->wordWrap() ? QTextOption::WordWrap : QTextOption::NoWrap);

    QFont font(m_style->font());
    if (font.pixelSize()==-1) font.setPixelSize(20);
    m_doc.setDefaultFont(font);
    m_doc.setDefaultTextOption(options);
    m_doc.setHtml(m_text);
    if (widthValid()) m_doc.setTextWidth(width());

    setImplicitWidth(m_doc.size().width()+m_style->strokeThickness());
    setImplicitHeight(m_doc.size().height()+m_style->strokeThickness());
    QQuickPaintedItem::update();
}


/** DeclarativeStyle class **/
DeclarativeStyle::DeclarativeStyle(QObject *parent) :
    QObject(parent),
    m_fill(Qt::black),
    m_align(Left),
    m_stroke("black"),
    m_strokeThickness(0),
    m_wordWrap(false)
{
    m_font.setFamily("Arial");
    m_font.setWeight(QFont::Bold);
}

QFont DeclarativeStyle::font() const{ return m_font; }
void DeclarativeStyle::setFont(const QFont font){
    if (font != m_font){
        m_font = font;
        emit fontChanged();
        emit styleChanged();
    }
}

QColor DeclarativeStyle::fill() const{ return m_fill; }
void DeclarativeStyle::setFill(const QColor color){
    if (color != m_fill){
        m_fill = color;
        emit fillChanged();
        emit styleChanged();
    }
}

DeclarativeStyle::Alignment DeclarativeStyle::align() const{ return m_align; }
void DeclarativeStyle::setAlign(const Alignment align){
    if (align != m_align){
        m_align = align;
        emit alignChanged();
        emit styleChanged();
    }
}

QColor DeclarativeStyle::stroke() const{ return m_stroke; }
void DeclarativeStyle::setStroke(const QColor color){
    if (color != m_stroke){
        m_stroke = color;
        emit strokeChanged();
        emit styleChanged();
    }
}

int DeclarativeStyle::strokeThickness() const{ return m_strokeThickness; }
void DeclarativeStyle::setStrokeThickness(const int thickness){
    if (thickness != m_strokeThickness){
        m_strokeThickness = thickness;
        emit strokeThicknessChanged();
        emit styleChanged();
    }
}

bool DeclarativeStyle::wordWrap() const{ return m_wordWrap; }
void DeclarativeStyle::setWordWrap(const bool wordWrap){
    if (wordWrap != m_wordWrap){
        m_wordWrap = wordWrap;
        emit wordWrapChanged();
        emit styleChanged();
    }
}
