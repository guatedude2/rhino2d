#ifndef DECLARATIVESCENE_H
#define DECLARATIVESCENE_H

#define MAX_TOUCHPOINTS 10
#define MAX_SWIPETIME 500
#define MIN_SWIPEDIST 100

#include "../core/qdeclarativeevents_p_p.h"
#include "../core/declarativestage.h"
#include "../core/declarativecore.h"
#include "../core/declarativetimer.h"
#include "../physics/declarativeworld.h"

#include <QQuickItem>
#include <QKeyEvent>
#include <QSwipeGesture>

class DeclarativeScene : public QQuickItem
{
    Q_OBJECT
    Q_ENUMS(SwipeDirection)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(DeclarativeWorld *world READ world NOTIFY worldChanged)
    Q_PROPERTY(QQmlListProperty<QDeclarativeTouchPoint> touchPoints READ touchPoints NOTIFY touchPointsChanged)
public:
    explicit DeclarativeScene(QQuickItem *parent = 0);
    ~DeclarativeScene();
    enum SwipeDirection{ SwipeUp, SwipeDown, SwipeLeft, SwipeRight };

    void componentComplete();

    QColor backgroundColor();
    void setBackgroundColor(const QColor color);
    DeclarativeWorld *world();
    void setWorld(DeclarativeWorld *world);
    QQmlListProperty<QDeclarativeTouchPoint> touchPoints();

    void pause();
    void resume();

    void addObject(CoreDisplayObject *object);
    void removeObject(CoreDisplayObject *object);

    Q_INVOKABLE DeclarativeTimer *addTimer(const int time, const QJSValue &callback, const bool repeat=false);
    Q_INVOKABLE void removeTimer(DeclarativeTimer *timer, const bool doCallback);

Q_SIGNALS:
    void backgroundColorChanged();
    void worldChanged();
    void touchPointsChanged();

    void initStage();

    void click(QDeclarativeMouseEvent *event);
    void mouseDown(QDeclarativeMouseEvent *event);
    void mouseUp(QDeclarativeMouseEvent *event);
    void mouseMove(QDeclarativeMouseEvent *event);
    void mouseOut(QDeclarativeMouseEvent *event);
    void keyDown(QDeclarativeKeyEvent *event);
    void keyUp(QDeclarativeKeyEvent *event);
    void swipe(SwipeDirection direction, qreal angle);
    void touchStart(QList<QObject *> touchPoints);
    void touchMove(QList<QObject *> touchPoints);
    void touchEnd(QList<QObject *> touchPoints);

public slots:
    void run();

protected:
    void update();

    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void hoverMoveEvent(QHoverEvent *e);
    void handleSwipe(SwipeDirection dir, qreal angle);
    void touchEvent(QTouchEvent *e);
    void clearTouchLists();
    void addTouchPoint(const QTouchEvent::TouchPoint *p);
    void updateTouchPoint(QDeclarativeTouchPoint *dtp, const QTouchEvent::TouchPoint *p);
    void ungrab();

private:
    DeclarativeStage *m_stage;
    QColor m_backgroundColor;
    DeclarativeWorld *m_world;
    QList<CoreDisplayObject *> m_objects;
    QList<DeclarativeTimer *> m_timers;
    QList<QDeclarativeTouchPoint *> m_touchPoints;
    QMap<int,QObject*> m_activeTouchPoints;
    QList<QObject *> m_touchPressed, m_touchReleased, m_touchMoved;
    QPointF m_swipeStartPos;
    qint64 m_swipeStartTime;
};

#endif // DECLARATIVESCENE_H
