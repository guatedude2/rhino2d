#include "declarativemovieclip.h"

DeclarativeMovieClip::DeclarativeMovieClip(QQuickItem *parent) :
    CoreSprite(parent),
    m_currentFrame(0),
    m_animationSpeed(1),
    m_loop(true)
{
}


QStringList DeclarativeMovieClip::textures() const{ return m_textures; }
void DeclarativeMovieClip::setTextures(QStringList textures){
    m_textures = textures;
    setTexture(m_textures.at(qRound(m_currentFrame) % m_textures.count()));
    emit texturesChanged();
}

int DeclarativeMovieClip::currentFrame() const{ return qRound(m_currentFrame); }
void DeclarativeMovieClip::setCurrentFrame(int frame){
    if (frame != qRound(m_currentFrame)){
        m_currentFrame = frame;
        emit currentFrameChanged();
    }
}

int DeclarativeMovieClip::totalFrames() const{ return m_textures.count(); }
qreal DeclarativeMovieClip::animationSpeed() const{ return m_animationSpeed; }
void DeclarativeMovieClip::setAnimationSpeed(const qreal speed){
    if (speed != m_animationSpeed){
        m_animationSpeed = speed;
        emit animationSpeedChanged();
    }
}

bool DeclarativeMovieClip::loop() const{ return m_loop; }
void DeclarativeMovieClip::setLoop(const bool loop){
    if (loop != m_loop){
        m_loop = loop;
        emit loopChanged();
    }
}

bool DeclarativeMovieClip::playing() const{ return m_playing; }
void DeclarativeMovieClip::setPlaying(const bool playing){
    if (playing) this->play();
    else this->stop();
}

void DeclarativeMovieClip::play(){
    if (!m_playing){
        m_playing = true;
        emit playingChanged();
    }
}

void DeclarativeMovieClip::stop(){
    if (m_playing){
        m_playing = false;
        emit playingChanged();
    }
}

void DeclarativeMovieClip::gotoAndPlay(int frame){
    setCurrentFrame(frame);
    play();
}

void DeclarativeMovieClip::gotoAndStop(int frame){
    stop();
    setCurrentFrame(frame);
    setTexture(m_textures.at(frame % m_textures.count()));
}

void DeclarativeMovieClip::sceneUpdate(){
    if (!m_playing) return;
    m_currentFrame = m_currentFrame < m_textures.count() ? m_currentFrame + m_animationSpeed : 0;
    emit currentFrameChanged();
    int round = qRound(m_currentFrame);
    if (m_loop || round < m_textures.count()){
        setTexture(m_textures.at(round % m_textures.count()));
    }else if(round >= m_textures.count()){
        this->gotoAndStop(m_textures.count() - 1);
        emit completed();
    }
}


void DeclarativeMovieClip::setTexture(const QString &texture){
    CoreSprite::setTexture(CoreTexture::fromImage(QUrl(texture)));
}
