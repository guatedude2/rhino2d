#include "declarativetilingsprite.h"
#include "../core/declarativegamewindow.h"

DeclarativeTilingSprite::DeclarativeTilingSprite(QQuickItem *parent) :
    CoreTilingSprite(parent)
{
}

QPointF DeclarativeTilingSprite::speed(){ return m_speed; }
void DeclarativeTilingSprite::setSpeed(QPointF speed){
    if (speed != m_speed){
        m_speed = speed;
        emit speedChanged();
    }
}
QString DeclarativeTilingSprite::texture() const{ return m_texture; }
void DeclarativeTilingSprite::setTexture(const QString &texture){
    if (texture != m_texture){
        m_texture = texture;
        CoreSprite::setTexture(CoreTexture::fromImage(QUrl(texture)));
        emit textureChanged();
    }
}

void DeclarativeTilingSprite::sceneUpdate(){
    if (DeclarativeGameWindow::instance()){
        QPointF pos(tilePosition());
        if (m_speed.x()!=0) pos.setX(pos.x() + m_speed.x() * DeclarativeGameWindow::instance()->system()->delta());
        if (m_speed.y()!=0) pos.setX(pos.y() + m_speed.y() * DeclarativeGameWindow::instance()->system()->delta());
        if (m_speed.x()<0) pos.setX(fmod(pos.x(), implicitWidth()));
        else pos.setX(fmod((pos.x()-implicitWidth()), implicitWidth()));

        if (m_speed.y()<0) pos.setY(fmod(pos.y(), implicitHeight()));
        else pos.setY(fmod((pos.y()-implicitHeight()), implicitHeight()));
        setTilePosition(pos);
    }
}
