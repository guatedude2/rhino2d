#ifndef DECLARATIVEBITMAPTEXT_H
#define DECLARATIVEBITMAPTEXT_H

#include "../core/declarativecore.h"
#include "declarativetext.h"
#include <QQuickItem>
#include <QHash>
#include <QFont>

class DeclarativeBitmapText : public CoreSprite
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(DeclarativeStyle *style READ style)
public:
    explicit DeclarativeBitmapText(QQuickItem *parent = 0);
    struct BitmapChar{
        CoreTexture *texture;
        int line;
        QChar charCode;
        QPoint position;
    };

    QString text() const;
    void setText(const QString &text);
    DeclarativeStyle *style();

    static QHash<QString, Core::BitmapFontSettings> fonts;
signals:
    void textChanged();

protected:
    void paint(QPainter *painter);
    void componentComplete();

protected slots:
    void updateSize();

private:
    QString m_text;
    DeclarativeStyle *m_style;
    QList<BitmapChar> m_chars;
    QList<int> m_lineAlignOffsets;
    qreal m_fontScale;

};

#endif // DECLARATIVEBITMAPTEXT_H
