#include "declarativebitmaptext.h"

QHash<QString, Core::BitmapFontSettings> DeclarativeBitmapText::fonts;
DeclarativeBitmapText::DeclarativeBitmapText(QQuickItem *parent) :
    CoreSprite(parent),
    m_style(new DeclarativeStyle(this))
{
}

QString DeclarativeBitmapText::text() const{ return m_text; }
void DeclarativeBitmapText::setText(const QString &text){
    if (text != m_text){
        m_text = text;
        emit textChanged();
        updateSize();
    }
}

DeclarativeStyle *DeclarativeBitmapText::style(){ return m_style; }

void DeclarativeBitmapText::paint(QPainter *painter){
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);

//    var lenChildren = this.children.length;
//    var tint = this.tint || 0xFFFFFF;
    for(int i = 0; i<m_chars.count(); i++){
        BitmapChar c = m_chars[i];
        //painter->scale(m_fontScale, m_fontScale);

        if (!c.texture->frame().isEmpty()) painter->drawPixmap(QRectF(QPointF((c.position.x() + m_lineAlignOffsets[m_chars[i].line])*m_fontScale, c.position.y()*m_fontScale), c.texture->frame().size()*m_fontScale), c.texture->baseTexture()->source(), c.texture->frame());
//        var c = i < lenChildren ? this.children[i] : this._pool.pop(); // get old child if have. if not - take from pool.

//        if (c) c.setTexture(chars[i].texture); // check if got one before.
//        else c = new PIXI.Sprite(chars[i].texture); // if no create new one.

//        c.position.x = (chars[i].position.x + lineAlignOffsets[chars[i].line]) * scale;
//        c.position.y = chars[i].position.y * scale;
//        c.scale.x = c.scale.y = scale;
//        c.tint = tint;
//        if (!c.parent) this.addChild(c);
    }
}

void DeclarativeBitmapText::componentComplete(){
    CoreSprite::componentComplete();
    connect(m_style, SIGNAL(styleChanged()), this, SLOT(updateSize()));
    updateSize();
}

void DeclarativeBitmapText::updateSize(){
    if (!isComponentComplete()) return;

    m_chars.clear();
    m_lineAlignOffsets.clear();

    if (!DeclarativeBitmapText::fonts.contains(style()->font().family())) return;

    Core::BitmapFontSettings data = DeclarativeBitmapText::fonts[style()->font().family()];
    QPoint pos(0,0);
    QChar prevCharCode = 0;
    int maxLineWidth = 0;
    QList<int> lineWidths;
    int line = 0;
    m_fontScale = (style()->font().pixelSize()>0 ? style()->font().pixelSize() / (qreal)data.size : 1.0);

    for(int i = 0; i < text().count(); i++){
        QChar charCode = text().at(i);

        //charCode == QChar::CarriageReturn;
        //charCode == QChar::LineFeed;
        //charCode == QChar::LineFeed | QChar::CarriageReturn;

        //if (/(?:\r\n|\r|\n)/.test(this.text.charAt(i))){
        if (charCode=='\r' || charCode=='\n'){
            lineWidths.append(pos.x());
            maxLineWidth = qMax(maxLineWidth, pos.x());
            line++;

            pos.setX(0);
            pos.setY(pos.y()+data.lineHeight+4);
            prevCharCode = 0;
            continue;
        }

        if(!data.chars.contains(charCode.unicode())) continue;
        Core::BitmapFontSettings::Char charData = data.chars[charCode.unicode()];

        if(!prevCharCode.isNull()/* && charData[prevCharCode.unicode()]*/){
            pos.setX(pos.x()+charData.kerning[prevCharCode.unicode()]);
        }

        BitmapChar bchar;
        bchar.texture = charData.texture;
        bchar.line = line;
        bchar.charCode = charCode;
        bchar.position = QPoint(pos.x() + charData.xOffset, pos.y() + charData.yOffset);
        m_chars.append(bchar);
        pos.setX(pos.x()+charData.xAdvance);

        prevCharCode = charCode;
    }

    lineWidths.append(pos.x());
    maxLineWidth = qMax(maxLineWidth, pos.x());

    for(int i = 0; i <= line; i++){
        qreal alignOffset = 0;
        if(style()->align() == DeclarativeStyle::Right){
            alignOffset = maxLineWidth - lineWidths[i];
        }
        else if(style()->align() == DeclarativeStyle::Center){
            alignOffset = (maxLineWidth - lineWidths[i]) / 2.0;
        }
        m_lineAlignOffsets.append(alignOffset);
    }

    //m_fontScale = 1.0;
    setImplicitWidth(maxLineWidth * m_fontScale);
    setImplicitHeight((pos.y() + data.lineHeight+4) * m_fontScale);

    QQuickPaintedItem::update();
}
