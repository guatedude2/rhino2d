#ifndef DECLARATIVESPRITE_H
#define DECLARATIVESPRITE_H

#include "../core/declarativecore.h"
#include <QPainter>

class DeclarativeSprite : public CoreSprite
{
    Q_OBJECT
    Q_PROPERTY(QString texture READ texture WRITE setTexture NOTIFY textureChanged)
public:
    explicit DeclarativeSprite(QQuickItem *parent = 0);

    QString texture() const;
    void setTexture(const QString &texture);

signals:
   void textureChanged();

private:
   QString m_texture;


};

#endif // DECLARATIVESPRITE_H
