#ifndef DECLARATIVECONTAINER_H
#define DECLARATIVECONTAINER_H

#include "../core/declarativecore.h"
#include <QQuickItem>

class DeclarativeContainer : public CoreDisplayObject
{
    Q_OBJECT
public:
    explicit DeclarativeContainer(QQuickItem *parent = 0);
    void sceneUpdate();
};

#endif // DECLARATIVECONTAINER_H
