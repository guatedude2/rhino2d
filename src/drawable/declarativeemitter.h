#ifndef DECLARATIVEEMITTER_H
#define DECLARATIVEEMITTER_H

#include "../core/declarativecore.h"
#include "declarativesprite.h"
#include <QQuickItem>

class DeclarativeParticle;
class DeclarativeEmitter : public CoreDisplayObject{
    Q_OBJECT
    Q_PROPERTY(QString poolName READ poolName WRITE setPoolName NOTIFY poolNameChanged)
    Q_PROPERTY(QStringList textures READ textures WRITE setTextures NOTIFY texturesChanged)
    Q_PROPERTY(QList<DeclarativeParticle *> particles READ particles NOTIFY particlesChanged)

    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QPointF positionVar READ positionVar WRITE setPositionVar NOTIFY positionVarChanged)
    Q_PROPERTY(QPointF target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(QPointF velocityLimit READ velocityLimit WRITE setVelocityLimit NOTIFY velocityLimitChanged)
    Q_PROPERTY(qreal angle READ angle WRITE setAngle NOTIFY angleChanged)
    Q_PROPERTY(qreal angleVar READ angleVar WRITE setAngleVar NOTIFY angleVarChanged)
    Q_PROPERTY(qreal speed READ speed WRITE setSpeed NOTIFY speedChanged)
    Q_PROPERTY(qreal speedVar READ speedVar WRITE setSpeedVar NOTIFY speedVarChanged)
    Q_PROPERTY(qreal life READ life WRITE setLife NOTIFY lifeChanged)
    Q_PROPERTY(qreal lifeVar READ lifeVar WRITE setLifeVar NOTIFY lifeVarChanged)
    Q_PROPERTY(qreal duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(qreal rate READ rate WRITE setRate NOTIFY rateChanged)
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(qreal rotate READ rotate WRITE setRotate NOTIFY rotateChanged)
    Q_PROPERTY(qreal velRotate READ velRotate WRITE setVelRotate NOTIFY velRotateChanged)
    Q_PROPERTY(qreal velRotateVar READ velRotateVar WRITE setVelRotateVar NOTIFY velRotateVarChanged)
    Q_PROPERTY(qreal startAlpha READ startAlpha WRITE setStartAlpha NOTIFY startAlphaChanged)
    Q_PROPERTY(qreal endAlpha READ endAlpha WRITE setEndAlpha NOTIFY endAlphaChanged)
    Q_PROPERTY(qreal rotateVar READ rotateVar WRITE setRotateVar NOTIFY rotateVarChanged)
    Q_PROPERTY(qreal startScale READ startScale WRITE setStartScale NOTIFY startScaleChanged)
    Q_PROPERTY(qreal startScaleVar READ startScaleVar WRITE setStartScaleVar NOTIFY startScaleVarChanged)
    Q_PROPERTY(qreal endScale READ endScale WRITE setEndScale NOTIFY endScaleChanged)
    Q_PROPERTY(qreal endScaleVar READ endScaleVar WRITE setEndScaleVar NOTIFY endScaleVarChanged)
    Q_PROPERTY(qreal targetForce READ targetForce WRITE setTargetForce NOTIFY targetForceChanged)
    Q_PROPERTY(qreal accelAngle READ accelAngle WRITE setAccelAngle NOTIFY accelAngleChanged)
    Q_PROPERTY(qreal accelAngleVar READ accelAngleVar WRITE setAccelAngleVar NOTIFY accelAngleVarChanged)
    Q_PROPERTY(qreal accelSpeed READ accelSpeed WRITE setAccelSpeed NOTIFY accelSpeedChanged)
    Q_PROPERTY(qreal accelSpeedVar READ accelSpeedVar WRITE setAccelSpeedVar NOTIFY accelSpeedVarChanged)
    Q_PROPERTY(QVariantMap spriteSettings READ spriteSettings WRITE setSpriteSettings NOTIFY spriteSettingsChanged)
public:
    explicit DeclarativeEmitter(QQuickItem *parent = 0);
    ~DeclarativeEmitter();

    void componentComplete();

    QString poolName() const;
    void setPoolName(const QString poolName);
    QStringList textures() const;
    void setTextures(const QStringList textures);
    QList<DeclarativeParticle *> particles();
    QPointF position() const;
    void setPosition(const QPointF position);
    QPointF positionVar() const;
    void setPositionVar(const QPointF positionVar);
    QPointF target() const;
    void setTarget(const QPointF target);
    QPointF velocityLimit() const;
    void setVelocityLimit(const QPointF velocityLimit);
    qreal angle() const;
    void setAngle(const qreal angle);
    qreal angleVar() const;
    void setAngleVar(const qreal angleVar);
    qreal speed() const;
    void setSpeed(const qreal speed);
    qreal speedVar() const;
    void setSpeedVar(const qreal speedVar);
    qreal life() const;
    void setLife(const qreal life);
    qreal lifeVar() const;
    void setLifeVar(const qreal lifeVar);
    qreal duration() const;
    void setDuration(const qreal duration);
    qreal rate() const;
    void setRate(const qreal rate);
    int count() const;
    void setCount(const int count);
    bool active() const;
    void setActive(const bool active);
    qreal rotate() const;
    void setRotate(const qreal rotate);
    qreal velRotate() const;
    void setVelRotate(const qreal velRotate);
    qreal velRotateVar() const;
    void setVelRotateVar(const qreal velRotateVar);
    qreal startAlpha() const;
    void setStartAlpha(const qreal startAlpha);
    qreal endAlpha() const;
    void setEndAlpha(const qreal endAlpha);
    qreal rotateVar() const;
    void setRotateVar(const qreal rotateVar);
    qreal startScale() const;
    void setStartScale(const qreal startScale);
    qreal startScaleVar() const;
    void setStartScaleVar(const qreal startScaleVar);
    qreal endScale() const;
    void setEndScale(const qreal endScale);
    qreal endScaleVar() const;
    void setEndScaleVar(const qreal endScaleVar);
    qreal targetForce() const;
    void setTargetForce(const qreal targetForce);
    qreal accelAngle() const;
    void setAccelAngle(const qreal accelAngle);
    qreal accelAngleVar() const;
    void setAccelAngleVar(const qreal accelAngleVar);
    qreal accelSpeed() const;
    void setAccelSpeed(const qreal accelSpeed);
    qreal accelSpeedVar() const;
    void setAccelSpeedVar(const qreal accelSpeedVar);
    QVariantMap spriteSettings() const;
    void setSpriteSettings(const QVariantMap spriteSettings);

    Q_INVOKABLE void reset(bool resetVec);
    Q_INVOKABLE void emitParticles(int count=1);
    Q_INVOKABLE DeclarativeParticle *addParticle();
    Q_INVOKABLE void removeParticle(DeclarativeParticle *particle);

    void sceneUpdate();

signals:
    void poolNameChanged();
    void texturesChanged();
    void particlesChanged();
    void positionChanged();
    void positionVarChanged();
    void targetChanged();
    void velocityLimitChanged();
    void angleChanged();
    void angleVarChanged();
    void speedChanged();
    void speedVarChanged();
    void lifeChanged();
    void lifeVarChanged();
    void durationChanged();
    void rateChanged();
    void countChanged();
    void activeChanged();
    void rotateChanged();
    void velRotateChanged();
    void velRotateVarChanged();
    void startAlphaChanged();
    void endAlphaChanged();
    void rotateVarChanged();
    void startScaleChanged();
    void startScaleVarChanged();
    void endScaleChanged();
    void endScaleVarChanged();
    void targetForceChanged();
    void accelAngleChanged();
    void accelAngleVarChanged();
    void accelSpeedChanged();
    void accelSpeedVarChanged();
    void spriteSettingsChanged();

    void create();

protected:
    void updateParticle(DeclarativeParticle *particle);
    qreal getVariance(qreal value);
    QString randomTexture();

private:
    QString m_poolName;
    QStringList m_textures;
    QList<DeclarativeParticle *> m_particles;
    QPointF m_position;
    QPointF m_positionVar;
    QPointF m_target;
    QPointF m_velocityLimit;
    qreal m_angle;
    qreal m_angleVar;
    qreal m_speed;
    qreal m_speedVar;
    qreal m_life;
    qreal m_lifeVar;
    qreal m_duration;
    qreal m_durationTimer;
    qreal m_rate;
    qreal m_rateTimer;
    int m_count;
    bool m_active;
    qreal m_rotate;
    qreal m_velRotate;
    qreal m_velRotateVar;
    qreal m_startAlpha;
    qreal m_endAlpha;
    qreal m_rotateVar;
    qreal m_startScale;
    qreal m_startScaleVar;
    qreal m_endScale;
    qreal m_endScaleVar;
    qreal m_targetForce;
    qreal m_accelAngle;
    qreal m_accelAngleVar;
    qreal m_accelSpeed;
    qreal m_accelSpeedVar;
    QVariantMap m_spriteSettings;
};



class DeclarativeParticle : public QObject
{
    Q_OBJECT
    Q_PROPERTY(CoreVector *position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(CoreVector *velocity READ velocity WRITE setVelocity NOTIFY velocityChanged)
    Q_PROPERTY(CoreVector *accel READ accel WRITE setAccel NOTIFY accelChanged)
    Q_PROPERTY(DeclarativeSprite *sprite READ sprite NOTIFY spriteChanged)
    Q_PROPERTY(qreal life READ life WRITE setLife NOTIFY lifeChanged)
    Q_PROPERTY(qreal rotate READ rotate WRITE setRotate NOTIFY rotateChanged)
    Q_PROPERTY(qreal velRotate READ velRotate WRITE setVelRotate NOTIFY velRotateChanged)
    Q_PROPERTY(qreal deltaAlpha READ deltaAlpha WRITE setDeltaAlpha NOTIFY deltaAlphaChanged)
    Q_PROPERTY(qreal deltaScale READ deltaScale WRITE setDeltaScale NOTIFY deltaScaleChanged)

public:
    explicit DeclarativeParticle(QObject *parent = 0);

    CoreVector *position();
    void setPosition(CoreVector *position);
    CoreVector *velocity();
    void setVelocity(CoreVector *velocity);
    CoreVector *accel();
    void setAccel(CoreVector *accel);
    DeclarativeSprite *sprite();
    void setSprite(DeclarativeSprite *sprite);
    qreal life();
    void setLife(qreal life);
    qreal rotate();
    void setRotate(qreal rotate);
    qreal velRotate();
    void setVelRotate(qreal velRotate);
    qreal deltaAlpha();
    void setDeltaAlpha(qreal deltaAlpha);
    qreal deltaScale();
    void setDeltaScale(qreal deltaScale);

    Q_INVOKABLE void setVelocity(qreal angle, qreal speed);
    Q_INVOKABLE void setAccel(qreal angle, qreal speed);

signals:
    void positionChanged();
    void velocityChanged();
    void accelChanged();
    void spriteChanged();
    void lifeChanged();
    void rotateChanged();
    void velRotateChanged();
    void deltaAlphaChanged();
    void deltaScaleChanged();

private:
    DeclarativeSprite *m_sprite;
    CoreVector *m_position;
    CoreVector *m_velocity;
    CoreVector *m_accel;
    qreal m_life;
    qreal m_rotate;
    qreal m_velRotate;
    qreal m_deltaAlpha;
    qreal m_deltaScale;
};

#endif // DECLARATIVEEMITTER_H
