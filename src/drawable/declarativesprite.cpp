#include "declarativesprite.h"

DeclarativeSprite::DeclarativeSprite(QQuickItem *parent) :
    CoreSprite(parent)
{
}

QString DeclarativeSprite::texture() const{ return m_texture; }
void DeclarativeSprite::setTexture(const QString &texture){
    if (texture != m_texture){
        m_texture = texture;
        CoreSprite::setTexture(CoreTexture::fromImage(QUrl(texture)));
        emit textureChanged();
    }
}
