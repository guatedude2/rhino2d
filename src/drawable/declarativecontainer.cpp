#include "declarativecontainer.h"

DeclarativeContainer::DeclarativeContainer(QQuickItem *parent) :
    CoreDisplayObject(parent)
{
}

void DeclarativeContainer::sceneUpdate(){
    QRectF rect(childrenRect());
    setImplicitSize(rect.width(), rect.height());
    CoreDisplayObject::sceneUpdate();
}
