#ifndef DECLARATIVETILINGSPRITE_H
#define DECLARATIVETILINGSPRITE_H

#include "../core/declarativecore.h"
#include <QQuickItem>

class DeclarativeTilingSprite : public CoreTilingSprite
{
    Q_OBJECT
    Q_PROPERTY(QPointF speed READ speed WRITE setSpeed NOTIFY speedChanged)
    Q_PROPERTY(QString texture READ texture WRITE setTexture NOTIFY textureChanged)
public:
    explicit DeclarativeTilingSprite(QQuickItem *parent = 0);

    QPointF speed();
    void setSpeed(QPointF speed);
    QString texture() const;
    void setTexture(const QString &texture);

    void sceneUpdate();
signals:
    void speedChanged();
    void textureChanged();

private:
    QPointF m_speed;
    QString m_texture;

};

#endif // DECLARATIVETILINGSPRITE_H
