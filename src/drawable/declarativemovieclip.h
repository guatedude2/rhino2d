#ifndef DECLARATIVEMOVIECLIP_H
#define DECLARATIVEMOVIECLIP_H

#include "../core/declarativecore.h"
#include <QQuickItem>

class DeclarativeMovieClip : public CoreSprite
{
    Q_OBJECT
    Q_PROPERTY(QStringList textures READ textures WRITE setTextures NOTIFY texturesChanged)
    Q_PROPERTY(int currentFrame READ currentFrame WRITE setCurrentFrame NOTIFY currentFrameChanged)
    Q_PROPERTY(int totalFrames READ totalFrames NOTIFY totalFramesChanged)
    Q_PROPERTY(qreal animationSpeed READ animationSpeed WRITE setAnimationSpeed NOTIFY animationSpeedChanged)
    Q_PROPERTY(bool loop READ loop WRITE setLoop NOTIFY loopChanged)
    Q_PROPERTY(bool playing READ playing WRITE setPlaying NOTIFY playingChanged)

public:
    explicit DeclarativeMovieClip(QQuickItem *parent = 0);

    QStringList textures() const;
    void setTextures(QStringList textures);
    int currentFrame() const;
    void setCurrentFrame(int frame);
    int totalFrames() const;
    qreal animationSpeed() const;
    void setAnimationSpeed(const qreal speed);
    bool loop() const;
    void setLoop(const bool loop);
    bool playing() const;
    void setPlaying(const bool playing);

    Q_INVOKABLE void play();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void gotoAndPlay(int frame);
    Q_INVOKABLE void gotoAndStop(int frame);

    void sceneUpdate();

signals:
    void texturesChanged();
    void currentFrameChanged();
    void totalFramesChanged();
    void animationSpeedChanged();
    void loopChanged();
    void playingChanged();

    void completed();

protected:
    void setTexture(const QString &texture);

private:
    QStringList m_textures;
    qreal m_currentFrame;
    qreal m_animationSpeed;
    bool m_loop;
    bool m_playing;

};

#endif // DECLARATIVEMOVIECLIP_H
