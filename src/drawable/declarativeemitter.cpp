#include "declarativeemitter.h"
#include "../core/declarativegamewindow.h"
#include "../core/declarativepool.h"

/** DeclarativeEmitter class **/
DeclarativeEmitter::DeclarativeEmitter(QQuickItem *parent) :
    CoreDisplayObject(parent),
    m_poolName("emitter"),
    m_angle(0),
    m_angleVar(M_PI),
    m_speed(100),
    m_speedVar(0),
    m_life(2),
    m_lifeVar(0),
    m_duration(0),
    m_durationTimer(0),
    m_rate(0.1),
    m_rateTimer(0),
    m_count(10),
    m_active(true),
    m_rotate(0),
    m_velRotate(0),
    m_velRotateVar(0),
    m_startAlpha(1),
    m_endAlpha(0),
    m_rotateVar(0),
    m_startScale(1),
    m_startScaleVar(0),
    m_endScale(1),
    m_endScaleVar(0),
    m_targetForce(0),
    m_accelAngle(M_PI / 2.0),
    m_accelAngleVar(0),
    m_accelSpeed(0),
    m_accelSpeedVar(0)
{
    QVariantMap anchorValues;
    anchorValues.insert("x", 0.5);
    anchorValues.insert("y", 0.5);
    m_spriteSettings.insert("anchor", anchorValues);
}

DeclarativeEmitter::~DeclarativeEmitter(){
    //qDeleteAll(m_particles);
}

void DeclarativeEmitter::componentComplete(){
    CoreDisplayObject::componentComplete();
    DeclarativePool::create(m_poolName);
    emit create();

}

QString DeclarativeEmitter::poolName() const{ return m_poolName; }
void DeclarativeEmitter::setPoolName(const QString poolName){
      if (poolName != m_poolName){
             m_poolName = poolName;
             if (isComponentComplete()) DeclarativePool::create(m_poolName);
             emit poolNameChanged();
      }
}

QStringList DeclarativeEmitter::textures() const{ return m_textures; }
void DeclarativeEmitter::setTextures(const QStringList textures){
      if (textures != m_textures){
             m_textures = textures;
             emit texturesChanged();
      }
}

QList<DeclarativeParticle *> DeclarativeEmitter::particles(){ return m_particles; }

QPointF DeclarativeEmitter::position() const{ return m_position; }
void DeclarativeEmitter::setPosition(const QPointF position){
      if (position != m_position){
             m_position = position;
             emit positionChanged();
      }
}

QPointF DeclarativeEmitter::positionVar() const{ return m_positionVar; }
void DeclarativeEmitter::setPositionVar(const QPointF positionVar){
      if (positionVar != m_positionVar){
             m_positionVar = positionVar;
             emit positionVarChanged();
      }
}

QPointF DeclarativeEmitter::target() const{ return m_target; }
void DeclarativeEmitter::setTarget(const QPointF target){
      if (target != m_target){
             m_target = target;
             emit targetChanged();
      }
}

QPointF DeclarativeEmitter::velocityLimit() const{ return m_velocityLimit; }
void DeclarativeEmitter::setVelocityLimit(const QPointF velocityLimit){
      if (velocityLimit != m_velocityLimit){
             m_velocityLimit = velocityLimit;
             emit velocityLimitChanged();
      }
}

qreal DeclarativeEmitter::angle() const{ return m_angle; }
void DeclarativeEmitter::setAngle(const qreal angle){
      if (angle != m_angle){
             m_angle = angle;
             emit angleChanged();
      }
}

qreal DeclarativeEmitter::angleVar() const{ return m_angleVar; }
void DeclarativeEmitter::setAngleVar(const qreal angleVar){
      if (angleVar != m_angleVar){
             m_angleVar = angleVar;
             emit angleVarChanged();
      }
}

qreal DeclarativeEmitter::speed() const{ return m_speed; }
void DeclarativeEmitter::setSpeed(const qreal speed){
      if (speed != m_speed){
             m_speed = speed;
             emit speedChanged();
      }
}

qreal DeclarativeEmitter::speedVar() const{ return m_speedVar; }
void DeclarativeEmitter::setSpeedVar(const qreal speedVar){
      if (speedVar != m_speedVar){
             m_speedVar = speedVar;
             emit speedVarChanged();
      }
}

qreal DeclarativeEmitter::life() const{ return m_life; }
void DeclarativeEmitter::setLife(const qreal life){
      if (life != m_life){
             m_life = life;
             emit lifeChanged();
      }
}

qreal DeclarativeEmitter::lifeVar() const{ return m_lifeVar; }
void DeclarativeEmitter::setLifeVar(const qreal lifeVar){
      if (lifeVar != m_lifeVar){
             m_lifeVar = lifeVar;
             emit lifeVarChanged();
      }
}

qreal DeclarativeEmitter::duration() const{ return m_duration; }
void DeclarativeEmitter::setDuration(const qreal duration){
      if (duration != m_duration){
             m_duration = duration;
             emit durationChanged();
      }
}

qreal DeclarativeEmitter::rate() const{ return m_rate; }
void DeclarativeEmitter::setRate(const qreal rate){
      if (rate != m_rate){
             m_rate = rate;
             emit rateChanged();
      }
}

int DeclarativeEmitter::count() const{ return m_count; }
void DeclarativeEmitter::setCount(const int count){
      if (count != m_count){
             m_count = count;
             emit countChanged();
      }
}

bool DeclarativeEmitter::active() const{ return m_active; }
void DeclarativeEmitter::setActive(const bool active){
      if (active != m_active){
             m_active = active;
             emit activeChanged();
      }
}

qreal DeclarativeEmitter::rotate() const{ return m_rotate; }
void DeclarativeEmitter::setRotate(const qreal rotate){
      if (rotate != m_rotate){
             m_rotate = rotate;
             emit rotateChanged();
      }
}

qreal DeclarativeEmitter::velRotate() const{ return m_velRotate; }
void DeclarativeEmitter::setVelRotate(const qreal velRotate){
      if (velRotate != m_velRotate){
             m_velRotate = velRotate;
             emit velRotateChanged();
      }
}

qreal DeclarativeEmitter::velRotateVar() const{ return m_velRotateVar; }
void DeclarativeEmitter::setVelRotateVar(const qreal velRotateVar){
      if (velRotateVar != m_velRotateVar){
             m_velRotateVar = velRotateVar;
             emit velRotateVarChanged();
      }
}

qreal DeclarativeEmitter::startAlpha() const{ return m_startAlpha; }
void DeclarativeEmitter::setStartAlpha(const qreal startAlpha){
      if (startAlpha != m_startAlpha){
             m_startAlpha = startAlpha;
             emit startAlphaChanged();
      }
}

qreal DeclarativeEmitter::endAlpha() const{ return m_endAlpha; }
void DeclarativeEmitter::setEndAlpha(const qreal endAlpha){
      if (endAlpha != m_endAlpha){
             m_endAlpha = endAlpha;
             emit endAlphaChanged();
      }
}

qreal DeclarativeEmitter::rotateVar() const{ return m_rotateVar; }
void DeclarativeEmitter::setRotateVar(const qreal rotateVar){
      if (rotateVar != m_rotateVar){
             m_rotateVar = rotateVar;
             emit rotateVarChanged();
      }
}

qreal DeclarativeEmitter::startScale() const{ return m_startScale; }
void DeclarativeEmitter::setStartScale(const qreal startScale){
      if (startScale != m_startScale){
             m_startScale = startScale;
             emit startScaleChanged();
      }
}

qreal DeclarativeEmitter::startScaleVar() const{ return m_startScaleVar; }
void DeclarativeEmitter::setStartScaleVar(const qreal startScaleVar){
      if (startScaleVar != m_startScaleVar){
             m_startScaleVar = startScaleVar;
             emit startScaleVarChanged();
      }
}

qreal DeclarativeEmitter::endScale() const{ return m_endScale; }
void DeclarativeEmitter::setEndScale(const qreal endScale){
      if (endScale != m_endScale){
             m_endScale = endScale;
             emit endScaleChanged();
      }
}

qreal DeclarativeEmitter::endScaleVar() const{ return m_endScaleVar; }
void DeclarativeEmitter::setEndScaleVar(const qreal endScaleVar){
      if (endScaleVar != m_endScaleVar){
             m_endScaleVar = endScaleVar;
             emit endScaleVarChanged();
      }
}

qreal DeclarativeEmitter::targetForce() const{ return m_targetForce; }
void DeclarativeEmitter::setTargetForce(const qreal targetForce){
      if (targetForce != m_targetForce){
             m_targetForce = targetForce;
             emit targetForceChanged();
      }
}

qreal DeclarativeEmitter::accelAngle() const{ return m_accelAngle; }
void DeclarativeEmitter::setAccelAngle(const qreal accelAngle){
      if (accelAngle != m_accelAngle){
             m_accelAngle = accelAngle;
             emit accelAngleChanged();
      }
}

qreal DeclarativeEmitter::accelAngleVar() const{ return m_accelAngleVar; }
void DeclarativeEmitter::setAccelAngleVar(const qreal accelAngleVar){
      if (accelAngleVar != m_accelAngleVar){
             m_accelAngleVar = accelAngleVar;
             emit accelAngleVarChanged();
      }
}

qreal DeclarativeEmitter::accelSpeed() const{ return m_accelSpeed; }
void DeclarativeEmitter::setAccelSpeed(const qreal accelSpeed){
      if (accelSpeed != m_accelSpeed){
             m_accelSpeed = accelSpeed;
             emit accelSpeedChanged();
      }
}

qreal DeclarativeEmitter::accelSpeedVar() const{ return m_accelSpeedVar; }
void DeclarativeEmitter::setAccelSpeedVar(const qreal accelSpeedVar){
      if (accelSpeedVar != m_accelSpeedVar){
             m_accelSpeedVar = accelSpeedVar;
             emit accelSpeedVarChanged();
      }
}

QVariantMap DeclarativeEmitter::spriteSettings() const{ return m_spriteSettings; }
void DeclarativeEmitter::setSpriteSettings(const QVariantMap spriteSettings){
      if (spriteSettings != m_spriteSettings){
             m_spriteSettings = spriteSettings;
             emit spriteSettingsChanged();
      }
}

void DeclarativeEmitter::reset(bool resetVec){
    if (resetVec){
        m_position = QPointF(0,0);
        m_positionVar = QPointF(0,0);
        m_target = QPointF(0,0);
        m_velocityLimit = QPointF(0,0);
    }
    m_angle = 0; m_angleVar = M_PI; m_speed = 100; m_speedVar = 0; m_life = 2; m_lifeVar = 0; m_duration = 0; m_rate = 0.1; m_count = 10; m_rotate = 0;
    m_velRotate = 0; m_velRotateVar = 0; m_startAlpha = 1; m_endAlpha = 0; m_rotateVar = 0; m_startScale = 1; m_startScaleVar = 0; m_endScale = 1;
    m_endScaleVar = 0; m_targetForce = 0; m_accelAngle = M_PI / 2.0; m_accelAngleVar = 0; m_accelSpeed = 0; m_accelSpeedVar = 0;
}

void DeclarativeEmitter::emitParticles(int count){
    for (int i = 0; i < count; i++) {
        addParticle();
    }
}

DeclarativeParticle *DeclarativeEmitter::addParticle(){
    QObject *particleObj = DeclarativePool::get(this->poolName());
    DeclarativeParticle * particle = (particleObj ? static_cast<DeclarativeParticle *>(particleObj) : 0);
    if(!particle) particle = new DeclarativeParticle();

    particle->position()->setX(this->position().x() + getVariance(this->positionVar().x()));
    particle->position()->setY(this->position().y() + getVariance(this->positionVar().y()));

    qreal angleVar = getVariance(this->angleVar());
    qreal angle = this->angle() + angleVar;
    qreal speed = this->speed() + getVariance(this->speedVar());

    particle->setVelocity(angle, speed);

    if(this->angleVar() != this->accelAngleVar()) angleVar = getVariance(this->accelAngleVar());

    angle = this->accelAngle() + angleVar;
    speed = this->accelSpeed() + getVariance(this->accelSpeedVar());

    particle->setAccel(angle, speed);

    particle->setLife(this->life() + getVariance(this->lifeVar()));

    if (!particle->sprite()) {
        particle->setSprite(new DeclarativeSprite());
        particle->sprite()->setTexture(this->randomTexture());
        particle->sprite()->setX(particle->position()->x());
        particle->sprite()->setY(particle->position()->y());
        foreach(const QString property, this->spriteSettings().keys()){
            particle->sprite()->setProperty(property.toLatin1(), this->spriteSettings().value(property));
        }
    } else {
        particle->sprite()->setTexture(this->randomTexture());
        particle->sprite()->setX(particle->position()->x());
        particle->sprite()->setY(particle->position()->y());
    }

    particle->setRotate(this->rotate() + getVariance(this->rotateVar()));
    particle->setVelRotate(this->velRotate() + getVariance(this->velRotateVar()));

    if (this->startAlpha() != this->endAlpha()) {
        particle->setDeltaAlpha(this->endAlpha() - this->startAlpha());
        particle->setDeltaAlpha(particle->deltaAlpha() / particle->life());
    } else particle->setDeltaAlpha(0);

    particle->sprite()->setOpacity(this->startAlpha());

    qreal startScale = this->startScale() + getVariance(this->startScaleVar());
    if (this->startScale() != this->endScale()) {
        particle->setDeltaScale(this->endScale() + getVariance(this->endScaleVar()) - startScale);
        particle->setDeltaScale(particle->deltaScale() / particle->life());
    } else particle->setDeltaScale(0);
    particle->sprite()->setScale(startScale);

    particle->sprite()->setParentItem(this);

    m_particles.append(particle);
    return particle;
}

void DeclarativeEmitter::removeParticle(DeclarativeParticle *particle){
    if (m_particles.contains(particle)){
        particle->sprite()->setParentItem(0);
        DeclarativePool::put(m_poolName, particle);
        m_particles.removeOne(particle);
    }
}

void DeclarativeEmitter::sceneUpdate(){
    m_durationTimer += DeclarativeGameWindow::instance()->system()->delta();
    if(this->duration() > 0) this->setActive(m_durationTimer < duration());

    if(this->rate() && this->active()) {
        m_rateTimer += DeclarativeGameWindow::instance()->system()->delta();
        if(m_rateTimer >= this->rate()) {
            m_rateTimer = 0;
            emitParticles(this->count());
        }
    }

    foreach(DeclarativeParticle *particle, m_particles){
        if (particle) updateParticle(particle);
    }
    CoreDisplayObject::sceneUpdate();
}

void DeclarativeEmitter::updateParticle(DeclarativeParticle *particle){
    if (!particle || !DeclarativeGameWindow::instance() || !DeclarativeGameWindow::instance()->system()) return;
    if(particle->life() > 0) {
        particle->setLife(particle->life() - DeclarativeGameWindow::instance()->system()->delta());
        if(particle->life() <= 0) return removeParticle(particle);
    }

    if(m_targetForce > 0) {
        particle->accel()->set(m_target.x() - particle->position()->x(), m_target.y() - particle->position()->y());
        particle->accel()->normalize();
        particle->accel()->multiply(m_targetForce);
    }

    particle->velocity()->multiplyAdd(particle->accel(), DeclarativeGameWindow::instance()->system()->delta());
    if (m_velocityLimit.x() > 0 || m_velocityLimit.y() > 0) particle->velocity()->limit(m_velocityLimit);
    if (particle->velRotate()) particle->velocity()->rotate(particle->velRotate() * DeclarativeGameWindow::instance()->system()->delta());
    particle->position()->multiplyAdd(particle->velocity(), /*game.scale */ DeclarativeGameWindow::instance()->system()->delta());

    if (particle->deltaAlpha()) particle->sprite()->setOpacity(qMax(0.0, particle->sprite()->opacity() + particle->deltaAlpha()) * DeclarativeGameWindow::instance()->system()->delta());
    if (particle->deltaScale()) particle->sprite()->setScale(particle->deltaScale() * DeclarativeGameWindow::instance()->system()->delta());

    particle->sprite()->setRotation(particle->sprite()->rotation() + particle->rotate() * DeclarativeGameWindow::instance()->system()->delta());
    particle->sprite()->setPosition(particle->position()->toPointF());
}

qreal DeclarativeEmitter::getVariance(qreal value){
    qreal rand = (qrand() % 100) / 100.0;
    return (rand * value) * (rand > 0.5 ? -1.0 : 1.0);
}

QString DeclarativeEmitter::randomTexture(){
    return m_textures.at(qrand() % m_textures.count());
}



/** DeclarativeParticle class **/
DeclarativeParticle::DeclarativeParticle(QObject *parent) :
    QObject(parent), m_sprite(0),
    m_life(0),
    m_rotate(0),
    m_velRotate(0),
    m_deltaAlpha(0),
    m_deltaScale(0)
{
    m_position = new CoreVector(this);
    m_velocity = new CoreVector(this);
    m_accel = new CoreVector(this);
}

CoreVector *DeclarativeParticle::position(){ return m_position; }
void DeclarativeParticle::setPosition(CoreVector *position){
    if (position != m_position){
        m_position = position;
        emit positionChanged();
    }
}

CoreVector *DeclarativeParticle::velocity(){ return m_velocity; }
void DeclarativeParticle::setVelocity(CoreVector *velocity){
    if (velocity != m_velocity){
        m_velocity = velocity;
        emit velocityChanged();
    }
}

CoreVector *DeclarativeParticle::accel(){ return m_accel; }
void DeclarativeParticle::setAccel(CoreVector *accel){
    if (accel != m_accel){
        m_accel = accel;
        emit accelChanged();
    }
}

DeclarativeSprite *DeclarativeParticle::sprite(){ return m_sprite; }
void DeclarativeParticle::setSprite(DeclarativeSprite *sprite){
    m_sprite = sprite;
    emit spriteChanged();
}

qreal DeclarativeParticle::life(){ return m_life; }

void DeclarativeParticle::setLife(qreal life){
    if (life != m_life){
        m_life = life;
        emit lifeChanged();
    }
}

qreal DeclarativeParticle::rotate(){ return m_rotate; }
void DeclarativeParticle::setRotate(qreal rotate){
    if (rotate != m_rotate){
        m_rotate = rotate;
        emit rotateChanged();
    }
}
qreal DeclarativeParticle::velRotate(){ return m_velRotate; }
void DeclarativeParticle::setVelRotate(qreal velRotate){
    if (velRotate != m_velRotate){
        m_velRotate = velRotate;
        emit velRotateChanged();
    }
}

qreal DeclarativeParticle::deltaAlpha(){ return m_deltaAlpha; }
void DeclarativeParticle::setDeltaAlpha(qreal deltaAlpha){
    if (deltaAlpha != m_deltaAlpha){
        m_deltaAlpha = deltaAlpha;
        emit deltaAlphaChanged();
    }
}

qreal DeclarativeParticle::deltaScale(){ return m_deltaScale; }
void DeclarativeParticle::setDeltaScale(qreal deltaScale){
    if (deltaScale != m_deltaScale){
        m_deltaScale = deltaScale;
        emit deltaScaleChanged();
    }
}

void DeclarativeParticle::setVelocity(qreal angle, qreal speed){
    velocity()->set(qCos(angle) * speed, qSin(angle) * speed);
}

void DeclarativeParticle::setAccel(qreal angle, qreal speed){
    accel()->set(qCos(angle) * speed, qSin(angle) * speed);
}
