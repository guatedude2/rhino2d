#include "declarativekeyboard.h"

DeclarativeKeyboard::DeclarativeKeyboard(QObject *parent) :
    QObject(parent)
{
}

bool DeclarativeKeyboard::isKeyPressed(Qt::Key key){
    return (m_pressedKeys.contains(key));
}

void DeclarativeKeyboard::handleKeyUp(QDeclarativeKeyEvent *event){
    m_pressedKeys.removeOne(event->key());
    emit keyUp(event);
}

void DeclarativeKeyboard::handleKeyDown(QDeclarativeKeyEvent *event){
    if (!isKeyPressed(Qt::Key(event->key()))) m_pressedKeys.append(event->key());
    emit keyDown(event);
}
