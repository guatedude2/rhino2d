#include "declarativepool.h"

#include <QDebug>

QHash<QString, QList<QObject *> *> DeclarativePool::s_pools;
DeclarativePool::DeclarativePool(QObject *parent) :
    QObject(parent)
{
}

bool DeclarativePool::create(QString pool) {
    if (!DeclarativePool::s_pools.contains(pool)){
        DeclarativePool::s_pools.insert(pool, new QList<QObject *>());
        return true;
    }
    return false;
}

QObject *DeclarativePool::get(QString pool){
    QObject *object = 0;
    if (DeclarativePool::s_pools.contains(pool) && DeclarativePool::s_pools.value(pool)->count() > 0){
        object = DeclarativePool::s_pools.value(pool)->last();
        DeclarativePool::s_pools.value(pool)->pop_back();
    }
    return object;
}

bool DeclarativePool::put(QString pool, QObject *object) {
    if (!DeclarativePool::s_pools.contains(pool)) return false;
    DeclarativePool::s_pools.value(pool)->push_back(object);
    return true;
}
