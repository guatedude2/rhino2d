#include "declarativeloader.h"
#include "declarativegamewindow.h"

#include <QtQml/QQmlInfo>
#include <QDateTime>

int DeclarativeLoader::s_timeout = 500;

DeclarativeLoader::DeclarativeLoader(QQuickItem *parent) :
    QQuickItem(parent),
    m_startTime(0),
    m_assetLoaderTimerId(0),
    m_scene(0),
    m_stage(0),
    m_timer(0),
    m_backgroundColor(Qt::black),
    m_loader(0),
    m_percent(0),
    m_loaded(0),
    m_currentAsset(0),
    m_ready(false)
{
}

void DeclarativeLoader::init(QQmlComponent *scene){
    if(DeclarativeTimer::seconds()) DeclarativeLoader::s_timeout /= 1000;

    m_scene = scene;
    m_stage = DeclarativeGameWindow::instance()->system()->stage();


    foreach(const QString id, DeclarativeGameWindow::instance()->resources().keys()){
        if (CoreTexture::TextureCache.contains(id)) continue;
        m_assets.insert(id, DeclarativeGameWindow::instance()->resources().value(id));
    }

    m_loader = new CoreAssetLoader(m_assets.values(), this);
    connect(m_loader, SIGNAL(progress()), this, SLOT(updateProgress()));
    connect(m_loader, SIGNAL(completed()), this, SIGNAL(completed()));
    connect(m_loader, SIGNAL(error()), this, SIGNAL(error()));

//    for(var name in game.Audio.queue) {
//        this.sounds.push(name);
//    }

    if(m_assets.count() == 0 /*&& m_sounds.count() === 0*/) m_percent = 100;
}
void DeclarativeLoader::start(){
    if(m_scene) {
        foreach(QObject *child, m_stage->children()){
            if (child!=this) child->deleteLater();
        }

        m_stage->setBackgroundColor(m_backgroundColor);

        m_stage->setInteractive(false);
    }
    //if(game.audio) game.audio.stopAll();

    emit initStage();

    m_startTime = QDateTime::currentMSecsSinceEpoch();

    m_loader->load();

    //else this.loadAudio();
    if(!DeclarativeGameWindow::instance()->scene()) m_loopId = DeclarativeGameWindow::instance()->setGameLoop(this, SLOT(run()));
    else DeclarativeGameWindow::instance()->setScene(this);

}

void DeclarativeLoader::componentComplete(){
    QQuickItem::componentComplete();
    QVariant itemParent = QQmlProperty::read(this, "parent");
    QQmlProperty::write(this, "anchors.fill", itemParent);
}


QColor DeclarativeLoader::backgroundColor(){ return m_backgroundColor; }
void DeclarativeLoader::setBackgroundColor(const QColor color){
    if (m_backgroundColor != color){
        m_backgroundColor = color;
        DeclarativeGameWindow::instance()->system()->stage()->setBackgroundColor(m_backgroundColor);
        emit backgroundColorChanged();
    }
}

int DeclarativeLoader::percent(){ return m_percent; }

void DeclarativeLoader::run(){
    DeclarativeTimer::update();
    update();
}

void DeclarativeLoader::updateProgress(/*loader*/){
    //if(loader && loader.json && !loader.json.frames && !loader.json.bones) game.json[loader.url] = loader.json;
    m_loaded++;
    m_percent = qRound(m_loaded / ((qreal)m_assets.count() /*+ m_sounds.length*/) * 100.0);
    emit percentChanged();
}

void DeclarativeLoader::ready(){
    setScene();
}

void DeclarativeLoader::setScene() {
    if(DeclarativeGameWindow::instance()->system()->retina() || DeclarativeGameWindow::instance()->system()->hires()) {
        /*for(var i in game.TextureCache) {
            if(i.indexOf('@2x') !== -1) {
                game.TextureCache[i.replace('@2x', '')] = game.TextureCache[i];
                delete game.TextureCache[i];
            }
        }*/
    }
    DeclarativeGameWindow::instance()->resources().clear();
//    game.Audio.resources = {};
    DeclarativeTimer::s_time = 0;
    DeclarativeGameWindow::instance()->clearGameLoop(m_loopId);
    DeclarativeGameWindow::instance()->system()->setScene(m_scene);
}

void DeclarativeLoader::update(){
    if (DeclarativeGameWindow::instance()->tweenEngine()) DeclarativeGameWindow::instance()->tweenEngine()->update();

    if(m_ready) return;
    if(m_timer) {
        if(m_timer->time() >= 0) {
            m_ready = true;
            ready();
        }
    } else if(m_loaded == m_assets.count() /*+ m_sounds.count()*/) {
        // Everything loaded
        int loadTime = QDateTime::currentMSecsSinceEpoch() - m_startTime;
        int waitTime = qMax(100, DeclarativeLoader::s_timeout - loadTime);
        m_timer = new DeclarativeTimer(waitTime, this);
    }
}

/*
    loadAudio: function() {
        for (var i = this.sounds.length - 1; i >= 0; i--) {
            game.audio.load(this.sounds[i], this.progress.bind(this));
        }
    },

*/

QUrl DeclarativeLoader::getPath(QUrl path){
    return (DeclarativeGameWindow::instance()->system()->retina() || DeclarativeGameWindow::instance()->system()->hires() ? QUrl(path.toLocalFile().replace(QRegExp("\\.(?=[^.]*$)"), "@2x.")) : path);
}
