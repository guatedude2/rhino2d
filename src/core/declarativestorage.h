#ifndef DECLARATIVESTORAGE_H
#define DECLARATIVESTORAGE_H

#include <QObject>
#include <QCoreApplication>
#include <QDir>
#include <QSettings>

class DeclarativeStorage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
public:
    explicit DeclarativeStorage(QObject *parent = 0);
    DeclarativeStorage(QString id, QObject *parent = 0);

    QString id() const;
    void setId(const QString id);

    Q_INVOKABLE QVariant get(const QString id, const QVariant &defaultValue = QVariant()) const;
    Q_INVOKABLE void remove(const QString id);
    Q_INVOKABLE void reset();
    Q_INVOKABLE void set(const QString id, const QVariant &value);

signals:
    void idChanged();

private:
    QString m_storageId;
    QSettings *m_settings;

};

#endif // DECLARATIVESTORAGE_H
