#include "declarativestage.h"

DeclarativeStage::DeclarativeStage(QQuickItem *parent) :
    QQuickItem(parent),
    m_textureChanged(true),
    m_backgroundColor(Qt::transparent)
{
    QVariant itemParent = QQmlProperty::read(this, "parent");
    QQmlProperty::write(this, "anchors.centerIn", itemParent);
    setFlag(QQuickItem::ItemHasContents);
}

QColor DeclarativeStage::backgroundColor(){ return m_backgroundColor; }
void DeclarativeStage::setBackgroundColor(const QColor color){
    if (m_backgroundColor != color){
        m_backgroundColor = color;
        m_textureChanged = true;
        update();
    }
}

bool DeclarativeStage::interactive(){ return m_interactive; }
void DeclarativeStage::setInteractive(bool interactive){
    if (interactive != m_interactive){
        m_interactive = interactive;
        emit interactiveChanged();
    }
}

QSGNode *DeclarativeStage::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData){
    Q_UNUSED(updatePaintNodeData);
    QSGSimpleRectNode *textureNode = static_cast<QSGSimpleRectNode *>(oldNode);
    if (!textureNode) {
        textureNode = new QSGSimpleRectNode();
    }
    if (m_textureChanged){
        textureNode->setColor(m_backgroundColor);
    }
    textureNode->setRect(boundingRect());
    return textureNode;
}
