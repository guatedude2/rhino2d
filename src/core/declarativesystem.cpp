#include "declarativesystem.h"
#include "declarativegamewindow.h"
#include "../drawable/declarativescene.h"

#include <QGuiApplication>
#include <QDesktopServices>
#include <QMessageBox>
#include <QScreen>

DeclarativeSystem::DeclarativeSystem(QObject *parent) :
    QObject(parent),
    m_platform(Unknown),
    m_stage(0),
    m_timer(0),
    m_scene(0),
    m_newSceneComponent(0),
    m_width(0),
    m_height(0),
    m_aspect(0),
    m_delta(0),
    m_paused(false),
    m_hires(false),
    m_retina(false),
    m_running(false),
    m_hiresFactor(1.5),
    m_resize(true),
    m_aspectFormat(AspectCrop),
    m_pauseOnHide(true)
{    
}

DeclarativeStage *DeclarativeSystem::stage(){ return m_stage; }

DeclarativeSystem::Aspect DeclarativeSystem::aspect() const{ return m_aspectFormat; }
void DeclarativeSystem::setAspect(const DeclarativeSystem::Aspect aspect){
    if (aspect != m_aspectFormat){
        m_aspectFormat = aspect;
        if (m_running) handleResize();
        emit aspectChanged();
    }
}

qreal DeclarativeSystem::width() const{ return m_width; }
void DeclarativeSystem::setWidth(const qreal width){
    if (width != m_width){
        m_width = width;
        handleResize();
        emit widthChanged();
    }
}

qreal DeclarativeSystem::height() const{ return m_height; }
void DeclarativeSystem::setHeight(const qreal height){
    if (height != m_height){
        m_height = height;
        handleResize();
        emit heightChanged();
    }
}

qreal DeclarativeSystem::delta(){ return m_delta; }

bool DeclarativeSystem::paused() const{ return m_paused; }
void DeclarativeSystem::setPaused(const bool paused){
    if (paused){
        resume();
    }else{
        pause();
    }
}

bool DeclarativeSystem::hires() const{ return m_hires; }
void DeclarativeSystem::setHires(const bool hires){
    if (hires != m_hires){
        m_hires = hires;
        emit hiresChanged();
    }
}

bool DeclarativeSystem::retina() const{ return m_retina; }
void DeclarativeSystem::setRetina(const bool retina){
    if (retina != m_retina){
        m_retina = retina;
        emit retinaChanged();
    }
}

bool DeclarativeSystem::running() const{ return m_running; }

DeclarativeSystem::Platform DeclarativeSystem::platform() const{ return m_platform; }
void DeclarativeSystem::setPlatform(const Platform platform){
    //if (m_platform==Unknown && !m_pauseOnHideSet) m_pauseOnHide = ((int)platform>=10);
    m_platform = platform;    
    emit platformChanged();
}

bool DeclarativeSystem::pauseOnHide() const{ return m_pauseOnHide; }
void DeclarativeSystem::setPauseOnHide(const bool paused){
    if (m_pauseOnHide != paused){
        m_pauseOnHide = paused;
        m_pauseOnHideSet = true;
        emit pauseOnHideChanged();
    }
}

qreal DeclarativeSystem::screenWidth() const{ return QGuiApplication::screens().first()->availableSize().width();}
qreal DeclarativeSystem::screenHeight() const{ return QGuiApplication::screens().first()->availableSize().height();}

void DeclarativeSystem::init(qreal width, qreal height){
     m_width = width;
     m_height = height;
     m_aspect = m_width / m_height;
     bool hires(m_hires);
     bool retina(m_retina);
     bool scale(1);
     m_hires = false;
     m_retina = false;

     if(hires && DeclarativeGameWindow::instance()->width() >= width * m_hiresFactor && DeclarativeGameWindow::instance()->height() >= height * m_hiresFactor) {
         m_hires = true;
     }

     if(retina && DeclarativeGameWindow::instance()->screen()->logicalDotsPerInch() == 2) {
         m_retina = true;
     }
     if(m_hires || m_retina) {
         m_width *= 2;
         m_height *= 2;
         scale = 2;
     }

     m_timer = new DeclarativeTimer(this);

     if (m_stage!=0) m_stage->deleteLater();
     m_stage = new DeclarativeStage(DeclarativeGameWindow::instance()->contentItem());
     if (m_retina){
         m_stage->setWidth(m_width / 2);
         m_stage->setHeight(m_height / 2);
     }else{
         m_stage->setWidth(m_width);
         m_stage->setHeight(m_height);
     }
     m_stage->setParent(DeclarativeGameWindow::instance()->contentItem());
     m_stage->setParentItem(DeclarativeGameWindow::instance()->contentItem());

     //TODO: set fullscreen depending on device default

     //TODO: load vibration module

     //TODO: load accelerometer module

    initResize();
}

bool DeclarativeSystem::vibrate(const qreal time) {
    qDebug() << "System::vibrate() NYI";
    //if(navigator.vibrate) return navigator.vibrate(time);
    //TODO: NATIVE SUPPORT
    return false;
}

int DeclarativeSystem::messageBox(const QString title, const QString message, const Buttons buttons, const Button defaultButton, Icon icon){
    qDebug() << "System::messageBox() NYI";
    //TODO: NATIVE SUPPORT
    return 0;
}

QString DeclarativeSystem::inputBox(const QString title, const QString message, const QString placeholder, const QJSValue &callback){
    qDebug() << "System::inputBox() NYI";
    //TODO: NATIVE SUPPORT
}

bool DeclarativeSystem::openUrl(const QUrl url){
    return QDesktopServices::openUrl(url);
}

bool DeclarativeSystem::sendEmail(const QString to, const QString subject, const QString message){
    QRegExp emailValidator("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", Qt::CaseInsensitive);
    if (emailValidator.indexIn(to)>=0){
        QUrl mailto(QString("mailto:%1?subject=%2&body=%3").arg(to).arg(subject).arg(message));
        return QDesktopServices::openUrl(mailto);
    }
    return false;
}

void DeclarativeSystem::pause() {
    if(m_paused) return;
    m_paused = true;
    if (m_scene && m_scene->inherits("DeclarativeScene")) static_cast<DeclarativeScene *>(m_scene)->pause();
    emit pausedChanged();
}

void DeclarativeSystem::resume() {
    if(!m_paused) return;
    m_paused = false;
    if (m_scene && m_scene->inherits("DeclarativeScene")) static_cast<DeclarativeScene *>(m_scene)->resume();
    DeclarativeGameWindow::instance()->update();
    emit pausedChanged();
}

void DeclarativeSystem::setScene(QQmlComponent *sceneComponent) {
    if (m_running) m_newSceneComponent = sceneComponent;
    else setSceneNow(sceneComponent);
}

void DeclarativeSystem::setSceneNow(QQmlComponent *sceneComponent) {
    if (m_scene) m_scene->deleteLater();
    if (DeclarativeGameWindow::instance()->tweenEngine()) DeclarativeGameWindow::instance()->tweenEngine()->removeAll();
    m_scene = static_cast<QQuickItem *>(sceneComponent->beginCreate(QQmlEngine::contextForObject(DeclarativeGameWindow::instance())));
    m_scene->setParent(m_stage);
    m_scene->setParentItem(m_stage);
    sceneComponent->completeCreate();
    startRunLoop();
}

void DeclarativeSystem::startRunLoop() {
    if(m_gameLoopId) stopRunLoop();
    m_gameLoopId = DeclarativeGameWindow::instance()->setGameLoop(this, SLOT(run()));
    m_running = true;
}

void DeclarativeSystem::stopRunLoop() {
    DeclarativeGameWindow::instance()->clearGameLoop(m_gameLoopId);
    m_running = false;
}

void DeclarativeSystem::run() {
    if (DeclarativeGameWindow::instance()) DeclarativeGameWindow::instance()->audio()->updateStates();
    if(m_paused) return;

    DeclarativeTimer::update();
    m_delta = m_timer->delta() / 1000.0f;
    emit deltaChanged();

    if (m_scene && m_scene->inherits("DeclarativeScene")) static_cast<DeclarativeScene *>(m_scene)->run();

    if (m_newSceneComponent) {
        setSceneNow(m_newSceneComponent);
        m_newSceneComponent = 0;
    }
}

void DeclarativeSystem::initResize() {
    //m_ratio = m_orientation === OrientationLandscape ? m_width / m_height : m_height / m_width;

    //if (game.System.center) this.canvas.style.margin = 'auto';

//    if(game.device.mobile) {
//        // Mobile position
//        if(!game.System.center) {
//            this.canvas.style.position = 'absolute';
//            this.canvas.style.left = game.System.left + 'px';
//            this.canvas.style.top = game.System.top + 'px';
//        }

//        document.addEventListener('touchstart', function(e) { e.preventDefault(); }, false);

//        var div = document.createElement('div');
//        div.innerHTML = game.System.rotateImg ? '' : game.System.rotateMsg;
//        div.id = 'ig_rotateMsg';
//        div.style.position = 'absolute';
//        div.style.height = '12px';
//        div.style.textAlign = 'center';
//        div.style.left = 0;
//        div.style.right = 0;
//        div.style.top = 0;
//        div.style.bottom = 0;
//        div.style.margin = 'auto';
//        div.style.display = 'none';
//        game.System.rotateDiv = div;
//        document.body.appendChild(game.System.rotateDiv);

//        if(game.System.rotateImg) {
//            var img = new Image();
//            var me = this;
//            img.onload = function(e) {
//                div.image = e.target;
//                div.appendChild(e.target);
//                div.style.height = e.target.height+'px';
//                me.resizeRotateImage();
//            };
//            img.src = game.System.rotateImg;
//            img.style.position = 'relative';
//        }
//    } else {
        // Desktop resize
//        if(game.System.resize) {
//            var minWidth = game.System.minWidth === 'auto' ? this.retina ? this.width / 4 : this.width / 2 : game.System.minWidth;
//            var minHeight = game.System.minHeight === 'auto' ? this.retina ? this.height / 4 : this.height / 2 : game.System.minHeight;
//            var maxWidth = game.System.maxWidth === 'auto' ? this.retina ? this.width / 2 : this.width : game.System.maxWidth;
//            var maxHeight = game.System.maxHeight === 'auto' ? this.retina ? this.height / 2 : this.height : game.System.maxHeight;
//            if(game.System.minWidth) this.canvas.style.minWidth = minWidth + 'px';
//            if(game.System.minHeight) this.canvas.style.minHeight = minHeight + 'px';
//            if(game.System.maxWidth) this.canvas.style.maxWidth = maxWidth + 'px';
//            if(game.System.maxHeight) this.canvas.style.maxHeight = maxHeight + 'px';
//        }
    //}

    connect(DeclarativeGameWindow::instance(), SIGNAL(heightChanged(int)), this, SLOT(handleResize()));
    connect(DeclarativeGameWindow::instance(), SIGNAL(widthChanged(int)), this, SLOT(handleResize()));

    handleResize();
}

void DeclarativeSystem::checkOrientation() {
//    QSize windowSize(DeclarativeGameWindow::instance()->size());
//    this.orientation = windowSize.width() < windowSize.height() ? game.System.PORTRAIT : game.System.LANDSCAPE;
//    if(game.device.android2 && windowSize.width() === 320 && windowSize.height() === 251) {
//        // Android 2.3 portrait fix
//        this.orientation = game.System.PORTRAIT;
//    }
//    game.System.rotateScreen = game.System.orientation !== this.orientation ? true : false;

//    this.canvas.style.display = game.System.rotateScreen ? 'none' : 'block';
//    game.System.rotateDiv.style.display = game.System.rotateScreen ? 'block' : 'none';

//    if(game.System.rotateScreen && game.System.backgroundColor.rotate) document.body.style.backgroundColor = game.System.backgroundColor.rotate;
//    if(!game.System.rotateScreen && game.System.backgroundColor.game) document.body.style.backgroundColor = game.System.backgroundColor.game;

//    if(game.System.rotateScreen) document.body.style.backgroundImage = game.System.backgroundImage.rotate ? 'url(' + game.System.backgroundImage.rotate + ')' : 'none';
//    if(!game.System.rotateScreen) document.body.style.backgroundImage = game.System.backgroundImage.game ? 'url(' + game.System.backgroundImage.game + ')' : 'none';

//    if(game.System.rotateScreen && game.system && typeof(game.system.pause) === 'function') game.system.pause();
//    if(!game.System.rotateScreen && game.system && typeof(game.system.resume) === 'function') game.system.resume();

//    if(game.System.rotateScreen) this.resizeRotateImage();
}

void DeclarativeSystem::handleResize() {
    // Mobile orientation
    //if(game.device.mobile) checkOrientation();

    if (!m_resize) return;

    qreal scale(1);
    QSize windowSize(DeclarativeGameWindow::instance()->size());
    qreal stageWidth(windowSize.width()), stageHeight(windowSize.height());
    if (m_aspectFormat == AspectFit) {
        qreal w = windowSize.width();
        qreal widthScale = w / qreal(m_width);
        qreal h = windowSize.height();
        qreal heightScale = h / qreal(m_height);
        if (widthScale <= heightScale) {
            stageWidth = w;
            stageHeight = widthScale * qreal(m_height);
        } else if (heightScale < widthScale) {
            stageWidth = heightScale * qreal(m_width);
            stageHeight = h;
        }
    } else if (m_aspectFormat == AspectCrop) {
        if (!m_width || !m_height)
            return;
        qreal widthScale = windowSize.width() / qreal(m_width);
        qreal heightScale = windowSize.height() / qreal(m_height);
        if (widthScale < heightScale) {
            widthScale = heightScale;
        } else if (heightScale < widthScale) {
            heightScale = widthScale;
        }

        stageHeight = heightScale * qreal(m_height);
        stageWidth = widthScale * qreal(m_width);
    }
    scale = (stageWidth/stageHeight > m_aspect ? stageWidth/m_width : stageHeight/m_height);
    m_stage->setScale(scale);
}
