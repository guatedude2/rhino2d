#ifndef DECLARATIVEKEYBOARD_H
#define DECLARATIVEKEYBOARD_H

#include "qdeclarativeevents_p_p.h"
#include <QObject>

class DeclarativeKeyboard : public QObject
{
    Q_OBJECT
    Q_ENUMS(Qt::Keys)
public:
    explicit DeclarativeKeyboard(QObject *parent = 0);

    Q_INVOKABLE bool isKeyPressed(Qt::Key key);

    void handleKeyUp(QDeclarativeKeyEvent *event);
    void handleKeyDown(QDeclarativeKeyEvent *event);

signals:
    void keyUp(QDeclarativeKeyEvent *event);
    void keyDown(QDeclarativeKeyEvent *event);

private:
    QList<int> m_pressedKeys;
};

#endif // DECLARATIVEKEYBOARD_H
