#ifndef DECLARATIVELOADER_H
#define DECLARATIVELOADER_H

#include "declarativestage.h"
#include "declarativetimer.h"
#include "declarativecore.h"

#include <QQuickItem>
#include <QPainter>

class DeclarativeLoader : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(int percent READ percent NOTIFY percentChanged)
public:
    explicit DeclarativeLoader(QQuickItem *parent = 0);

    void init(QQmlComponent *scene);
    void start();

    void componentComplete();

    QColor backgroundColor();
    void setBackgroundColor(const QColor color);

    int percent();

    static QUrl getPath(QUrl path);
Q_SIGNALS:
    void progress();
    void completed();
    void error();
    void initStage();
    void backgroundColorChanged();
    void percentChanged();

public slots:
    void run();
    void updateProgress();

private:
    int m_startTime;
    int m_assetLoaderTimerId;
    QMetaObject::Connection m_loopId;
    QQmlComponent *m_scene;
    DeclarativeStage *m_stage;
    DeclarativeTimer *m_timer;
    QMap<QString, QUrl> m_assets;
    QColor m_backgroundColor;
    CoreAssetLoader *m_loader;
    int m_percent;
    int m_loaded;
    int m_currentAsset;
    bool m_ready;

    static int s_timeout;

    void ready();
    void setScene();
    void update();
};

#endif // DECLARATIVELOADER_H
