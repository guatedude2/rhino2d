/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QDECLARATIVEEVENTS_P_H
#define QDECLARATIVEEVENTS_P_H

#include <QtCore/qobject.h>
#include <QtGui/qevent.h>

class QDeclarativeKeyEvent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int key READ key)
    Q_PROPERTY(QString text READ text)
    Q_PROPERTY(int modifiers READ modifiers)
    Q_PROPERTY(bool isAutoRepeat READ isAutoRepeat)
    Q_PROPERTY(int count READ count)
    Q_PROPERTY(bool accepted READ isAccepted WRITE setAccepted)

public:
    QDeclarativeKeyEvent(QEvent::Type type, int key, Qt::KeyboardModifiers modifiers, const QString &text=QString(), bool autorep=false, ushort count=1)
        : event(type, key, modifiers, text, autorep, count) { event.setAccepted(false); }
    QDeclarativeKeyEvent(const QKeyEvent &ke)
        : event(ke) { event.setAccepted(false); }

    int key() const { return event.key(); }
    QString text() const { return event.text(); }
    int modifiers() const { return event.modifiers(); }
    bool isAutoRepeat() const { return event.isAutoRepeat(); }
    int count() const { return event.count(); }

    bool isAccepted() { return event.isAccepted(); }
    void setAccepted(bool accepted) { event.setAccepted(accepted); }

private:
    QKeyEvent event;
};

class QDeclarativeMouseEvent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int x READ x)
    Q_PROPERTY(int y READ y)
    Q_PROPERTY(QPointF scenePos READ scenePos)
    Q_PROPERTY(int button READ button)
    Q_PROPERTY(int buttons READ buttons)
    Q_PROPERTY(int modifiers READ modifiers)
    Q_PROPERTY(bool wasHeld READ wasHeld)
    Q_PROPERTY(bool isClick READ isClick)
    Q_PROPERTY(bool accepted READ isAccepted WRITE setAccepted)

public:
    QDeclarativeMouseEvent(QPointF pos, QPointF scenePos, Qt::MouseButton button, Qt::MouseButtons buttons, Qt::KeyboardModifiers modifiers
                  , bool isClick=false, bool wasHeld=false)
        : _x(pos.x()), _y(pos.y()), _scenePos(scenePos), _button(button), _buttons(buttons), _modifiers(modifiers)
          , _wasHeld(wasHeld), _isClick(isClick), _accepted(true) {}

    int x() const { return _x; }
    int y() const { return _y; }
    QPointF scenePos() const { return _scenePos; }
    int button() const { return _button; }
    int buttons() const { return _buttons; }
    int modifiers() const { return _modifiers; }
    bool wasHeld() const { return _wasHeld; }
    bool isClick() const { return _isClick; }

    // only for internal usage
    void setX(int x) { _x = x; }
    void setY(int y) { _y = y; }

    bool isAccepted() { return _accepted; }
    void setAccepted(bool accepted) { _accepted = accepted; }

private:
    int _x;
    int _y;
    QPointF _scenePos;
    Qt::MouseButton _button;
    Qt::MouseButtons _buttons;
    Qt::KeyboardModifiers _modifiers;
    bool _wasHeld;
    bool _isClick;
    bool _accepted;
};

class QDeclarativeTouchPoint : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int pointId READ pointId NOTIFY pointIdChanged)
    Q_PROPERTY(bool pressed READ pressed NOTIFY pressedChanged)
    Q_PROPERTY(qreal x READ x NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y NOTIFY yChanged)
    Q_PROPERTY(qreal pressure READ pressure NOTIFY pressureChanged)
    Q_PROPERTY(QVector2D velocity READ velocity NOTIFY velocityChanged)
    Q_PROPERTY(QRectF area READ area NOTIFY areaChanged)

    Q_PROPERTY(qreal startX READ startX NOTIFY startXChanged)
    Q_PROPERTY(qreal startY READ startY NOTIFY startYChanged)
    Q_PROPERTY(qreal previousX READ previousX NOTIFY previousXChanged)
    Q_PROPERTY(qreal previousY READ previousY NOTIFY previousYChanged)
    Q_PROPERTY(qreal sceneX READ sceneX NOTIFY sceneXChanged)
    Q_PROPERTY(qreal sceneY READ sceneY NOTIFY sceneYChanged)

public:
    QDeclarativeTouchPoint(bool qmlDefined = true)
        : _id(0),
          _x(0.0), _y(0.0),
          _pressure(0.0),
          _qmlDefined(qmlDefined),
          _inUse(false),
          _pressed(false),
          _previousX(0.0), _previousY(0.0),
          _sceneX(0.0), _sceneY(0.0)
    {}

    int pointId() const { return _id; }
    void setPointId(int id){ if (id != _id){ _id = id; emit pointIdChanged(); } }

    qreal x() const { return _x; }
    void setX(qreal x){ if (x != _x){ _x = x; emit xChanged(); } }

    qreal y() const { return _y; }
    void setY(qreal y){ if (y != _y){ _y = y; emit yChanged(); } }

    qreal pressure() const { return _pressure; }
    void setPressure(qreal pressure){ if (pressure != _pressure){ _pressure = pressure; emit pressureChanged(); } }

    QVector2D velocity() const { return _velocity; }
    void setVelocity(const QVector2D &velocity){ if (velocity != _velocity){ _velocity = velocity; emit velocityChanged(); } }

    QRectF area() const { return _area; }
    void setArea(const QRectF &area){ if (area != _area){ _area = area; emit areaChanged(); } }

    bool isQmlDefined() const { return _qmlDefined; }

    bool inUse() const { return _inUse; }
    void setInUse(bool inUse) { _inUse = inUse; }

    bool pressed() const { return _pressed; }
    void setPressed(bool pressed){ if (pressed != _pressed){ _pressed = pressed; emit pressedChanged(); } }

    qreal startX() const { return _startX; }
    void setStartX(qreal startX){ if (startX != _startX){ _startX = startX; emit startXChanged(); } }

    qreal startY() const { return _startY; }
    void setStartY(qreal startY){ if (startY != _startY){ _startY = startY; emit startYChanged(); } }

    qreal previousX() const { return _previousX; }
    void setPreviousX(qreal previousX){ if (previousX != _previousX){ _previousX = previousX; emit previousXChanged(); } }

    qreal previousY() const { return _previousY; }
    void setPreviousY(qreal previousY){ if (previousY != _previousY){ _previousY = previousY; emit previousYChanged(); } }

    qreal sceneX() const { return _sceneX; }
    void setSceneX(qreal sceneX){ if (sceneX != _sceneX){ _sceneX = sceneX; emit sceneXChanged(); } }

    qreal sceneY() const { return _sceneY; }
    void setSceneY(qreal sceneY){ if (sceneY != _sceneY){ _sceneY = sceneY; emit sceneYChanged(); } }

Q_SIGNALS:
    void pressedChanged();
    void pointIdChanged();
    void xChanged();
    void yChanged();
    void pressureChanged();
    void velocityChanged();
    void areaChanged();
    void startXChanged();
    void startYChanged();
    void previousXChanged();
    void previousYChanged();
    void sceneXChanged();
    void sceneYChanged();

private:
    int _id;
    qreal _x;
    qreal _y;
    qreal _pressure;
    QVector2D _velocity;
    QRectF _area;
    bool _qmlDefined;
    bool _inUse;    //whether the point is currently in use (only valid when _qmlDefined == true)
    bool _pressed;
    qreal _startX;
    qreal _startY;
    qreal _previousX;
    qreal _previousY;
    qreal _sceneX;
    qreal _sceneY;
};

#endif // QDECLARATIVEEVENTS_P_H
