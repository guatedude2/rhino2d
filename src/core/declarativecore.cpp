#include "declarativegamewindow.h"
#include "declarativecore.h"
#include "../drawable/declarativescene.h"
#include "../drawable/declarativebitmaptext.h"

#include <QQmlInfo>
#include <QDebug>

/** CoreShape class **/
CoreShape::CoreShape(QObject *parent) :
    QObject(parent)
{
}


/** CoreShapeRectangle class **/
CoreShapeRectangle::CoreShapeRectangle(QObject *parent) :
    CoreShape(parent), m_x(0), m_y(0), m_width(0), m_height(0)
{
}
CoreShapeRectangle::CoreShapeRectangle(qreal x, qreal y, qreal width, qreal height, QObject *parent) :
    CoreShape(parent), m_x(x), m_y(y), m_width(width), m_height(height)
{
}
CoreShapeRectangle::CoreShapeRectangle(QRectF rect, QObject *parent) :
    CoreShape(parent)
{
    CoreShapeRectangle(rect.x(), rect.y(), rect.width(), rect.height());
}
qreal CoreShapeRectangle::x(){ return m_x; }
void CoreShapeRectangle::setX(qreal x){
    SETPROP(x, m_x, xChanged)
}
qreal CoreShapeRectangle::y(){ return m_y; }
void CoreShapeRectangle::setY(qreal y){
    SETPROP(y, m_y, yChanged)
}
qreal CoreShapeRectangle::width(){ return m_width; }
void CoreShapeRectangle::setWidth(qreal width){
    SETPROP(width, m_width, widthChanged)
}
qreal CoreShapeRectangle::height(){ return m_height; }
void CoreShapeRectangle::setHeight(qreal height){
    SETPROP(height, m_height, heightChanged)
}
bool CoreShapeRectangle::contains(qreal x, qreal y){
    if(m_width <= 0 || m_height <= 0) return false;
    qreal x1 = m_x;
    if(x >= x1 && x <= x1 + m_width){
        qreal y1 = m_y;
        if(y >= y1 && y <= y1 + m_height) return true;
    }
    return false;
}
bool CoreShapeRectangle::contains (QPoint p){
    return contains(p.x(), p.y());
}
QRectF CoreShapeRectangle::toRect(){
    return QRectF(m_x, m_y, m_width, m_height);
}


/** CoreShapePolygon class **/
CoreShapePolygon::CoreShapePolygon(QObject *parent) :
    CoreShape(parent)
{
}
CoreShapePolygon::CoreShapePolygon(QPoint point, QObject *parent) :
    CoreShape(parent), m_points(QList<QPoint>() << point)
{
}
CoreShapePolygon::CoreShapePolygon(QList<QPoint> points, QObject *parent) :
    CoreShape(parent), m_points(points)
{
}
CoreShapePolygon::CoreShapePolygon(QList<qreal> points, QObject *parent) :
    CoreShape(parent)
{
    for(int i = 0, il = points.count(); i < il; i+=2) {
        m_points.append(QPoint(points[i], points[i + 1]));
    }
}
QList<QPoint> CoreShapePolygon::points(){ return m_points; }
void CoreShapePolygon::setPoints(QList<QPoint> points){
    m_points = points;
    emit pointsChanged();
}

bool CoreShapePolygon::contains(qreal x, qreal y){
    bool inside = false;
    // use some raycasting to test hits
    // https://github.com/substack/point-in-polygon/blob/master/index.js
    for(int i = 0, j = m_points.count() - 1; i < m_points.count(); j = i++) {
        qreal xi = m_points[i].x(), yi = m_points[i].y(), xj = m_points[j].x(), yj = m_points[j].y();
        bool intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if(intersect) inside = !inside;
    }
    return inside;
}


/** CoreShapeCircle class **/
CoreShapeCircle::CoreShapeCircle(QObject *parent) :
    CoreShape(parent), m_x(0), m_y(0), m_radius(0)
{
}
CoreShapeCircle::CoreShapeCircle(qreal x, qreal y, qreal radius, QObject *parent) :
    CoreShape(parent), m_x(x), m_y(y), m_radius(radius)
{
}
qreal CoreShapeCircle::x(){ return m_x; }
void CoreShapeCircle::setX(qreal x){
    SETPROP(x, m_x, xChanged)
}
qreal CoreShapeCircle::y(){ return m_y; }
void CoreShapeCircle::setY(qreal y){
    SETPROP(y, m_y, yChanged)
}
qreal CoreShapeCircle::radius(){ return m_radius; }
void CoreShapeCircle::setRadius(qreal radius){
    SETPROP(radius, m_radius, radiusChanged)
}

bool CoreShapeCircle::contains(qreal x, qreal y){
    if(m_radius <= 0) return false;
    qreal dx = (m_x - x), dy = (m_y - y);
    qreal r2 = m_radius * m_radius;
    dx *= dx; dy *= dy;
    return (dx + dy <= r2);
}
bool CoreShapeCircle::contains(QPoint p){
    return contains(p.x(), p.y());
}


/** CoreShapeElipse class **/
CoreShapeElipse::CoreShapeElipse(QObject *parent) :
    CoreShapeRectangle(parent)
{
}
CoreShapeElipse::CoreShapeElipse(qreal x, qreal y, qreal width, qreal height, QObject *parent) :
    CoreShapeRectangle(x,y,width,height, parent)
{
}
CoreShapeElipse::CoreShapeElipse(QRect rect) :
    CoreShapeRectangle(rect)
{
}
bool CoreShapeElipse::contains(qreal x, qreal y){
    if(width() <= 0 || height() <= 0) return false;
    //normalize the coords to an ellipse with center 0,0
    qreal normx = ((x - this->x()) / this->width()), normy = ((y - this->y()) / this->height());
    normx *= normx;
    normy *= normy;
    return (normx + normy <= 1);
}
bool CoreShapeElipse::contains(QPoint p){
    return contains(p.x(), p.y());
}

CoreShapeRectangle *CoreShapeElipse::getBounds(){
    return new CoreShapeRectangle(x(), y(), width(), height());
}



/** CoreVector class **/
CoreVector::CoreVector(QObject *parent) :
    QObject(parent), m_x(0), m_y(0)
{
}

CoreVector::CoreVector(qreal x, qreal y, QObject *parent) :
    QObject(parent), m_x(x), m_y(y)
{
}

qreal CoreVector::x(){ return m_x; }
void CoreVector::setX(qreal x){
    if (x != m_x){
        m_x = x;
        emit xChanged();
    }
}

qreal CoreVector::y(){ return m_y; }
void CoreVector::setY(qreal y){
    if (y != m_y){
        m_y = y;
        emit yChanged();
    }
}


void CoreVector::set(qreal x, qreal y) {
    setX(x);
    setY(y);
}

CoreVector *CoreVector::clone() {
    return new CoreVector(m_x, m_y);
}

void CoreVector::copy(CoreVector *v) {
    setX(v->x());
    setY(v->y());
}

void CoreVector::add(qreal x, qreal y) {
    setX(this->x() + x);
    setY(this->y() + (y!=std::numeric_limits<qreal>::max() ? y : x));
}

void CoreVector::add(CoreVector *v) {
    setX(this->x() + v->x());
    setY(this->y() + v->y());
}

void CoreVector::subtract(qreal x, qreal y) {
    setX(this->x() - x);
    setY(this->y() - (y!=std::numeric_limits<qreal>::max() ? y : x));
}

void CoreVector::subtract(CoreVector *v) {
    setX(this->x() - v->x());
    setY(this->y() - v->y());
}

void CoreVector::multiply(qreal x, qreal y) {
    setX(this->x() * x);
    setY(this->y() * (y!=std::numeric_limits<qreal>::max() ? y : x));
}

void CoreVector::multiply(CoreVector *v) {
    setX(this->x() * v->x());
    setY(this->y() * v->y());
}

void CoreVector::multiplyAdd(qreal x, qreal y) {
    setX(this->x() + x*y);
    setY(this->x() + x*y);
}

void CoreVector::multiplyAdd(CoreVector *v,  qreal y) {
    setX(this->x() + v->x() * y);
    setY(this->y() + v->y() * y);
}

void CoreVector::divide(qreal x, qreal y) {
    setX(this->x() / x);
    setY(this->y() / (y!=std::numeric_limits<qreal>::max() ? y : x));
}

void CoreVector::divide(CoreVector *v) {
    setX(this->x() / v->x());
    setY(this->y() / v->y());
}

qreal CoreVector::distance(CoreVector *v) {
    qreal x = v->x() - this->x();
    qreal y = v->y() - this->y();
    return qSqrt(x * x + y * y);
}

qreal CoreVector::length() {
    return qSqrt(dot());
}

qreal CoreVector::dot(CoreVector *v) {
    if (v) return this->x() * v->x() + this->y() * v->y();
    else return this->x() * this->x() + this->y() * this->y();
}

qreal CoreVector::dotNormalized(CoreVector *v) {
    qreal len1 = length();
    qreal x1 = x() / len1;
    qreal y1 = y() / len1;

    if (v) {
        qreal len2 = v->length();
        qreal x2 = v->x() / len2;
        qreal y2 = v->y() / len2;
        return x1 * x2 + y1 * y2;
    } else return x1 * x1 + y1 * y1;
}

void CoreVector::rotate(qreal angle) {
    qreal c = qCos(angle);
    qreal s = qSin(angle);
    qreal x = this->x() * c - this->y() * s;
    qreal y = this->y() * c + this->x() * s;
    set(x, y);
}

void CoreVector::normalize() {
    qreal len = length();
    if (!len) len = 1;
    setX(this->x() / len);
    setY(this->y() / len);
}

void CoreVector::limit(QPointF p) {
    setX(Core::limit(this->x(), -p.x(), p.x()));
    setY(Core::limit(this->y(), -p.y(), p.y()));
}

qreal CoreVector::angle(CoreVector *v) {
    return qAtan2(v->y() - this->y(), v->x() - this->x());
}

qreal CoreVector::angleFromOrigin(CoreVector *v) {
    return qAtan2(v->y(), v->x()) - qAtan2(this->y(), this->x());
}

void CoreVector::round() {
    setX(qRound(this->x()));
    setY(qRound(this->y()));
}

QPointF CoreVector::toPointF(){ return QPointF(this->x(), this->y()); }


/** CoreCoreTexture class **/
QMap<QString, CoreCoreTexture *> CoreCoreTexture::CoreTextureCache;
CoreCoreTexture::CoreCoreTexture(QUrl source, Core::ScaleMode scaleMode, QObject *parent) :
    QObject(parent),
    m_hasLoaded(false),
    m_scaleMode(scaleMode),
    m_resource(0)
{
    updateSourceImage(source);
}

CoreCoreTexture::~CoreCoreTexture(){
    CoreCoreTexture::CoreTextureCache.remove(m_imageUrl.toString());
    if (!m_resource) m_resource->deleteLater();
}

qreal CoreCoreTexture::width(){ return m_baseImage.width(); }
qreal CoreCoreTexture::height(){ return m_baseImage.height(); }
bool CoreCoreTexture::hasLoaded(){ return m_hasLoaded; }
QPixmap CoreCoreTexture::source(){ return m_baseImage; }
Core::ScaleMode CoreCoreTexture::scaleMode(){ return m_scaleMode; }

void CoreCoreTexture::updateSourceImage(QUrl source){
    QUrl sourceUrl((DeclarativeGameWindow::instance() ? DeclarativeGameWindow::instance()->config()->baseUrl() : QUrl()).resolved(source.toString()));
    m_hasLoaded = false;
    m_baseImage = QPixmap();
    if (sourceUrl.scheme()=="file"){
        m_imageUrl = sourceUrl;
        m_baseImage = QPixmap(m_imageUrl.toLocalFile());
        CoreCoreTexture::CoreTextureCache.insert(m_imageUrl.toString(), this);
        if (m_baseImage.isNull()){
            qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load image from source " << m_imageUrl.toLocalFile();
            return;
        }
        m_hasLoaded = true;
    }else if (sourceUrl.toString().mid(0, 5).toLower() == "data:") {
        QString textureString(sourceUrl.toString());
        QRegExp dataRegExp("^data:([\\w/]+);(\\w+),", Qt::CaseInsensitive);
        if (dataRegExp.indexIn(textureString.toLower())==0){
            if (dataRegExp.cap(1)=="image/png" && dataRegExp.cap(2)=="base64"){
                m_baseImage = QPixmap::fromImage(QImage::fromData(QByteArray::fromBase64(QString(textureString).remove(0, 22).toLatin1()), "png"));
                CoreCoreTexture::CoreTextureCache.insert(m_imageUrl.toString(), this);
                if (m_baseImage.isNull()){
                    qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load image from data url";
                    return;
                }
                m_hasLoaded = true;
            }else{
                qmlInfo(DeclarativeGameWindow::instance()) << "Texture data url not supported";
            }
        }
    }else if (!sourceUrl.isEmpty()){
        m_imageUrl = sourceUrl;
        CoreCoreTexture::CoreTextureCache.insert(m_imageUrl.toString(), this);
        m_resource = new CoreRemoteResource(m_imageUrl);
        QObject::connect(m_resource, SIGNAL(complete()), this, SLOT(complete()));
        QObject::connect(m_resource, SIGNAL(error(QNetworkReply::NetworkError,QString)), this, SLOT(error(QNetworkReply::NetworkError,QString)));
    }else{
        qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load texture";
    }
}

QUrl CoreCoreTexture::url(){ return m_imageUrl; }

CoreCoreTexture *CoreCoreTexture::fromImage(QUrl imageUrl, Core::ScaleMode scaleMode){
    CoreCoreTexture *baseTexture = 0;
    if(CoreCoreTexture::CoreTextureCache.contains(imageUrl.toString())){
        baseTexture = CoreCoreTexture::CoreTextureCache.value(imageUrl.toString());
    }else{
        baseTexture = new CoreCoreTexture(imageUrl, scaleMode);
        CoreCoreTexture::CoreTextureCache.insert(imageUrl.toString(), baseTexture);
    }

    return baseTexture;
}

void CoreCoreTexture::complete(){
    m_baseImage = QPixmap::fromImage(QImage::fromData(m_resource->data()));
    emit completed();
}

void CoreCoreTexture::error(QNetworkReply::NetworkError status, QString error){
    qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load texture #" << status << error;
}


/** CoreTexture class **/
QMap<QString, CoreTexture *> CoreTexture::TextureCache;
CoreTexture::CoreTexture(QObject * parent) :
    QObject(parent),
    m_baseTexture(0)
{
}

CoreTexture::CoreTexture(CoreCoreTexture *baseTexture, QRectF frame, QObject * parent) :
    QObject(parent), m_baseTexture(baseTexture), m_frame(frame), m_noFrame(false)
{
    if(frame.isEmpty() && !frame.isNull()){
        m_noFrame = true;
    }

    if (m_baseTexture->hasLoaded()){
        if (m_noFrame) frame = QRect(0,0, m_baseTexture->width(), m_baseTexture->height());
        setFrame(frame);
    }else{
        connect(m_baseTexture, SIGNAL(completed()), this, SLOT(baseTextureLoaded()));
    }
}

CoreTexture::~CoreTexture(){
    if (m_baseTexture) m_baseTexture->deleteLater();
}

CoreCoreTexture *CoreTexture::baseTexture(){ return m_baseTexture; }

QRectF CoreTexture::frame(){ return m_frame; }
void CoreTexture::setFrame(QRectF frame){
        m_frame = frame;

        if(frame.x() + frame.width() > m_baseTexture->width() || frame.y() + frame.height() > m_baseTexture->height()){
            qmlInfo(DeclarativeGameWindow::instance()) << "Texture Error: frame does not fit inside the base Texture dimensions";
            return;
        }

        //m_updateFrame = true;

        //CoreTexture::s_frameUpdates.push(this);
}

qreal CoreTexture::width(){ return m_frame.width(); }

qreal CoreTexture::height(){ return m_frame.height(); }

CoreTexture *CoreTexture::fromImage(QUrl source, Core::ScaleMode scaleMode){
    CoreTexture *texture = 0;

    if (CoreTexture::TextureCache.contains(source.toString())){
        texture = CoreTexture::TextureCache.value(source.toString());
    }else{
        texture = new CoreTexture(CoreCoreTexture::fromImage(source, scaleMode));
        CoreTexture::TextureCache.insert(source.toString(), texture);
    }

    return texture;
}

void CoreTexture::baseTextureLoaded(){
    QRectF frame(this->frame());
    disconnect(m_baseTexture, SIGNAL(completed()), this, SLOT(baseTextureLoaded()));
    if (m_noFrame) frame = QRectF(0,0, m_baseTexture->width(), m_baseTexture->height());
    setFrame(frame);

}


/** CoreImageLoader class **/
CoreImageLoader::CoreImageLoader(QUrl source, QObject *parent) :
    CoreLoader(parent),
    m_texture(0)
{
    m_texture = CoreTexture::fromImage(source);
}

CoreTexture *CoreImageLoader::texture(){ return m_texture; }

void CoreImageLoader::load(){
    if (!m_texture->baseTexture()->hasLoaded()){
        connect(m_texture->baseTexture(), SIGNAL(completed()), this, SIGNAL(loaded()));
    }else{
        emit loaded();
    }
}


/** CoreBitmapFontLoader class **/
CoreBitmapFontLoader::CoreBitmapFontLoader(QUrl source, QObject *parent) :
    CoreLoader(parent),
    m_resource(0),
    m_device(0),
    m_texture(0)
{
    m_source = (DeclarativeGameWindow::instance() ? DeclarativeGameWindow::instance()->config()->baseUrl() : QUrl()).resolved(source.toString());
}

CoreBitmapFontLoader::~CoreBitmapFontLoader(){
}

void CoreBitmapFontLoader::load(){
    if (m_source.scheme()=="file"){
        m_device = new QFile(m_source.toLocalFile(), this);
        xmlLoaded();
    }else if (!m_source.isEmpty()){
        m_resource = new CoreRemoteResource(m_source, this);
        QObject::connect(m_resource, SIGNAL(complete()), this, SLOT(xmlLoaded()));
        QObject::connect(m_resource, SIGNAL(error(QNetworkReply::NetworkError,QString)), this, SLOT(error(QNetworkReply::NetworkError,QString)));
    }

}

void CoreBitmapFontLoader::xmlLoaded(){
    QDomDocument *doc = 0;
    if (m_device) {
        if (m_device->open(QFile::ReadOnly)){
            doc = loadXmlDocument(m_device->readAll());
            m_device->close();
        }else{
            qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load font" << m_source.toLocalFile();
        }
    }else if (m_resource){
        doc = loadXmlDocument(m_resource->data());

    }
    if (!doc || doc->isNull()){
        qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load font" << m_source.toLocalFile();
        return;
    }

    QString fontFileName = (doc->elementsByTagName("page").count()>0 ? doc->elementsByTagName("page").at(0).toElement().attribute("file") : QString());
    CoreImageLoader *image = new CoreImageLoader(m_source.resolved(fontFileName), this);
    m_texture = image->texture();

    Core::BitmapFontSettings data;
    QDomElement info = (doc->elementsByTagName("info").count()>0 ? doc->elementsByTagName("info").at(0).toElement() : QDomElement());
    QDomElement common = (doc->elementsByTagName("common").count()>0 ? doc->elementsByTagName("common").at(0).toElement() : QDomElement());
    data.font = info.attribute("face");
    data.size = info.attribute("size").toInt();
    data.lineHeight = common.attribute("lineHeight").toInt();

    QDomNodeList letters = doc->elementsByTagName("char");

    for (int i = 0; i < letters.count(); i++){
        QDomElement letter = letters.at(i).toElement();
        int charCode = letter.attribute("id").toInt();
        QRect textureRect = QRect(
            letter.attribute("x").toInt(),
            letter.attribute("y").toInt(),
            letter.attribute("width").toInt(),
            letter.attribute("height").toInt()
        );

        Core::BitmapFontSettings::Char charData;
        charData.xOffset = letter.attribute("xoffset").toInt();
        charData.yOffset = letter.attribute("yoffset").toInt();
        charData.xAdvance = letter.attribute("xadvance").toInt();

        CoreTexture::TextureCache.insert(QString(charCode), new CoreTexture(m_texture->baseTexture(), textureRect));
        charData.texture = CoreTexture::TextureCache.value(QString(charCode));
        data.chars.insert(charCode, charData);

    }

    //parse kernings
    QDomNodeList kernings = doc->elementsByTagName("kerning");
    for (int i = 0; i < kernings.count(); i++){
        QDomElement kerning = kernings.at(i).toElement();
        int first = kerning.attribute("first").toInt();
        int second = kerning.attribute("second").toInt();
        int amount = kerning.attribute("amount").toInt();

        data.chars[second].kerning[first] = amount;
    }

    DeclarativeBitmapText::fonts[data.font] = data;

    connect(image, SIGNAL(loaded()), this, SLOT(imageLoaded()));
    image->load();
}

void CoreBitmapFontLoader::imageLoaded(){
    emit loaded();
}



void CoreBitmapFontLoader::error(QNetworkReply::NetworkError status, QString error){
    qmlInfo(DeclarativeGameWindow::instance()) << "Unable to load bitmap font #" << status << error;
}

QDomDocument *CoreBitmapFontLoader::loadXmlDocument(QByteArray text){
    int errorLine, errorColumn;
    QString errorMsg;
    QDomDocument *document = new QDomDocument;

    if (!document->setContent(text, &errorMsg, &errorLine, &errorColumn)){
        QString error(QString("Syntax error line %1, column %2: %3")
                .arg(errorLine)
                .arg(errorColumn)
                .arg(errorMsg));
        qmlInfo(DeclarativeGameWindow::instance()) << error;
        return 0;
    }

    return document;
}

/*QVariantMap CoreBitmapFontLoader::parseNodeElement(QDomElement *element){
    QVariantMap map;
    if (element->hasChildNodes()){
        QVariantMap childrenMap;
        for(QDomElement node = element->firstChildElement(); !node.isNull(); node = node.nextSiblingElement()){
            childrenMap.insertMulti(node.tagName(), parseNodeElement(&node));
        }
        map.insertMulti(element->tagName(), childrenMap);
    }else if (!element->isNull()){
        QVariantMap tagValue, attributeMap;
        tagValue.insert("_text", element->text());
        for(int i=0; i<element->attributes().count(); i++){
            QDomNode attr = element->attributes().item(i);
            attributeMap.insertMulti(attr.nodeName(), attr.nodeValue());
        }
        tagValue.insert("attributes", attributeMap);
        map.insertMulti(element->tagName(), attributeMap);
    }
    return map;
}*/


/** CoreAssetLoader class **/
CoreAssetLoader::CoreAssetLoader(QList<QUrl> assets, QObject *parent) :
    QObject(parent),
    m_assetURLs(assets)
{
}

QList<QUrl> CoreAssetLoader::assetURLs(){ return m_assetURLs; }

void CoreAssetLoader::load(){
    m_loadCount = m_assetURLs.count();

    for (int i=0; i<m_assetURLs.count(); i++){
        QUrl fileName = m_assetURLs.at(i);

        //first see if we have a data URI scheme..
        QString fileType = getDataType(fileName.toString());

        //if not, assume it's a file URI
        if (fileType.isEmpty()){
            QFileInfo info(fileName.toString());
            fileType = info.suffix();
        }

        CoreLoader *loader = 0;
        if (fileType == "bmp"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "gif"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "jpg"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "jpeg"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "png"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "pbm"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "pgm"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "ppm"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "xbm"){ loader = new CoreImageLoader(fileName, this);
        }else if (fileType == "xpm"){ loader = new CoreImageLoader(fileName, this);
        //}else if (fileType == "json"){ loader = new CoreJsonLoader(fileName, this);
        //}else if (fileType == "atlas"){ loader = new CoreAtlasLoader(fileName, this);
        //}else if (fileType == "anim"){ loader = new CoreSpineLoader(fileName, this);
        }else if (fileType == "xml"){ loader = new CoreBitmapFontLoader(fileName, this);
        }else if (fileType == "fnt"){  loader = new CoreBitmapFontLoader(fileName, this);
        }else{
            qmlInfo(DeclarativeGameWindow::instance()) << fileType + " is an unsupported file type";
        }

        connect(loader, SIGNAL(loaded()), SLOT(assetLoaded()));
        loader->load();
    }
}

void CoreAssetLoader::assetLoaded(){
    m_loadCount--;
    emit progress();

    if (!m_loadCount){
        emit completed();
    }
}

QString CoreAssetLoader::getDataType(const QString str) const{

    QString test = "data:";
    //starts with 'data:'
    QString start = str.mid(0, test.count()).toLower();
    if (start == test) {
        QString data = str.mid(test.count());

        int sepIdx = data.indexOf(",");
        if (sepIdx == -1) //malformed data URI scheme
            return QString();

        //e.g. 'image/gif;base64' => 'image/gif'
        QString info = data.mid(0, sepIdx).split(";").at(0);

        //We might need to handle some special cases here...
        //standardize text/plain to 'txt' file extension
        if (info.isEmpty() || info.toLower() == "text/plain")
            return "txt";

        //User specified mime type, try splitting it by '/'
        return info.split('/').at(1).toLower();
    }

    return QString();
}


/** CoreDisplayObject class **/
CoreDisplayObject::CoreDisplayObject(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_hitArea(new CoreShapeRectangle(0,0,1,1, this)),
    m_defaultCursor("default"),
    m_buttonMode(false),
    m_filterArea(new CoreShapeRectangle(0,0,1,1, this)),
    m_interactive(false),
    m_mask(0),
    m_scene(0),
    m_tint(Qt::transparent),
    m_blendMode(Core::BlendModeScreen)
{
    setFlag(QQuickItem::ItemHasContents, false);
    setMipmap(true);
    setRenderTarget(FramebufferObject);
}

CoreDisplayObject::~CoreDisplayObject(){    
    if (m_scene) static_cast<DeclarativeScene *>(m_scene)->removeObject(this);
}

CoreShape *CoreDisplayObject::hitArea(){ return m_hitArea; }
void CoreDisplayObject::setHitArea(CoreShape * shape){ SETPROP(m_hitArea, shape, hitAreaChanged) }
QString CoreDisplayObject::defaultCursor(){ return m_defaultCursor; }
void CoreDisplayObject::setDefaultCursor(QString cursor){ SETPROP(m_defaultCursor, cursor, defaultCursorChanged) }
bool CoreDisplayObject::buttonMode(){ return m_buttonMode; }
void CoreDisplayObject::setButtonMode(bool buttonMode){ SETPROP(m_buttonMode, buttonMode, buttonModeChanged) }
CoreShapeRectangle *CoreDisplayObject::filterArea(){ return m_filterArea; }
void CoreDisplayObject::setFilterArea(CoreShapeRectangle * rect){ SETPROP(m_filterArea, rect, filterAreaChanged) }
bool CoreDisplayObject::interactive(){ return m_interactive; }
void CoreDisplayObject::setInteractive(bool interactive){
    if (m_interactive != interactive){
        m_interactive = interactive;
        if (m_interactive){
            setAcceptedMouseButtons(Qt::AllButtons);
            setAcceptHoverEvents(true);
        }else{
            setAcceptedMouseButtons(Qt::NoButton);
            setAcceptHoverEvents(false);
        }
        emit interactiveChanged();
    }
}
CoreDisplayObject *CoreDisplayObject::mask(){ return m_mask; }
void CoreDisplayObject::setMask(CoreDisplayObject * mask){ SETPROP(m_mask, mask, maskChanged) }

QColor CoreDisplayObject::tint(){ return m_tint; }
void CoreDisplayObject::setTint(QColor tint){
    if (tint != m_tint){
        m_tint = tint;
        QQuickItem::update();
    }
}
Core::BlendModes CoreDisplayObject::blendMode(){ return m_blendMode; }
void CoreDisplayObject::setBlendMode(Core::BlendModes mode){
    if (mode != m_blendMode){
        m_blendMode = mode;
        QQuickItem::update();
    }
}

void CoreDisplayObject::componentComplete(){
    QQuickItem *parent = parentItem();
    while(parent){
        if (parent->inherits("DeclarativeScene")){
            m_scene = parent;
            break;
        }
        parent = parent->parentItem();
    }

    if (m_scene) static_cast<DeclarativeScene *>(m_scene)->addObject(this);
    QQuickItem::componentComplete();
    emit create();
}

void CoreDisplayObject::mousePressEvent(QMouseEvent *e){
    QDeclarativeMouseEvent me(e->pos(), mapToScene(e->pos()), e->button(), e->buttons(), e->modifiers(), false, false);
    emit mouseDown(&me);
}

void CoreDisplayObject::mouseReleaseEvent(QMouseEvent *e){
    QDeclarativeMouseEvent mePress(e->pos(), mapToScene(e->pos()), e->button(), e->buttons(), e->modifiers(), false, true);
    emit mouseUp(&mePress);

    if (boundingRect().contains(e->pos())){
        QDeclarativeMouseEvent meClick(e->pos(), mapToScene(e->pos()), e->button(), e->buttons(), e->modifiers(), true, true);
        emit click(&meClick);
    }
}

void CoreDisplayObject::mouseMoveEvent(QMouseEvent *e){
    QDeclarativeMouseEvent me(e->pos(), mapToScene(e->pos()), e->button(), e->buttons(), e->modifiers(), false, true);
    emit mouseMove(&me);
}

void CoreDisplayObject::hoverMoveEvent(QHoverEvent *e){
    QDeclarativeMouseEvent me(e->pos(), mapToScene(e->pos()), Qt::NoButton, Qt::NoButton, e->modifiers(), false, false);
    emit mouseMove(&me);
}



/** CoreSprite class **/
CoreSprite::CoreSprite(QQuickItem *parent) :
    CoreDisplayObject(parent),
    m_texture(0)
{
    setFlag(QQuickItem::ItemHasContents);
}

qreal CoreSprite::x() const{ return CoreDisplayObject::x() + m_anchorPoint.x()*width(); }
void CoreSprite::setX(qreal x){
    if (x != this->x()){
        CoreDisplayObject::setX(x - m_anchorPoint.x()*width());
        emit xChanged();
    }
}

qreal CoreSprite::y() const{ return CoreDisplayObject::y() + m_anchorPoint.y()*height(); }
void CoreSprite::setY(qreal y){
    if (y != this->y()){
        CoreDisplayObject::setY(y - m_anchorPoint.y()*height());
        emit yChanged();
    }
}

CoreTexture *CoreSprite::texture(){ return m_texture; }
void CoreSprite::setTexture(CoreTexture *texture){
    QPointF pos(x(), y());
    if (m_texture!=0) disconnect(m_texture, SIGNAL(update()), this, SLOT(textureUpdated()));
    m_texture = texture;
    if (m_texture!=0) connect(m_texture, SIGNAL(update()), this, SLOT(textureUpdated()));
    if (m_texture){
        setImplicitSize(m_texture->width(), m_texture->height());
    }else{
        setImplicitSize(0, 0);
    }
    if (isComponentComplete()) QQuickItem::setPosition(QPointF(pos.x()-m_anchorPoint.x()*width(), pos.y()-m_anchorPoint.y()*height()));
    QQuickItem::update();
}

QPointF CoreSprite::anchorPoint() const{ return m_anchorPoint; }
void CoreSprite::setAnchorPoint(const QPointF anchor){
    if (anchor != m_anchorPoint){
        if (isComponentComplete()) QQuickItem::setPosition(QPointF(this->x()-anchor.x()*width(), this->y()-anchor.y()*height()));
        m_anchorPoint = anchor;
        QQuickItem::update();
        emit anchorPointChanged();
    }
}

void CoreSprite::setTint(QColor tint){
    if (tint != blendMode()){
        CoreDisplayObject::setTint(tint);
        emit tintChanged();
    }
}

void CoreSprite::setBlendMode(Core::BlendModes mode){
    if (mode != blendMode()){
        CoreDisplayObject::setBlendMode(mode);
        emit blendModeChanged();
    }
}

void CoreSprite::textureUpdated(){
    QPointF pos(x(), y());
    if (m_texture){
        setImplicitSize(m_texture->width(), m_texture->height());
    }else{
        setImplicitSize(0, 0);
    }
    if (isComponentComplete()) QQuickItem::setPosition(QPointF(pos.x()-m_anchorPoint.x()*width(), pos.y()-m_anchorPoint.y()*height()));
}

void CoreSprite::componentComplete(){
    CoreDisplayObject::componentComplete();

    QPointF pos(QQuickItem::position());
    QQuickItem::setPosition(QPointF(pos.x()-m_anchorPoint.x()*width(), pos.y()-m_anchorPoint.y()*height()));

}

void CoreSprite::paint(QPainter *painter){
    //setTransformOriginPoint(QPointF(-width()*m_anchorPoint.x(), -height()*m_anchorPoint.y()));
    painter->setRenderHint(QPainter::Antialiasing);
    if (m_texture!=0 && m_texture->baseTexture()!=0){
        painter->drawPixmap(0, 0, width(), height(), m_texture->baseTexture()->source());
    }
}

//QSGNode *CoreSprite::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *paintData){
//    QSGSimpleTextureNode *node = static_cast<QSGSimpleTextureNode *>(CoreDisplayObject::updatePaintNode(oldNode, paintData));
//    return node;
//}


/** CoreTilingSprite class **/
CoreTilingSprite::CoreTilingSprite(QQuickItem *parent) :
    CoreSprite(parent)
{
}

QPointF CoreTilingSprite::tilePosition(){ return m_tilePosition; }
void CoreTilingSprite::setTilePosition(QPointF point){
    if (point != m_tilePosition){
        m_tilePosition = point;
        QQuickItem::update();
        emit tilePositionChanged();
    }
}

void CoreTilingSprite::paint(QPainter *painter){
    if (texture()!=0 && texture()->baseTexture()!=0){
        painter->drawTiledPixmap(0, 0, width(), height(), texture()->baseTexture()->source(), -m_tilePosition.x(), -m_tilePosition.y());
    }
}


/** CoreGraphics class **/
CoreGraphics::CoreGraphics(QQuickItem *parent) :
    CoreDisplayObject(parent), m_lineWidth(0), m_lineColor(Qt::black), m_lineAlpha(1),
    m_fillColor(Qt::transparent), m_fillAlpha(1), m_filling(false), m_dirty(true),
    m_boundsPadding(0)
{
    setFlag(QQuickItem::ItemHasContents);
}

void CoreGraphics::componentComplete(){
    connect(this, SIGNAL(create()), this, SLOT(updateGraphics()), Qt::QueuedConnection);
    CoreDisplayObject::componentComplete();
}

void CoreGraphics::paint(QPainter *painter){
    QRectF bounds(getGraphicBounds());
    if (bounds.isEmpty()) return;
    painter->setRenderHint(QPainter::Antialiasing, (smooth() || antialiasing()));
    for(int i=0; i<m_graphicsData.count(); i++){
        GraphicsPath data = m_graphicsData.at(i);
        QList<qreal> points = data.points;

        painter->setPen(QPen(data.lineColor, data.lineWidth, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));

        if(data.type == Poligon && points.count()>2){
            QPainterPath path;
            path.moveTo(points[0], points[1]);
            for (qreal j=1; j<points.count() / 2; j++){
                path.lineTo(points[j * 2], points[j * 2 + 1]);
            }
            // if the first and last point are the same close the path - much neater :)
            if(points.count()>3 && points[0] == points[points.count()-2] && points[1] == points[points.count()-1]){
                path.closeSubpath();
            }

            if(data.fill){
                painter->setOpacity(data.fillAlpha);
                painter->fillPath(path, data.fillColor);
            }
            if(data.lineWidth){
                painter->setOpacity(data.lineAlpha);
                painter->strokePath(path, QPen(data.lineColor, data.lineWidth, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
            }
        }else if(data.type == Rectangle){
            if (data.fill && data.fillColor != Qt::transparent){
                painter->setOpacity(data.fillAlpha);
                painter->fillRect(points[0], points[1], points[2], points[3], data.fillColor);
            }
            if(data.lineWidth>0){
                painter->setOpacity(data.lineAlpha);
                painter->drawRect(points[0], points[1], points[2], points[3]);
            }

        }else if(data.type == Circle || data.type == Elipse){
            QPainterPath path;
            path.addEllipse(QPointF(points[0], points[1]), points[2], points[3]);
            path.closeSubpath();

            if(data.fill){
                painter->setOpacity(data.fillAlpha);
                painter->fillPath(path, data.fillColor);
            }
            if(data.lineWidth){
                painter->setOpacity(data.lineAlpha);
                painter->strokePath(path, QPen(data.lineColor, data.lineWidth, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
            }
        }
    }
}

//QSGNode *CoreGraphics::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *){
//    QSGSimpleTextureNode *textureNode = static_cast<QSGSimpleTextureNode *>(oldNode);
//    if (!textureNode) {
//         textureNode = new QSGSimpleTextureNode;
//          m_textureChanged = true;
//    }
//    if (m_textureChanged || textureNode->texture()->textureSize() != QSize(width(), height())) {
//        textureNode->setTexture(window()->createTextureFromImage(m_textureImage));
//        textureNode->setRect(0, 0, width(), height());
//        textureNode->setFiltering(QSGTexture::Linear); //TODO: add filtering
//        m_textureChanged = false;
//    }

//    return textureNode;
//}

void CoreGraphics::updateGraphics(){
    if (m_dirty) m_graphicsData.push_back(m_currentPath);
    QRectF bounds(getGraphicBounds());
    m_dirty = false;
    setImplicitSize(bounds.topLeft().x() + bounds.bottomRight().x(), bounds.topLeft().y() + bounds.bottomRight().y());
    QQuickItem::update();

}

void CoreGraphics::lineStyle(const qreal lineWidth, const QColor color, const qreal alpha){
    if (!m_currentPath.points.isEmpty() && m_graphicsData.count()>0) m_graphicsData.pop_back();

    m_lineWidth = lineWidth;
    m_lineColor = color;
    m_lineAlpha = alpha;

    m_graphicsData.push_back(m_currentPath);

    m_currentPath.lineWidth = m_lineWidth;
    m_currentPath.lineColor = m_lineColor;
    m_currentPath.lineAlpha = m_lineAlpha;
    m_currentPath.fillColor = m_fillColor;
    m_currentPath.fillAlpha = m_fillAlpha;
    m_currentPath.fill = m_filling;
    m_currentPath.points = QList<qreal>();
    m_currentPath.type = Poligon;

}

void CoreGraphics::lineTo(const qreal x, const qreal y){
    m_currentPath.points << x << y;
    m_dirty = true;
}

void CoreGraphics::drawRect(const qreal x, const qreal y, const qreal width, const qreal height){
    if (!m_currentPath.points.isEmpty() && m_graphicsData.count()>0) m_graphicsData.pop_back();

    m_graphicsData.push_back(m_currentPath);

    m_currentPath.lineWidth = m_lineWidth;
    m_currentPath.lineColor = m_lineColor;
    m_currentPath.lineAlpha = m_lineAlpha;
    m_currentPath.fillColor = m_fillColor;
    m_currentPath.fillAlpha = m_fillAlpha;
    m_currentPath.fill = m_filling;
    m_currentPath.points = QList<qreal>() << x << y << width << height;
    m_currentPath.type = Rectangle;

    m_dirty = true;
}

void CoreGraphics::moveTo(const qreal x, const qreal y){
    if (!m_currentPath.points.isEmpty() && m_graphicsData.count()>0) m_graphicsData.pop_back();

    m_graphicsData.push_back(m_currentPath);

    m_currentPath.lineWidth = m_lineWidth;
    m_currentPath.lineColor = m_lineColor;
    m_currentPath.lineAlpha = m_lineAlpha;
    m_currentPath.fillColor = m_fillColor;
    m_currentPath.fillAlpha = m_fillAlpha;
    m_currentPath.fill = m_filling;
    m_currentPath.points = QList<qreal>() << x << y;
    m_currentPath.type = Poligon;
}

void CoreGraphics::beginFill(const QColor color, const qreal alpha){
    m_filling = true;
    m_fillColor = color;
    m_fillAlpha = alpha;
}

void CoreGraphics::endFill(){
    m_filling = false;
    m_fillColor = Qt::transparent;
    m_fillAlpha = 1;
}

void CoreGraphics::drawCircle(const qreal x,const qreal y,const qreal radius){
    if (!m_currentPath.points.isEmpty() && m_graphicsData.count()>0) m_graphicsData.pop_back();

    m_graphicsData.push_back(m_currentPath);

    m_currentPath.lineWidth = m_lineWidth;
    m_currentPath.lineColor = m_lineColor;
    m_currentPath.lineAlpha = m_lineAlpha;
    m_currentPath.fillColor = m_fillColor;
    m_currentPath.fillAlpha = m_fillAlpha;
    m_currentPath.fill = m_filling;
    m_currentPath.points = QList<qreal>() << x << y << radius << radius;
    m_currentPath.type = Circle;

    m_dirty = true;
}

QRectF CoreGraphics::getGraphicBounds(){

    qreal minX = 99999999.0;
    qreal maxX = -99999999.0;

    qreal minY = 99999999.0;
    qreal maxY = -99999999.0;

    QList<qreal> points;
    qreal x, y, w, h;

    for (int i=0; i<m_graphicsData.count(); i++) {
        GraphicsPath data = m_graphicsData.at(i);
        GraphicsPathType type = data.type;
        qreal lineWidth = data.lineWidth;

        points = data.points;

        if(type == Rectangle){
            x = points[0] - lineWidth/2;
            y = points[1] - lineWidth/2;
            w = points[2] + lineWidth;
            h = points[3] + lineWidth;

            minX = x < minX ? x : minX;
            maxX = x + w > maxX ? x + w : maxX;

            minY = y < minY ? x : minY;
            maxY = y + h > maxY ? y + h : maxY;
        }else if(type == Circle || type == Elipse){
            x = points[0];
            y = points[1];
            w = points[2] + lineWidth/2;
            h = points[3] + lineWidth/2;

            minX = x - w < minX ? x - w : minX;
            maxX = x + w > maxX ? x + w : maxX;

            minY = y - h < minY ? y - h : minY;
            maxY = y + h > maxY ? y + h : maxY;
        }else{
            // Poligon
            for (int j=0; j < points.count(); j+=2){
                x = points[j];
                y = points[j+1];
                minX = x-lineWidth < minX ? x-lineWidth : minX;
                maxX = x+lineWidth > maxX ? x+lineWidth : maxX;

                minY = y-lineWidth < minY ? y-lineWidth : minY;
                maxY = y+lineWidth > maxY ? y+lineWidth : maxY;
            }
        }
    }

    qreal padding = m_boundsPadding;
    if (minX<99999999.0 && maxX>-99999999.0 && minY<99999999.0 && maxY>-99999999.0){
        return QRectF(QPoint(minX - padding, minY - padding), QPoint((maxX - minX) + padding * 2, (maxY - minY) + padding * 2));
    }
    return QRectF();
}




CoreRemoteResource::CoreRemoteResource(QUrl sourceUrl, QObject *parent) :
    QObject(parent)
{
    connect(&m_netman, SIGNAL(finished(QNetworkReply*)), SLOT(fileFinished(QNetworkReply*)));
    QNetworkRequest request(sourceUrl);
    m_netman.get(request);
}

QByteArray CoreRemoteResource::data() const{ return m_data; }

void CoreRemoteResource::fileFinished(QNetworkReply *reply){
    if (reply->error()==QNetworkReply::NoError){
        m_data = reply->readAll();
        emit complete();
    }else{
        emit error(reply->error(), reply->errorString());
    }
    reply->deleteLater();
}
