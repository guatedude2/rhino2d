#ifndef DECLARATIVEPIXI_H
#define DECLARATIVEPIXI_H

#define RAD_TO_DEG 180.0/M_PI
#define DEG_TO_RAD M_PI/180.0

#define SETPROP(VAL,VAR,SIG) if (VAL != VAR){ VAR = VAL; emit SIG(); }

#include "qdeclarativeevents_p_p.h"
#include <QtMath>
#include <QFileInfo>
#include <QQuickItem>
#include <QImage>
#include <QPainter>
#include <QSGNode>
#include <QSGSimpleTextureNode>
#include <QQuickPaintedItem>
#include <QSGTexture>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QQuickImageProvider>
#include <QDomDocument>


class CoreGraphics;
class CoreTexture;
class CoreCoreTexture;
class CoreRemoteResource;
class Core : public QObject{
    Q_OBJECT
    Q_ENUMS(BlendModes)
public:

    enum BlendModes{
        BlendModeAdd = 12,
        //BlendModeColor = --,
        BlendModeColorBurn = 19,
        BlendModeColorDodge = 18,
        BlendModeDarken = 16,
        BlendModeDifference = 22,
        BlendModeExclusion = 23,
        BlendModeHardLight = 20,
        //BlendModeHue = --,
        BlendModeLighten = 17,
        //BlendModeLuminosity = --,
        BlendModeMultiply = 13,
        //BlendModeNormal = --,
        BlendModeOverlay = 15,
        //BlendModeSaturation = --,
        BlendModeScreen = 14,
        BlendModeSoftLight = 21
    };

    enum ScaleMode{
        ScaleModeDefault = 0,
        ScaleModesLinear = 0,
        ScaleModesNearest = 1
    };

    struct BitmapFontSettings{
        struct Char{
            int xOffset;
            int yOffset;
            int xAdvance;
            QHash<int, int> kerning;
            CoreTexture *texture;
        };

        QString font;
        int size;
        int lineHeight;
        QHash<ushort, Char> chars;
    };

    static inline qreal limit(qreal num, qreal min, qreal max) {
        qreal i = num;
        if (i < min) i = min;
        if (i > max) i = max;
        return i;
    }
};


class CoreShape : public QObject{
    Q_OBJECT
public:
    explicit CoreShape(QObject *parent = 0);
};


class CoreShapeRectangle : public CoreShape{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
public:
    explicit CoreShapeRectangle(QObject *parent = 0);
    CoreShapeRectangle(qreal x, qreal y, qreal width=0, qreal height=0, QObject *parent = 0);
    CoreShapeRectangle(QRectF rect, QObject *parent = 0);
    qreal x();
    void setX(qreal x);
    qreal y();
    void setY(qreal y);
    qreal width();
    void setWidth(qreal width);
    qreal height();
    void setHeight(qreal height);
    Q_INVOKABLE bool contains(qreal x, qreal y);

    bool contains (QPoint p);
    QRectF toRect();

signals:
    void xChanged();
    void yChanged();
    void widthChanged();
    void heightChanged();

private:
    qreal m_x, m_y, m_width, m_height;
};


class CoreShapePolygon : public CoreShape{
    Q_OBJECT
    Q_PROPERTY(QList<QPoint> points READ points WRITE setPoints NOTIFY pointsChanged)
public:
    CoreShapePolygon(QObject *parent = 0);
    CoreShapePolygon(QPoint point, QObject *parent = 0);
    CoreShapePolygon(QList<QPoint> points, QObject *parent = 0);
    CoreShapePolygon(QList<qreal> points, QObject *parent = 0);
    QList<QPoint> points();
    void setPoints(QList<QPoint> points);

    Q_INVOKABLE bool contains(qreal x, qreal y);

signals:
    void pointsChanged();
private:
    QList<QPoint> m_points;
};


class CoreShapeCircle : public CoreShape{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
public:
    CoreShapeCircle(QObject *parent = 0);
    CoreShapeCircle(qreal x, qreal y, qreal radius=0, QObject *parent = 0);
    qreal x();
    void setX(qreal x);
    qreal y();
    void setY(qreal y);
    qreal radius();
    void setRadius(qreal radius);

    Q_INVOKABLE bool contains(qreal x, qreal y);

    bool contains(QPoint p);

signals:
    void xChanged();
    void yChanged();
    void radiusChanged();

private:
    qreal m_x, m_y, m_radius;
};


class CoreShapeElipse : public CoreShapeRectangle{
public:
    CoreShapeElipse(QObject *parent=0);
    CoreShapeElipse(qreal x, qreal y, qreal width=0, qreal height=0, QObject *parent=0);
    CoreShapeElipse(QRect rect);
    Q_INVOKABLE bool contains(qreal x, qreal y);
    bool contains(QPoint p);

    Q_INVOKABLE CoreShapeRectangle *getBounds();

};


class CoreVector : public QObject{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
public:
    explicit CoreVector(QObject *parent = 0);
    CoreVector(qreal x, qreal y, QObject *parent = 0);

    qreal x();
    void setX(qreal x);
    qreal y();
    void setY(qreal y);

    Q_INVOKABLE void set(qreal x, qreal y);
    Q_INVOKABLE CoreVector *clone();
    Q_INVOKABLE void copy(CoreVector *v);
    Q_INVOKABLE void add(qreal x, qreal y=std::numeric_limits<qreal>::max());
    Q_INVOKABLE void add(CoreVector *v);
    Q_INVOKABLE void subtract(qreal x, qreal y=std::numeric_limits<qreal>::max());
    Q_INVOKABLE void subtract(CoreVector *v);
    Q_INVOKABLE void multiply(qreal x, qreal y=std::numeric_limits<qreal>::max());
    Q_INVOKABLE void multiply(CoreVector *v);
    Q_INVOKABLE void multiplyAdd(qreal x, qreal y);
    Q_INVOKABLE void multiplyAdd(CoreVector *v,  qreal y);
    Q_INVOKABLE void divide(qreal x, qreal y=std::numeric_limits<qreal>::max());
    Q_INVOKABLE void divide(CoreVector *v);
    Q_INVOKABLE qreal distance(CoreVector *v);
    Q_INVOKABLE qreal length();
    Q_INVOKABLE qreal dot(CoreVector *v=0);
    Q_INVOKABLE qreal dotNormalized(CoreVector *v=0);
    Q_INVOKABLE void rotate(qreal angle);
    Q_INVOKABLE void normalize();
    Q_INVOKABLE void limit(QPointF p);
    Q_INVOKABLE qreal angle(CoreVector *v);
    Q_INVOKABLE qreal angleFromOrigin(CoreVector *v);
    Q_INVOKABLE void round();

    QPointF toPointF();
signals:
    void xChanged();
    void yChanged();

protected:
    qreal m_x;
    qreal m_y;
};

// Textures

class CoreCoreTexture : public QObject{
    Q_OBJECT
public:

    static QMap<QString, CoreCoreTexture *> CoreTextureCache;
    CoreCoreTexture(QUrl source, Core::ScaleMode scaleMode=Core::ScaleModeDefault, QObject *parent=0);
    ~CoreCoreTexture();
    qreal width();
    qreal height();
    bool hasLoaded();
    QPixmap source();

    Core::ScaleMode scaleMode();

    void updateSourceImage(QUrl newSrc);
    QUrl url();
    static CoreCoreTexture *fromImage(QUrl imageUrl, Core::ScaleMode scaleMode);
signals:
    void completed();
protected slots:
    void complete();
    void error(QNetworkReply::NetworkError status, QString error);

protected:
    bool m_hasLoaded;
    QPixmap m_baseImage;
    QUrl m_imageUrl;
    Core::ScaleMode m_scaleMode;
    CoreRemoteResource *m_resource;
};


class CoreTexture : public QObject{
    Q_OBJECT
public:
    explicit CoreTexture(QObject * parent = 0);
    CoreTexture(CoreCoreTexture *baseTexture, QRectF frame = QRect(0,0, -1, -1), QObject * parent = 0);
    ~CoreTexture();

    CoreCoreTexture *baseTexture();
    QRectF frame();
    void setFrame(QRectF frame);
    qreal width();
    qreal height();

    static CoreTexture *fromImage(QUrl source, Core::ScaleMode scaleMode = Core::ScaleModeDefault);
    static QMap<QString, CoreTexture *> TextureCache;

signals:
    void update();

protected slots:
    void baseTextureLoaded();

protected:
    CoreCoreTexture *m_baseTexture;
    QRectF m_frame;
    QRectF m_trim;
    bool m_noFrame;

};

// Loaders

class CoreLoader : public QObject{
    Q_OBJECT
public:
    CoreLoader(QObject *parent=0) :
        QObject(parent){}
    virtual void load(){}
signals:
    void loaded();
};


class CoreImageLoader : public CoreLoader{
    Q_OBJECT
public:
    CoreImageLoader(QUrl source, QObject *parent=0);
    CoreTexture *texture();
    void load();
private:
    CoreTexture *m_texture;
};


class CoreBitmapFontLoader : public CoreLoader{
    Q_OBJECT
public:
    CoreBitmapFontLoader(QUrl source, QObject *parent=0);
    ~CoreBitmapFontLoader();

    void load();

protected slots:
    void xmlLoaded();
    void imageLoaded();
    void error(QNetworkReply::NetworkError status, QString error);

protected:
    QDomDocument *loadXmlDocument(QByteArray text);
    //QVariantMap parseNodeElement(QDomElement *element);
private:
    QUrl m_source;
    CoreRemoteResource *m_resource;
    QIODevice *m_device;
    CoreTexture *m_texture;

};


class CoreAssetLoader : public QObject{
    Q_OBJECT
public:
    CoreAssetLoader(QList<QUrl> assets, QObject *parent=0);
    QList<QUrl> assetURLs();
    void load();

signals:
    void progress();
    void completed();
    void error();
protected slots:
    void assetLoaded();

protected:
    QList<QUrl> m_assetURLs;
    int m_loadCount;
    QString getDataType(const QString str) const;
};

// Drawables

class CoreDisplayObject : public QQuickPaintedItem{
    Q_OBJECT
    Q_PROPERTY(CoreShape *hitArea READ hitArea WRITE setHitArea NOTIFY hitAreaChanged)
    Q_PROPERTY(QString defaultCursor READ defaultCursor WRITE setDefaultCursor NOTIFY defaultCursorChanged)
    Q_PROPERTY(bool buttonMode READ buttonMode WRITE setButtonMode NOTIFY buttonModeChanged)
    Q_PROPERTY(CoreShapeRectangle *filterArea READ filterArea WRITE setFilterArea NOTIFY filterAreaChanged)
    Q_PROPERTY(bool interactive READ interactive WRITE setInteractive NOTIFY interactiveChanged)
    Q_PROPERTY(CoreDisplayObject *mask READ mask WRITE setMask NOTIFY maskChanged)
    Q_PROPERTY(bool antialiasing READ antialiasing WRITE setAntialiasing NOTIFY antialiasingChanged)
    //filters
public:
    explicit CoreDisplayObject(QQuickItem *parent = 0);
    ~CoreDisplayObject();
    CoreShape *hitArea();
    void setHitArea(CoreShape * shape);
    QString defaultCursor();
    void setDefaultCursor(QString cursor);
    bool buttonMode();
    void setButtonMode(bool buttonMode);
    CoreShapeRectangle *filterArea();
    void setFilterArea(CoreShapeRectangle * rect);
    bool interactive();
    void setInteractive(bool interactive);
    CoreDisplayObject *mask();
    void setMask(CoreDisplayObject * maks);

    QColor tint();
    void setTint(QColor tint);
    Core::BlendModes blendMode();
    void setBlendMode(Core::BlendModes mode);

    virtual void sceneUpdate(){ emit update(); }

signals:
    void hitAreaChanged();
    void defaultCursorChanged();
    void buttonModeChanged();
    void filterAreaChanged();
    void interactiveChanged();
    void maskChanged();

    void click(QDeclarativeMouseEvent *event);
    void mouseDown(QDeclarativeMouseEvent *event);
    void mouseUp(QDeclarativeMouseEvent *event);
    void mouseMove(QDeclarativeMouseEvent *event);
    void mouseOut(QDeclarativeMouseEvent *event);
    void keyDown(QDeclarativeKeyEvent *event);
    void keyUp(QDeclarativeKeyEvent *event);

    void update();
    void create();
    void antialiasingChanged();

protected:
    void componentComplete();
    virtual void paint(QPainter *){}

    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void hoverMoveEvent(QHoverEvent *e);

private:
    CoreShape *m_hitArea;
    QString m_defaultCursor;
    bool m_buttonMode;
    CoreShapeRectangle *m_filterArea;
    bool m_interactive;
    CoreDisplayObject *m_mask;
    QQuickItem *m_scene;
    QColor m_tint;
    Core::BlendModes m_blendMode;
};

class CoreSprite : public CoreDisplayObject
{
    Q_OBJECT
    Q_ENUMS(Core::BlendModes)
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(QPointF anchorPoint READ anchorPoint WRITE setAnchorPoint NOTIFY anchorPointChanged)
    Q_PROPERTY(QColor tint READ tint WRITE setTint NOTIFY tintChanged)
    Q_PROPERTY(Core::BlendModes blendMode READ blendMode WRITE setBlendMode NOTIFY blendModeChanged)
public:
    explicit CoreSprite(QQuickItem *parent = 0);

    qreal x() const;
    void setX(qreal x);
    qreal y() const;
    void setY(qreal y);

    CoreTexture *texture();
    void setTexture(CoreTexture *texture);
    QPointF anchorPoint() const;
    void setAnchorPoint(const QPointF anchor);
    void setTint(QColor tint);
    void setBlendMode(Core::BlendModes mode);

signals:
   void textureChanged();
   void anchorPointChanged();
   void tintChanged();
   void blendModeChanged();
   void xChanged();
   void yChanged();

protected slots:
   void textureUpdated();

protected:
   void componentComplete();
   virtual void paint(QPainter *painter);
   //QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

private:
   CoreTexture *m_texture;
   QPointF m_anchorPoint;

};

class CoreTilingSprite : public CoreSprite{
    Q_OBJECT
    Q_PROPERTY(QPointF tilePosition READ tilePosition WRITE setTilePosition NOTIFY tilePositionChanged)
public:
    CoreTilingSprite(QQuickItem *parent=0);

    QPointF tilePosition();
    void setTilePosition(QPointF point);

signals:
    void tilePositionChanged();

protected:
   void paint(QPainter *painter);

private:
    QPointF m_tilePosition;

};

class CoreGraphics : public CoreDisplayObject{
    Q_OBJECT
public:
    explicit CoreGraphics(QQuickItem *parent = 0);

    enum GraphicsPathType{ Poligon = 0, Rectangle = 1, Circle = 2, Elipse = 3 };
    struct GraphicsPath{
        qreal lineWidth;
        QColor lineColor;
        qreal lineAlpha;
        QColor fillColor;
        qreal fillAlpha;
        bool fill;
        QList<qreal> points;
        GraphicsPathType type;
    };

    Q_INVOKABLE void lineStyle(const qreal lineWidth = 0, const QColor color = Qt::transparent, const qreal alpha = 1);
    Q_INVOKABLE void lineTo(const qreal x, const qreal y);
    Q_INVOKABLE void drawRect(const qreal x, const qreal y, const qreal width, const qreal height);
    Q_INVOKABLE void moveTo(const qreal x, const qreal y);
    Q_INVOKABLE void beginFill(const QColor color = Qt::transparent, const qreal alpha = 1);
    Q_INVOKABLE void endFill();
    Q_INVOKABLE void drawCircle(const qreal x, const qreal y, const qreal radius);

protected slots:
    void updateGraphics();

protected:
    void componentComplete();
    void paint(QPainter *painter);

private:
    QList<GraphicsPath> m_graphicsData;
    GraphicsPath m_currentPath;
    qreal m_lineWidth;
    QColor m_lineColor;
    qreal m_lineAlpha;
    QColor m_fillColor;
    qreal m_fillAlpha;
    bool m_filling;
    bool m_dirty;
    int m_boundsPadding;

    QRectF getGraphicBounds();
};


class CoreRemoteResource : public QObject{
    Q_OBJECT
public:
    explicit CoreRemoteResource(QUrl sourceUrl, QObject *parent = 0);

    QByteArray data() const;

signals:
    void complete();
    void error(QNetworkReply::NetworkError status, QString error);

private slots:
    void fileFinished(QNetworkReply* reply);

private:

    QNetworkAccessManager m_netman;
    QByteArray m_data;
};

#endif // DECLARATIVEPIXI_H
