#ifndef DECLARATIVETIMER_H
#define DECLARATIVETIMER_H

#include <QObject>
#include <QJSValue>

class DeclarativeTimer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool repeat READ repeat WRITE setRepeat NOTIFY repeatChanged)
public:
    DeclarativeTimer(int ms, QObject *parent = 0);
    DeclarativeTimer(QObject *parent = 0);

    double delta();
    void set(int ms);
    void reset();
    double time();
    void pause();
    void resume();
    bool repeat();
    void setRepeat(bool repeat);
    QJSValue callback() const;
    void setCallback(const QJSValue &callback);

    static bool seconds();
    static void update();

    static double s_last;
    static double s_time;
    static int s_speedFactor;
    static int s_maxStep;
    static bool s_seconds;
signals:
    void repeatChanged();

public slots:

private:
    bool m_repeat;
    double m_target;
    double m_base;
    double m_last;
    double m_pauseTime;
    QJSValue m_callback;

};

#endif // DECLARATIVETIMER_H
