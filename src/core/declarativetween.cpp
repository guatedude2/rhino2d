#include "declarativegamewindow.h"
#include "declarativetween.h"

#include <QQmlProperty>

DeclarativeTween::DeclarativeTween(QObject *parent) :
    QObject(parent),
    m_componentComplete(false),
    m_target(0),
    m_isPlaying(false),
    m_onStartCallbackFired(false),
    m_paused(false),
    m_startTime(0),
    m_delayTime(0),
    m_originalStartTime(0),
    m_duration(1000),
    m_delay(0),
    m_repeat(0),
    m_repeats(0),
    m_yoyo(false),
    m_reversed(false),
    m_delayRepeat(false)
{
    QJSEngine *myEngine = new QJSEngine(this);
    m_easing = myEngine->evaluate("(function (r){ return r; })");
    m_interpolation = myEngine->evaluate("(function (v, k) { var m = v.length - 1, f = m * k, i = Math.floor(f), fn = Interpolation.Utils.Linear; if(k < 0) return fn(v[0], v[1], f); if(k > 1) return fn(v[m], v[m - 1], m - f); return fn(v[i], v[i + 1 > m ? m : i + 1], f - i); })");
}

DeclarativeTween::~DeclarativeTween(){
    qDeleteAll(m_data);
}

void DeclarativeTween::componentComplete(){
    m_componentComplete = true;
    checkRunning();
}

void DeclarativeTween::classBegin(){

}

QQmlListProperty<QObject> DeclarativeTween::data(){
    return QQmlListProperty<QObject>(this, m_data); //TODO: change to non-prototype version
}

QObject *DeclarativeTween::target(){ return m_target; }
void DeclarativeTween::setTarget(QObject *target){
    if (target != m_target){
        m_target = target;
        emit targetChanged();
    }
}

bool DeclarativeTween::isPlaying() const{ return m_isPlaying; }
bool DeclarativeTween::paused() const{ return m_paused; }
int DeclarativeTween::duration() const{ return m_duration; }
void DeclarativeTween::setDuration(const int duration){
    if (duration != m_duration){
        m_duration = duration;
        emit durationChanged();
    }
}

int DeclarativeTween::delay() const{ return m_delay; }
void DeclarativeTween::setDelay(const int delay){
    if (delay != m_delay){
        m_delay = delay;
        emit delayChanged();
    }
}

QJSValue DeclarativeTween::easing() const{ return m_easing; }
void DeclarativeTween::setEasing(const QJSValue &easing){
    m_easing = easing;
    emit easingChanged();
}

QJSValue DeclarativeTween::interpolation() const{ return m_interpolation; }
void DeclarativeTween::setInterpolation(const QJSValue &interpolation){
    m_interpolation = interpolation;
    emit interpolationChanged();
}

QVariantMap DeclarativeTween::to() const{ return m_valuesEnd; }
void DeclarativeTween::setTo(const QVariantMap &to){
    if (to != m_valuesEnd){
        m_valuesEnd = to;
        emit toChanged();
    }
}

QVariantMap DeclarativeTween::from() const{ return m_valuesStart; }
void DeclarativeTween::setFrom(const QVariantMap &from){
    if (from != m_valuesStart){
        m_valuesStart = from;
        emit fromChanged();
    }
}

int DeclarativeTween::repeat() const{ return m_repeat; }
void DeclarativeTween::setRepeat(const int repeat){
    if (repeat != m_repeat){
        m_repeat = repeat;
        emit repeatChanged();
    }
}

bool DeclarativeTween::yoyo() const{ return m_yoyo; }
void DeclarativeTween::setYoyo(const bool yoyo){
    if (yoyo != m_yoyo){
        m_yoyo = yoyo;
        emit yoyoChanged();
    }
}

bool DeclarativeTween::running(){ return m_isPlaying; }
void DeclarativeTween::setRunning(const bool running){    
    if (running && !m_isPlaying){
        m_isPlaying = true;
        m_paused = false;
        checkRunning();
    }else if (!running && m_isPlaying){
        m_isPlaying = false;
        m_paused = true;
        checkRunning();
    }
}

bool DeclarativeTween::update(int time){
    if(time < m_startTime) {
        return true;
    }
    if(m_onStartCallbackFired == false) {
        emit started();
        m_onStartCallbackFired = true;
    }
    qreal elapsed = (time - m_startTime) / (qreal) m_duration;
    elapsed = elapsed > 1 ? 1 : elapsed;

    qreal value = m_easing.call(QJSValueList() << elapsed).toNumber();
    foreach(const QString property, m_valuesEnd.keys()) {
        QVariant start = m_valuesStart.value(property, 0);
        QVariant end = m_valuesEnd.value(property);
        if(end.type()==QVariant::List) {
            //_object[property] = _interpolationFunction(end, value);
            // Parses relative end values with start as base (e.g.: +10, -3)
        }else if (end.canConvert<QColor>()){
            QColor endColor = end.value<QColor>();
            QColor startColor = start.value<QColor>();
            int startA = startColor.alpha(), startR = startColor.red(), startG = startColor.green(), startB = startColor.blue();
            int endA = endColor.alpha(), endR = endColor.red(), endG = endColor.green(), endB = endColor.blue();

            int valA = (startA + (endA - startA) * value);
            int valR = (startR + (endR - startR) * value);
            int valG = (startG + (endG - startG) * value);
            int valB = (startB + (endB - startB) * value);

            QQmlProperty::write(m_target, property, QColor(valR, valG, valB, valA));
        }else{
            if(end.type() == QVariant::String) end = QVariant(start.toDouble() + end.toString().toUInt());
            // protect against non numeric properties.
            bool ok; end.toFloat(&ok);
            if(ok) {
                qreal startNum = start.toFloat();
                qreal endNum = end.toFloat();
                QQmlProperty::write(m_target, property, (startNum + (endNum - startNum) * value));
            }
        }
    }
    emit update();

    if (elapsed == 1) {
        if(m_repeat != 0) {
            if(m_repeat>0) {
                m_repeat--;
            }
            m_repeats += 1;
            // reassign starting values, restart by making startTime = now
            foreach (const QString property, m_valuesStartRepeat.keys()) {
//                if(typeof (_valuesEnd[property]) === 'string') {
//                    _valuesStartRepeat[property] = _valuesStartRepeat[property] + parseFloat(_valuesEnd[property], 10);
//                }
                if(m_yoyo) {
                    QVariant tmp = m_valuesStartRepeat.value(property);
                    m_valuesStartRepeat.insert(property, m_valuesEnd.value(property));
                    m_valuesEnd.insert(property, tmp);
                    m_reversed = !m_reversed;
                }
                m_valuesStart.insert(property, m_valuesStartRepeat.value(property));
            }
            if(!m_delayRepeat) m_delayTime = 0;
            m_startTime = m_originalStartTime + m_repeats * (m_duration + m_delayTime);
            return true;
        } else {
            m_isPlaying = false;
            emit completed();
//            for (var i = 0, numChainedTweens = _chainedTweens.length; i < numChainedTweens; i++) {
//                _chainedTweens[i].start(time);
//            }
            return false;
        }
    }
    return true;
}

void DeclarativeTween::start(int time){
    DeclarativeGameWindow::instance()->tweenEngine()->add(this);
    m_isPlaying = true;
    m_onStartCallbackFired = false;
    m_startTime = (time!=0 ? time  : DeclarativeTimer::s_time);
    m_startTime += m_delayTime;
    m_originalStartTime = m_startTime;
    foreach(const QString property, m_valuesEnd.keys()){
        QVariant objectProperty = QQmlProperty::read(target(), property);
        // check if an Array was provided as property value
        if (m_valuesEnd.value(property).type() == QVariant::List && m_valuesEnd.value(property).toList().count()==0) continue;
        m_valuesStart.insert(property, m_valuesStart.value(property, objectProperty));
        if (m_valuesStart.value(property).type() != QVariant::List && m_valuesStart.value(property).type() != QVariant::Color){
            //ensure we are using numeric values only
            bool ok; m_valuesStart.value(property).toDouble(&ok);
            if (!ok){
                m_valuesStart.remove(property);
                continue;
            }
        }
        m_valuesStartRepeat.insert(property, m_valuesStart.value(property, 0));
    }
}

void DeclarativeTween::pause(){

}

void DeclarativeTween::resume(){

}

void DeclarativeTween::stop(){
    //DeclarativeGameWindow::instance()->tweenEngine()->remove(this);

}

void DeclarativeTween::checkRunning(){
    if (m_componentComplete){
        if (m_isPlaying) start();
        else stop();
    }
}


TweenEngine::TweenEngine(QObject *parent) :
    QObject(parent)
{
}

QList<DeclarativeTween *> TweenEngine::getAll() {
    return m_tweens;
}

void TweenEngine::removeAll() {
    m_tweens.clear();
}

void TweenEngine::stopAllForObject(QObject *obj) {
    foreach(DeclarativeTween *tween, m_tweens){
        if (tween->target()==obj) tween->stop();
    }
}

//return list of tweens?
QList<DeclarativeTween *> TweenEngine::getTweensForObject(QObject *obj) {
    QList<DeclarativeTween *> list;
    foreach(DeclarativeTween *tween, m_tweens){
        if(tween->target() == obj) list.append(tween);
    }
    return list;
}

void TweenEngine::add(DeclarativeTween *tween){
    m_tweens.append(tween);
}

void TweenEngine::remove(DeclarativeTween *tween){
    int i = m_tweens.indexOf(tween);
    if(i != -1) {
        m_tweens.removeAt(i);
    }
}

bool TweenEngine::update(int time) {
    if(m_tweens.count() == 0) return false;
    time = time != 0 ? time : DeclarativeTimer::s_time;
    for(int i=m_tweens.count()-1; i>=0; i--){
        if (!m_tweens.at(i)->update(time)) m_tweens.removeAt(i);
    }
    return true;
}
