#ifndef DECLARATIVEPOOL_H
#define DECLARATIVEPOOL_H

#include <QObject>
#include <QHash>
#include <QVariantList>

class DeclarativePool : public QObject
{
    Q_OBJECT
public:
    explicit DeclarativePool(QObject *parent = 0);

    Q_INVOKABLE static bool create(QString pool);
    Q_INVOKABLE static QObject *get(QString pool);
    Q_INVOKABLE static bool put(QString pool, QObject *object);

protected:
    static QHash<QString, QList<QObject *> *> s_pools;

};

#endif // DECLARATIVEPOOL_H
