#ifndef DECLARATIVETWEEN_H
#define DECLARATIVETWEEN_H

#include "declarativetimer.h"

#include <QQuickItem>
#include <QQmlParserStatus>
#include <QQmlListProperty>

const float C_PI = 3.14159f;

class DeclarativeTween : public QObject, public QQmlParserStatus
{
    Q_INTERFACES(QQmlParserStatus)
    Q_OBJECT
    Q_ENUMS(RepeatType)
    Q_PROPERTY(QQmlListProperty<QObject> data READ data)

    Q_PROPERTY(QObject *target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(bool isPlaying READ isPlaying NOTIFY isPlayingChanged)
    Q_PROPERTY(bool paused READ paused NOTIFY pausedChanged)
    Q_PROPERTY(int duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(int delay READ delay WRITE setDelay NOTIFY delayChanged)
    Q_PROPERTY(QJSValue easing READ easing WRITE setEasing NOTIFY easingChanged)
    Q_PROPERTY(QJSValue interpolation READ interpolation WRITE setInterpolation NOTIFY interpolationChanged)
    Q_PROPERTY(QVariantMap to READ to WRITE setTo NOTIFY toChanged)
    Q_PROPERTY(QVariantMap from READ from WRITE setFrom NOTIFY fromChanged)
    Q_PROPERTY(int repeat READ repeat WRITE setRepeat NOTIFY repeatChanged)
    Q_PROPERTY(bool yoyo READ yoyo WRITE setYoyo NOTIFY yoyoChanged)
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)

    Q_CLASSINFO("DefaultProperty", "data")

public:
    explicit DeclarativeTween(QObject *parent = 0);
    ~DeclarativeTween();
    enum RepeatType { RepeatNone = 0,  RepeatForever = -1 };

    void componentComplete();
    void classBegin();

    QQmlListProperty<QObject> data();

    QObject *target();
    void setTarget(QObject *target);
    bool isPlaying() const;
    bool paused() const;
    int duration() const;
    void setDuration(const int duration);
    int delay() const;
    void setDelay(const int delay);
    QJSValue easing() const;
    void setEasing(const QJSValue &easing);
    QJSValue interpolation() const;
    void setInterpolation(const QJSValue &interpolation);
    QVariantMap to() const;
    void setTo(const QVariantMap &to);
    QVariantMap from() const;
    void setFrom(const QVariantMap &from);
    int repeat() const;
    void setRepeat(const int repeat);
    bool yoyo() const;
    void setYoyo(const bool yoyo);
    bool running();
    void setRunning(const bool running);

    bool update(int time);

    //chain
    Q_INVOKABLE void start(int time = 0);
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void stop();
    //stopChainedTweens() ?

Q_SIGNALS:
    void targetChanged();
    void isPlayingChanged();
    void pausedChanged();
    void durationChanged();
    void delayChanged();
    void easingChanged();
    void interpolationChanged();
    void toChanged();
    void fromChanged();
    void repeatChanged();
    void yoyoChanged();
    void runningChanged();

    void completed();
    void started();
    void update();


protected:
    bool m_componentComplete;
    QList<QObject *> m_data;

    QObject *m_target;
    bool m_isPlaying;
    bool m_onStartCallbackFired;
    bool m_paused;
    int m_startTime;
    int m_delayTime;
    int m_originalStartTime;
    int m_duration;
    int m_delay;
    QJSValue m_easing;
    QJSValue m_interpolation;
    QVariantMap m_valuesEnd;
    QVariantMap m_valuesStart;
    QVariantMap m_valuesStartRepeat;
    int m_repeat;
    int m_repeats;
    bool m_yoyo;
    bool m_reversed;
    bool m_delayRepeat;

    void checkRunning();
};


class TweenEngine : public QObject{
public:
    explicit TweenEngine(QObject * parent = 0);

    QList<DeclarativeTween *> getAll();
    void removeAll();
    void stopAllForObject(QObject *obj);
    QList<DeclarativeTween *> getTweensForObject(QObject *obj);
    void add(DeclarativeTween *tween);
    void remove(DeclarativeTween *tween);
    bool update(int time = 0);

protected:
    QList<DeclarativeTween *> m_tweens;
};

#endif // DECLARATIVETWEEN_H
