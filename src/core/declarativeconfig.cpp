#include "declarativeconfig.h"

DeclarativeConfig::DeclarativeConfig(QObject *parent) :
    QObject(parent)
{
}

QUrl DeclarativeConfig::baseUrl() const{ return m_baseUrl; }
void DeclarativeConfig::setCoreUrl(const QUrl baseUrl){
    if (baseUrl != m_baseUrl){
        m_baseUrl = baseUrl;
        emit baseUrlChanged();
    }
}

QString DeclarativeConfig::storageId() const{ return m_storageId; }
void DeclarativeConfig::setStorageId(const QString storageId){
    if (storageId != m_storageId){
        m_storageId = storageId;
        emit storageIdChanged();
    }
}

QString DeclarativeConfig::analyticsId() const{ return m_analyticsId; }
void DeclarativeConfig::setAnalyticsId(const QString analyticsId){
    if (analyticsId != m_analyticsId){
        m_analyticsId = analyticsId;
        emit analyticsIdChanged();
    }
}
