#ifndef DECLARATIVEGAMEWINDOW_H
#define DECLARATIVEGAMEWINDOW_H

#include "../audio/declarativeaudio.h"
#include "declarativesystem.h"
#include "declarativetween.h"
#include "declarativecore.h"
#include "declarativepool.h"
#include "declarativestorage.h"
#include "declarativeconfig.h"
#include "declarativekeyboard.h"

#include <QtQuick/QQuickWindow>
#include <QQmlParserStatus>
#include <QtCore/QCoreApplication>
#include <QtQml/QQmlEngine>
#include <QQmlContext>
#include <QtQml/QQmlComponent>
#include <QQuickItem>
#include <QQmlProperty>

class DeclarativeGameWindow : public QQuickWindow, public QQmlParserStatus
{    
    Q_INTERFACES(QQmlParserStatus)
    Q_OBJECT

    Q_PROPERTY(QString version READ version)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(Visibility visibility READ visibility WRITE setVisibility NOTIFY visibilityChanged)
    Q_PROPERTY(QQuickItem *scene READ scene NOTIFY sceneChanged)
    Q_PROPERTY(DeclarativeAudio *audio READ audio NOTIFY audioChanged)
    Q_PROPERTY(DeclarativeSystem *system READ system NOTIFY systemChanged)
    Q_PROPERTY(DeclarativePool *pool READ pool NOTIFY poolChanged)
    //device
    Q_PROPERTY(DeclarativeStorage *storage READ storage NOTIFY storageChanged)
    Q_PROPERTY(DeclarativeKeyboard *keyboard READ keyboard NOTIFY keyboardChanged)
    Q_PROPERTY(DeclarativeConfig *config READ config NOTIFY configChanged)

public:
    explicit DeclarativeGameWindow(QWindow *parent = 0);

    QString version();
    DeclarativeAudio *audio();
    DeclarativeSystem *system();    
    DeclarativePool *pool();
    DeclarativeStorage *storage();
    DeclarativeKeyboard *keyboard();
    DeclarativeConfig *config();

    QQuickItem *scene();
    void setScene(QQuickItem *scene);

    void setVisible(bool visible);
    void setVisibility(Visibility visibility);

    QMetaObject::Connection setGameLoop(QObject *obj, const char *slot);
    void clearGameLoop(QMetaObject::Connection connId);
    TweenEngine *tweenEngine();

    void classBegin();
    void componentComplete();

    Q_INVOKABLE QString addAsset(const QUrl &source, const QString &id = QString());
    Q_INVOKABLE QString addSound(const QUrl &source, const QString &id);
    Q_INVOKABLE QString addMusic(const QUrl &source, const QString &id);
    Q_INVOKABLE void start(QQmlComponent *scene, const qreal width, const qreal height, QQmlComponent *loaderComponent = 0);

    QMap<QString, QUrl> resources();

    static DeclarativeGameWindow *instance();
signals:
    void visibleChanged(bool arg);
    void visibilityChanged(QWindow::Visibility visibility);
    void sceneChanged();
    void audioChanged();
    void systemChanged();
    void poolChanged();
    void storageChanged();
    void keyboardChanged();
    void configChanged();

protected slots:
    void requestPaint();
    void setWindowVisibility();

protected:
    DeclarativeSystem::Platform platform();

private:
    static DeclarativeGameWindow *s_instance;
    QColor m_backgroundColor;
    QMap<QString, QUrl> m_resources;
    bool m_complete;
    bool m_visible;
    Visibility m_visibility;
    DeclarativeAudio m_audioService;
    DeclarativeSystem m_systemService;
    DeclarativePool m_poolService;
    DeclarativeStorage m_storageService;
    DeclarativeKeyboard m_keyboardService;
    DeclarativeConfig m_config;
    QQuickItem *m_scene;
    TweenEngine *m_tweenEngine;

};

#endif // DECLARATIVEGAMEWINDOW_H
