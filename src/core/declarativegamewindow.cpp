/*!
    \qmltype GameWindow
    \instantiates DeclarativeGameWindow
    \inherits QQuickWindow
    \inqmlmodule Rhino2D 1.0
    \since 1.0
    \brief The main window component that is used as the root
    element for displaying graphical elements in a game.

    The GameWindow element provides a window for displaying
    all graphical elements in a game.

    The GameWindow provides an interface to the following
    components:

    \list
      \li \l Audio
      \li \l System
      \li \l Pool
      \li \l Device
      \li \l Storage
      \li \l Keyboard
      \li \l Config
    \endlist

    The GameWindow also allows you to preload
    images, audio files, spine animations, atlas images,
    spritesheets and json assets.

*/

#include "rhino2d_plugin.h"
#include "declarativegamewindow.h"
#include "declarativeloader.h"

#include <QtQml/QQmlInfo>
#include <QSysInfo>


DeclarativeGameWindow *DeclarativeGameWindow::s_instance = 0;
DeclarativeGameWindow::DeclarativeGameWindow(QWindow *parent) :
    QQuickWindow(parent),
    m_backgroundColor(Qt::black),
    m_complete(false),
    m_visible(isVisible()),
    m_visibility(AutomaticVisibility),
    m_scene(0),
    m_tweenEngine(0)
{
    setColor(m_backgroundColor);
    m_audioService.setParent(contentItem());
    m_systemService.setParent(contentItem());
    m_poolService.setParent(contentItem());
    m_storageService.setParent(contentItem());
    m_keyboardService.setParent(contentItem());
    m_config.setParent(contentItem());

    m_systemService.setPlatform(platform());

    if (s_instance!=0) qFatal("Only one instance of GameWindow can be created");
    s_instance = this;
    connect(this, &QWindow::visibleChanged, this, &DeclarativeGameWindow::visibleChanged);
    connect(this, &QWindow::visibilityChanged, this, &DeclarativeGameWindow::visibilityChanged);
    connect(this, &DeclarativeGameWindow::beforeRendering, this, &DeclarativeGameWindow::requestPaint, Qt::DirectConnection);
    connect(this, &DeclarativeGameWindow::activeChanged, this, &DeclarativeGameWindow::requestPaint);
    m_tweenEngine = new TweenEngine(this);
    //setClearBeforeRendering(false);
    setWindowState(Qt::WindowFullScreen);
}

/*!
        \qmlproperty string GameWindow::version
        \readonly

        Returns the current version of the game engine.
*/
QString DeclarativeGameWindow::version(){ return Q_RHINO_VERSION; }


/*!
    \qmlproperty Audio GameWindow::audio
    \readonly

    The \l Audio service controls the playback of loaded
    audio assets.
*/
DeclarativeAudio *DeclarativeGameWindow::audio(){ return &m_audioService; }

/*!
    \qmlproperty System GameWindow::system
    \readonly

    This \l System service element provides an interface to
    screen functions, state of the game, and other functions.
*/
DeclarativeSystem *DeclarativeGameWindow::system(){ return &m_systemService; }

/*!
    \qmlproperty Pool GameWindow::pool
    \readonly

    The \l Pool service allows objects to be stored and used
    in the future to reduce loading times and optimize memory.
*/
DeclarativePool *DeclarativeGameWindow::pool(){ return &m_poolService; }

/*!
    \qmlproperty Storage GameWindow::storage
    \readonly

    This \l Storage service allows persistent platform-independent
    settings to be stored and restored.
*/
DeclarativeStorage *DeclarativeGameWindow::storage(){ return &m_storageService; }

/*!
    \qmlproperty Keyboard GameWindow::keyboard
    \readonly

    The \l Keyboard service provides a simple way to interface with
    keyboard key presses.
*/
DeclarativeKeyboard *DeclarativeGameWindow::keyboard(){ return &m_keyboardService; }

/*!
    \qmlproperty Config GameWindow::config::baseUrl

    Sets the base url path of the game.
*/
/*!
    \qmlproperty Config GameWindow::config::storageId

    Sets the id of the storage to be used. e.g com.example.mygame
*/
/*!
    \qmlproperty Config GameWindow::config::analyticsId

    Sets the id for google analtics tracking.
*/
DeclarativeConfig *DeclarativeGameWindow::config(){ return &m_config; }

/*!
    \qmlproperty GameScene GameWindow::scene
    \readonly

    Returns the current loaded \l GameScene. If a \l GameScene
    hasn't been loaded then it will return \c null.
*/
QQuickItem *DeclarativeGameWindow::scene(){ return m_scene; }
void DeclarativeGameWindow::setScene(QQuickItem *scene){
    m_scene = scene;
    emit sceneChanged();
}

void DeclarativeGameWindow::setVisible(bool visible) {
    if (!m_complete)
        m_visible = visible;
    else if (!transientParent() || transientParent()->isVisible())
        QQuickWindow::setVisible(visible);
}

void DeclarativeGameWindow::setVisibility(Visibility visibility){
    if (!m_complete)
        m_visibility = visibility;
    else
        QQuickWindow::setVisibility(visibility);
}


QMetaObject::Connection DeclarativeGameWindow::setGameLoop(QObject *obj, const char *slot){
    return connect(this, SIGNAL(beforeSynchronizing()), obj, slot); //Qt::DirectConnection
    update();
}

void DeclarativeGameWindow::clearGameLoop(QMetaObject::Connection connId){
    disconnect(connId);
}

TweenEngine *DeclarativeGameWindow::tweenEngine(){ return m_tweenEngine; }

void DeclarativeGameWindow::classBegin() {
}

void DeclarativeGameWindow::componentComplete() {
    m_complete = true;
    m_storageService.setId(m_config.storageId());
    if (transientParent() && !transientParent()->isVisible()) {
        connect(transientParent(), &QQuickWindow::visibleChanged, this,
                &DeclarativeGameWindow::setWindowVisibility, Qt::QueuedConnection);
    } else {
        setWindowVisibility();
    }
}

void DeclarativeGameWindow::requestPaint(){
    if (system()->pauseOnHide()) system()->setPaused(isActive());
    if (!system()->paused()) update();
}

void DeclarativeGameWindow::setWindowVisibility(){
    if (m_visibility == AutomaticVisibility) {
        setVisible(m_visible);
    } else {
        setVisibility(m_visibility);
    }
}

DeclarativeSystem::Platform DeclarativeGameWindow::platform(){
#if defined(Q_OS_WIN) || defined(Q_OS_CYGWIN)
    return DeclarativeSystem::Windows;
#elif defined(Q_OS_MAC)
    return DeclarativeSystem::Mac;
#elif defined(Q_OS_IOS)
    return DeclarativeSystem::iOS;
#elif defined(Q_OS_ANDROID)
    return DeclarativeSystem::Android;
#elif defined(Q_OS_UNIX)
    return DeclarativeSystem::Linux;
#else
    return DeclarativeSystem::Unknown;
#endif
}
/*!
    \qmlmethod string GameWindow::addAsset(url source, string id = "")

    Preloads an asset into the engine given a source url path of
    the asset to be loaded. Optionally an id can be given to be
    used to locate a preloaded asset.

    The following image asset types are supported:
    \list
        \li BMP - Windows Bitmap image
        \li GIF - Graphic Interchange Format image
        \li JPG/JPEG - Joint Photographic Experts image
        \li PNG - Portable Network Graphics image
        \li PBM - Portable Bitmap image
        \li PGM - Portable Graymap image
        \li PPM - Portable Pixmap image
        \li XBM - X11 Bitmap image
        \li XPM - X11 Pixmap image
    \endlist

    Other supported asset types:
    \list
        \li atlas/json - SpriteSheet texture format (http://www.codeandweb.com/texturepacker)
        \li anim - Spine Animation format (http://esotericsoftware.com/)
        \li fnt/xml - Bitmap font format - (http://www.angelcode.com/products/bmfont/)
    \endlist
*/
QString DeclarativeGameWindow::addAsset(const QUrl &source, const QString &id){
    QString _id(id.length()>0 ? id : source.toString());

    if (!m_resources.contains(_id)){
        m_resources.insert(_id, source);
        return _id;
    }

    return QString();
}

/*!
    \qmlmethod string GameWindow::addSound(url source, string id = "")

    Loads a sound effect asset into the engine's audio service
    given a \c source url path to an audio file. An optional
    \c id can be given to be used to locate the sound asset.

    The following audio formats are supported:
    \list
        \li AIFF - Audio Interchange File Format
        \li ASF - Advanced Streaming format
        \li DLS - DownLoadable Sound format for midi playback.
        \li FLAC - Lossless compression codec
        \li FSB - FMOD sample bank format
        \li IT - Impulse tracker sequenced mod format
        \li MID - MIDI using operating system or custom DLS patches.
        \li MOD - Protracker / Fasttracker and others sequenced mod format
        \li MP2 - MPEG I/II Layer 2
        \li MP3 - MPEG I/II Layer 3, including VBR support
        \li OGG - Ogg Vorbis format
        \li RAW - Raw file format support.
        \li S3M - ScreamTracker 3 sequenced mod format
        \li VAG - PS2 / PSP format
        \li WAV - Microsoft Wave files
        \li WAX - playlist format
        \li WMA - Windows Media Audio format
        \li XM - FastTracker 2 sequenced format
    \endlist
*/
QString DeclarativeGameWindow::addSound(const QUrl &source, const QString &id){
    QUrl _source = config()->baseUrl().resolved(source);
    QString _id(id.length()>0 ? id : source.toString());

    if (m_audioService.loadSound(_source, _id)){
        return _id;
    }else{
        qmlInfo(this) << "Unable to load audio file " << _source.toLocalFile();
    }
    return QString();
}
/*!
    \qmlmethod string GameWindow::addMusic(url source, string id = "")

    Loads a music track asset into the engine's audio service
    given a \c source url path to an audio file. An optional
    \c id can be given to be used to locate the music track asset.

    The following audio formats are supported:
    \list
        \li AIFF - Audio Interchange File Format
        \li ASF - Advanced Streaming format
        \li ASX - playlist format
        \li DLS - DownLoadable Sound format for midi playback.
        \li FLAC - Lossless compression codec
        \li FSB - FMOD sample bank format
        \li IT - Impulse tracker sequenced mod format
        \li M3U - playlist format - contains links to other audio files.
        \li MID - MIDI using operating system or custom DLS patches.
        \li MOD - Protracker / Fasttracker and others sequenced mod format
        \li MP2 - MPEG I/II Layer 2
        \li MP3 - MPEG I/II Layer 3, including VBR support
        \li OGG - Ogg Vorbis format
        \li PLS - playlist format
        \li RAW - Raw file format support.
        \li S3M - ScreamTracker 3 sequenced mod format
        \li VAG - PS2 / PSP format
        \li WAV - Microsoft Wave files
        \li WAX - playlist format
        \li WMA - Windows Media Audio format
        \li XM - FastTracker 2 sequenced format
    \endlist
*/
QString DeclarativeGameWindow::addMusic(const QUrl &source, const QString &id){
    QUrl _source = config()->baseUrl().resolved(source);
    QString _id(id.length()>0 ? id : source.toString());

    if (m_audioService.loadMusic(_source, _id)){
        return _id;
    }else{
        qmlInfo(this) << "Unable to load audio file " << _source.toLocalFile();
    }
    return QString();
}


void DeclarativeGameWindow::start(QQmlComponent *scene, const qreal width, const qreal height, QQmlComponent *loaderComponent){
    setWidth(width);
    setHeight(height);
    setVisible(true);

    system()->init(width, height);

    QQmlComponent defaultLoader(qmlEngine(this), QUrl("qrc:/internal/DefaultLoader.qml"), system()->stage());

    DeclarativeLoader *loaderItem = 0;
    if (!loaderComponent){
        loaderComponent = &defaultLoader;
    }
    if (loaderComponent->isError()){
        qWarning() << "Unable to start engine: " << loaderComponent->errors();
        return;
    }

    QObject *loaderObject = loaderComponent->beginCreate(QQmlEngine::contextForObject(this));

    if (!loaderObject->inherits("DeclarativeLoader")){
        loaderItem->deleteLater();
        qmlInfo(loaderItem) << "start() loader component Must be of type \"Loader\"";
        return;
    }
    loaderItem = qobject_cast<DeclarativeLoader *>(loaderObject);
    loaderItem->setParent(system()->stage());
    loaderItem->setParentItem(system()->stage());
    loaderComponent->completeCreate();
    loaderItem->init(scene);
    loaderItem->start();

}

QMap<QString, QUrl> DeclarativeGameWindow::resources(){ return m_resources; }


DeclarativeGameWindow *DeclarativeGameWindow::instance(){
    return DeclarativeGameWindow::s_instance;
}
