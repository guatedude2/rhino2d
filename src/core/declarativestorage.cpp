#include "declarativestorage.h"

DeclarativeStorage::DeclarativeStorage(QObject *parent) :
    QObject(parent),
    m_settings(0)
{
}

DeclarativeStorage::DeclarativeStorage(QString id, QObject *parent) :
    QObject(parent),
    m_settings(0)
{
    setId(!id.isEmpty() ? id : "com.rhino2d.generic");
}

QString DeclarativeStorage::id() const{ return m_storageId; }
void DeclarativeStorage::setId(const QString id){
    if (id != m_storageId){
        m_storageId = id;
        QCoreApplication::setOrganizationDomain(m_storageId);
        #ifdef Q_OS_ANDROID
            QString s( qgetenv( "EXTERNAL_STORAGE" ) );
            QDir dir( s+ "/data/"+ m_storageId +"/" );
            m_settings = new QSettings(dir.absolutePath());
        #else
            m_settings = new QSettings();
        #endif
        emit idChanged();
    }
}

QVariant DeclarativeStorage::get(const QString id, const QVariant &defaultValue) const{
    return m_settings->value(id, defaultValue);
}

void DeclarativeStorage::remove(const QString id){
    m_settings->remove(id);
}

void DeclarativeStorage::reset(){
    m_settings->clear();
}

void DeclarativeStorage::set(const QString id, const QVariant &value){
    m_settings->setValue(id, value);
}
