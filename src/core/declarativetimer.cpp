#include "declarativetimer.h"
#include <limits>
#include <QDateTime>
#include <QDebug>

double DeclarativeTimer::s_last(0);
double DeclarativeTimer::s_time(0);
int DeclarativeTimer::s_speedFactor(1);
int DeclarativeTimer::s_maxStep(50);
bool DeclarativeTimer::s_seconds(false);

DeclarativeTimer::DeclarativeTimer(int ms, QObject *parent) :
    QObject(parent),
    m_repeat(false),
    m_target(0),
    m_base(0),
    m_last(0),
    m_pauseTime(0)
{
    set(ms);
}

DeclarativeTimer::DeclarativeTimer(QObject *parent) :
    QObject(parent),
    m_target(0),
    m_base(0),
    m_last(0),
    m_pauseTime(0)
{
}

void DeclarativeTimer::set(int ms){
    m_target = ms;
    reset();
}


void DeclarativeTimer::reset() {
    m_base = DeclarativeTimer::s_time;
    m_pauseTime = 0;
}

double DeclarativeTimer::delta(){
    double delta = DeclarativeTimer::s_time - m_last;
    m_last = DeclarativeTimer::s_time;
    return m_pauseTime ? 0 : delta;
}

double DeclarativeTimer::time() {
    double time = (m_pauseTime ? m_pauseTime : DeclarativeTimer::s_time) - m_base - m_target;
    if (DeclarativeTimer::s_seconds) time /= 1000.0f;
    return time;
}

void DeclarativeTimer::pause() {
    if(!m_pauseTime) m_pauseTime = DeclarativeTimer::s_time;
}

void DeclarativeTimer::resume() {
    if(m_pauseTime) {
        m_base += DeclarativeTimer::s_time - m_pauseTime;
        m_pauseTime = 0;
    }
}

bool DeclarativeTimer::repeat(){ return m_repeat; }
void DeclarativeTimer::setRepeat(bool repeat){
    if (repeat != m_repeat){
        m_repeat = repeat;
        emit repeatChanged();
    }
}

QJSValue DeclarativeTimer::callback() const{ return m_callback; }
void DeclarativeTimer::setCallback(const QJSValue &callback){
    m_callback = callback;
}

bool DeclarativeTimer::seconds(){ return DeclarativeTimer::s_seconds; }

void DeclarativeTimer::update(){
    double now = QDateTime::currentMSecsSinceEpoch();
    DeclarativeTimer::s_time += qMin((now - DeclarativeTimer::s_last), (double)DeclarativeTimer::s_maxStep) * DeclarativeTimer::s_speedFactor;
    DeclarativeTimer::s_last = now;
}
