#ifndef DECLARATIVESTAGE_H
#define DECLARATIVESTAGE_H

#include <QQuickItem>
#include <QQmlProperty>
#include <QSGSimpleRectNode>

class DeclarativeStage : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(bool interactive READ interactive WRITE setInteractive NOTIFY interactiveChanged)
public:
    explicit DeclarativeStage(QQuickItem *parent = 0);

    QColor backgroundColor();
    void setBackgroundColor(const QColor color);

    bool interactive();
    void setInteractive(bool interactive);
Q_SIGNALS:
    void backgroundColorChanged();
    void interactiveChanged();

protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData);

private:
    bool m_interactive;
    bool m_textureChanged;
    QColor m_backgroundColor;
};

#endif // DECLARATIVESTAGE_H
