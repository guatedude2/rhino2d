#ifndef DECLARATIVECONFIG_H
#define DECLARATIVECONFIG_H

#include <QUrl>
#include <QString>
#include <QObject>

class DeclarativeConfig : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl baseUrl READ baseUrl WRITE setCoreUrl NOTIFY baseUrlChanged)
    Q_PROPERTY(QString storageId READ storageId WRITE setStorageId NOTIFY storageIdChanged)
    Q_PROPERTY(QString analyticsId READ analyticsId WRITE setAnalyticsId NOTIFY analyticsIdChanged)

public:
    explicit DeclarativeConfig(QObject *parent = 0);

    QUrl baseUrl() const;
    void setCoreUrl(const QUrl baseUrl);
    QString storageId() const;
    void setStorageId(const QString storageId);
    QString analyticsId() const;
    void setAnalyticsId(const QString analyticsId);

signals:
    void baseUrlChanged();
    void storageIdChanged();
    void analyticsIdChanged();

private:
    QUrl m_baseUrl;
    QString m_storageId;
    QString m_analyticsId;
};

#endif // DECLARATIVECONFIG_H
