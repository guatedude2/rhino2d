#ifndef DECLARATIVESYSTEM_H
#define DECLARATIVESYSTEM_H

#include "declarativetimer.h"
#include "declarativestage.h"

#include <QObject>
#include <QJSValue>

class DeclarativeSystem : public QObject
{
    Q_OBJECT
    Q_ENUMS(Aspect Platform Button)
    Q_FLAGS(Buttons)
    Q_PROPERTY(Aspect aspect READ aspect WRITE setAspect NOTIFY aspectChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(qreal delta READ delta NOTIFY deltaChanged)
    Q_PROPERTY(bool paused READ paused WRITE setPaused NOTIFY pausedChanged)
    Q_PROPERTY(bool hires READ hires WRITE setHires NOTIFY hiresChanged) //TODO: RO?
    Q_PROPERTY(bool retina READ retina WRITE setRetina NOTIFY retinaChanged) //TODO: RO?
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
    Q_PROPERTY(Platform platform READ platform NOTIFY platformChanged)
    Q_PROPERTY(bool pauseOnHide READ pauseOnHide WRITE setPauseOnHide NOTIFY pauseOnHideChanged)
    Q_PROPERTY(qreal screenWidth READ screenWidth NOTIFY screenWidthChanged)
    Q_PROPERTY(qreal screenHeight READ screenHeight NOTIFY screenHeightChanged)

    //minWidth
    //maxWidth
    //minHeight
    //maxHeight

    //TODO: these properties change the GameWindow background
    //backgroundColor.game.
    //backgroundColor.rotate
    //backgroundImage.game
    //backgroundImage.rotate
public:
    explicit DeclarativeSystem(QObject *parent = 0);
    enum Aspect{ AspectFit, AspectCrop };
    enum Platform{ Unknown=0, Linux=1, Windows=2, Mac=3, iOS=10, Android=11 };
    enum Icon{
        NoIcon = 0,
        Information = 1,
        Warning = 2,
        Critical = 3,
        Question = 4
    };
    enum Button{
        Ok = 0x00000400,
        Open = 0x00002000,
        Save = 0x00000800,
        Cancel = 0x00400000,
        Close = 0x00200000,
        Discard = 0x00800000,
        Apply = 0x02000000,
        Reset = 0x04000000,
        RestoreDefaults = 0x08000000,
        Help = 0x01000000,
        SaveAll = 0x00001000,
        Yes = 0x00004000,
        YesToAll = 0x00008000,
        No = 0x00010000,
        NoToAll = 0x00020000,
        Abort = 0x00040000,
        Retry = 0x00080000,
        Ignore = 0x00100000,
        NoButton = 0x00000000
    };
    Q_DECLARE_FLAGS(Buttons, Button)

    Aspect aspect() const;
    void setAspect(const Aspect aspect);
    qreal width() const;
    void setWidth(const qreal width);
    qreal height() const;
    void setHeight(const qreal height);
    qreal delta();
    bool paused() const;
    void setPaused(const bool paused);
    bool hires() const;
    void setHires(const bool hires);
    bool retina() const;
    void setRetina(const bool retina);
    bool running() const;
    Platform platform() const;
    void setPlatform(const Platform platform);
    bool pauseOnHide() const;
    void setPauseOnHide(const bool paused);
    qreal screenWidth() const;
    qreal screenHeight() const;

    DeclarativeStage *stage();

    void init(qreal width, qreal height);
    Q_INVOKABLE bool vibrate(const qreal time);
    Q_INVOKABLE int messageBox(const QString title, const QString message, const Buttons buttons, const Button defaultButton=Ok, Icon icon=NoIcon);
    Q_INVOKABLE QString inputBox(const QString title, const QString message, const QString placeholder="", const QJSValue &callback=QJSValue());
    Q_INVOKABLE bool openUrl(const QUrl url);
    Q_INVOKABLE bool sendEmail(const QString to, const QString subject, const QString message);

    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void setScene(QQmlComponent *sceneComponent);

Q_SIGNALS:
    void aspectChanged();
    void widthChanged();
    void heightChanged();
    void deltaChanged();
    void pausedChanged();
    void hiresChanged();
    void retinaChanged();
    void runningChanged();
    void platformChanged();
    void pauseOnHideChanged();
    void screenWidthChanged();
    void screenHeightChanged();

private slots:
    void handleResize();
    void run();

private:
    Platform m_platform;
    DeclarativeStage *m_stage;
    DeclarativeTimer *m_timer;
    QQuickItem *m_scene;
    QQmlComponent *m_newSceneComponent;
    qreal m_width;
    qreal m_height;
    qreal m_aspect;
    qreal m_delta;
    bool m_paused;
    bool m_hires;
    bool m_retina;
    bool m_running;
    qreal m_hiresFactor;
    QMetaObject::Connection m_gameLoopId;
    bool m_resize;
    Aspect m_aspectFormat;
    bool m_pauseOnHide, m_pauseOnHideSet;

    void setSceneNow(QQmlComponent *sceneComponent);
    void startRunLoop();
    void stopRunLoop();
    void initResize();
    void checkOrientation();
};


class DeclarativeSystemBackgroundColor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor game READ game WRITE setGame NOTIFY gameChanged)
    Q_PROPERTY(QColor rotate READ rotate WRITE setRotate NOTIFY rotateChanged)
public:
    explicit DeclarativeSystemBackgroundColor(QObject *parent = 0) :
        QObject(parent), m_game(Qt::black), m_rotate(Qt::black) {}

    QColor game() const{ return m_game; }
    void setGame(const QColor color){ m_game = color; emit gameChanged(); }
    QColor rotate() const{ return m_rotate; }
    void setRotate(const QColor color){ m_rotate = color; emit rotateChanged(); }

Q_SIGNALS:
    void gameChanged();
    void rotateChanged();
protected:
    QColor m_game;
    QColor m_rotate;
};


class DeclarativeSystemBackgroundImage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl game READ game WRITE setGame NOTIFY gameChanged)
    Q_PROPERTY(QUrl rotate READ rotate WRITE setRotate NOTIFY rotateChanged)
public:
    explicit DeclarativeSystemBackgroundImage(QObject *parent = 0) :
        QObject(parent) {}

    QUrl game() const{ return m_game; }
    void setGame(const QUrl source){ m_game = source; emit gameChanged(); }
    QUrl rotate() const{ return m_rotate; }
    void setRotate(const QUrl source){ m_rotate = source; emit rotateChanged(); }

Q_SIGNALS:
    void gameChanged();
    void rotateChanged();
protected:
    QUrl m_game;
    QUrl m_rotate;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DeclarativeSystem::Buttons)

#endif // DECLARATIVESYSTEM_H
