#include "rhino2d_plugin.h"

#include "physics/declarativeworld.h"

#include "core/declarativegamewindow.h"
#include "core/declarativeloader.h"
#include "core/declarativetween.h"
#include "core/declarativetimer.h"
#include "core/declarativeconfig.h"
#include "core/declarativestorage.h"
#include "core/declarativekeyboard.h"

#include "drawable/declarativescene.h"
#include "drawable/declarativesprite.h"
#include "drawable/declarativetilingsprite.h"
#include "drawable/declarativemovieclip.h"
#include "drawable/declarativecontainer.h"
#include "drawable/declarativetext.h"
#include "drawable/declarativebitmaptext.h"
#include "drawable/declarativeemitter.h"

#include <QQmlEngine>

void Rhino2dPlugin::registerTypes(const char *uri)
{
    // @uri Rhino2d

    qmlRegisterType<DeclarativeGameWindow>(uri, 1, 0,"GameWindow");
    qmlRegisterType<DeclarativeScene>(uri, 1, 0,"GameScene");
    qmlRegisterType<DeclarativeLoader>(uri, 1, 0,"GameLoader");

    qmlRegisterType<CoreDisplayObject>(uri, 1, 0,"GameDisplayObject");
    qmlRegisterType<DeclarativeContainer>(uri, 1, 0, "GameContainer");
    qmlRegisterType<DeclarativeContainer>(uri, 1, 0, "GameClass");
    qmlRegisterType<CoreGraphics>(uri, 1, 0, "GameGraphics");
    qmlRegisterType<DeclarativeSprite>(uri, 1, 0,"GameSprite");
    qmlRegisterType<DeclarativeTilingSprite>(uri, 1, 0,"GameTilingSprite");
    qmlRegisterType<DeclarativeMovieClip>(uri, 1, 0, "GameMovieClip");
    qmlRegisterType<DeclarativeText>(uri, 1, 0, "GameText");
    qmlRegisterType<DeclarativeBitmapText>(uri, 1, 0, "GameBitmapText");
    qmlRegisterType<DeclarativeStyle>();
    qmlRegisterType<DeclarativeAudio>();
    qmlRegisterType<DeclarativeConfig>();
    qmlRegisterType<DeclarativeStorage>();
    qmlRegisterType<QDeclarativeTouchPoint>(uri, 1, 0, "GameTouchPoint");
    qmlRegisterUncreatableType<QDeclarativeMouseEvent>(uri, 1, 0, "GameMouseEvent", "GameMouseEvent component is uninstantiatable");
    qmlRegisterUncreatableType<QDeclarativeKeyEvent>(uri, 1, 0, "GameKeyEvent", "GameKeyEvent component is uninstantiatable");
    qmlRegisterUncreatableType<DeclarativeKeyboard>(uri, 1, 0, "Keyboard", "Keyboard component is uninstantiatable");

    qmlRegisterType<DeclarativeTween>(uri, 1, 0,"GameTween");
    qmlRegisterType<DeclarativeTimer>(uri, 1, 0,"GameTimer");

    qmlRegisterType<DeclarativeEmitter>(uri, 1, 0,"GameEmitter");
    qmlRegisterUncreatableType<DeclarativeParticle>(uri, 1, 0,"GameParticle", "GameParticle component is uninstantiatable");

    qmlRegisterUncreatableType<DeclarativeSystem>(uri, 1, 0, "System", "System component is uninstantiatable");
    qmlRegisterUncreatableType<DeclarativeStage>(uri, 1, 0, "Stage", "Stage component is uninstantiatable");

    qmlRegisterUncreatableType<DeclarativeWorld>(uri, 1, 0,"GameWorld", "GameWorld component is uninstantiatable");
    qmlRegisterType<DeclarativeStaticBody>(uri, 1, 0,"GameStaticBody");
    qmlRegisterType<DeclarativeBody>(uri, 1, 0,"GameBody");

    qmlRegisterType<DeclarativeRectangleShape>(uri, 1, 0, "RectangleShape");
    qmlRegisterType<DeclarativeCircleShape>(uri, 1, 0, "CircleShape");
    qmlRegisterType<DeclarativeEllipseShape>(uri, 1, 0, "EllipseShape");
    qmlRegisterType<DeclarativePolygonShape>(uri, 1, 0, "PolygonShape");
    qmlRegisterType<DeclarativePolygonPoint>(uri, 1, 0, "PolygonPoint");

}

void Rhino2dPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri)
    m_engineSet = true;
    m_engine = engine;
}

QQmlEngine *Rhino2dPlugin::engine(){ return (m_engineSet ? m_engine : 0); }


