import QtQuick 2.2
import Rhino2d 1.0
import "objects"

GameScene{
    id: scene
    property real cloudSpeedFactor: 0.2
    property QtObject anvil;
    backgroundColor: "#b2dcef"
    world.gravity.y: 65
    //world.debugDraw: true

    onMouseDown: {
        spawnGranade(event.x, event.y);
    }

    onKeyDown: {                
        if (event.key==Qt.Key_P){
            if (game.system.paused){
                game.system.resume()                
            }else{
                game.system.pause()
            }
        }else if (event.key==Qt.Key_Back){
            game.system.messageBox("", "Are you sure you want exit?", System.Yes | System.No, System.No, System.Question)
            //TODO: confirm quit
            Qt.quit()
        }
    }
    Component.onCompleted: {
        //create top girders
        createGirder(-15, 150);
        createGirder(scene.width+15, 150);

        //create middle girders
        createGirder(-75, 350);
        createGirder(scene.width+75, 350);

        //stack 2 boxes on the middle girder
        createBoxStack(2, 75, 365)
        createBoxStack(2, scene.width-225, 365)

        //create bottom girders
        createGirder(-50, 550);
        createGirder(scene.width+50, 550);

        //create two small box pyramids on the bottom girders
        createBoxPyramid(2, 150, 515)
        createBoxPyramid(2, scene.width-150, 515)

        //create a big pyramid in the center of the screen
        createBoxPyramid(5, scene.width/2, 775)

        //creates a pile of soccerballs
        spawnSoccerBall(200, 125)
        //spawnSoccerBall(scene.width-100, 150)
        spawnFootball(scene.width/2, 530)

        //creates an anvil
        spawnAnvil(scene.width-200, 125)
    }

    function createBoxPyramid(size, x, y){
        var boxSize = 50
        for(var v=0; v<=size; v++){
            for (var h=0; h<v; h++){
                spawnCrate(x+ h*boxSize-v*boxSize/2+boxSize/2, y-(size-v)*boxSize)
            }
        }
    }
    function createBoxStack(size, x, y){
        var boxSize = 50
        for(var v=0; v<size; v++){
            spawnCrate(x+ boxSize+boxSize/2, y-(size-v)*boxSize)
        }
    }

    function spawnCrate(x, y) {
        crateComponent.createObject(boxContainer, {x: x || Math.random()*scene.width, y: y || 0})
    }

    function spawnSoccerBall(x, y){
        soccerBallComponent.createObject(boxContainer, {x: x, y: y})
    }

    function spawnFootball(x, y){
        footballComponent.createObject(boxContainer, {x: x, y: y})
    }

    function spawnAnvil(x, y){
        anvil = anvilComponent.createObject(boxContainer, {x: x, y: y})
    }

    function spawnGranade(x, y) {
        var obj = granadeComponent.createObject(boxContainer, {x: x || Math.random()*scene.width, y: y || 0})
        obj.start();
    }

    function createExplosion(x, y) {
        var obj = explosionComponent.createObject(boxContainer, {x: x || Math.random()*scene.width, y: y || 0})
    }

    function createGirder(x, y){
        var obj = girderComponent.createObject(boxContainer, {x: x, y: y});
    }

    GameStaticBody{
        id: groundBody
        x: game.system.width / 2
        y: 850
        collisionGroup: 0
        shape: RectangleShape{
            width: game.system.width
            height: 100
        }
    }
    GameStaticBody{
        id: skyBody
        x: game.system.width / 2
        y: -50
        collisionGroup: 0
        shape: RectangleShape{
            width: game.system.width
            height: 100
        }
    }
    GameStaticBody{
        id: leftWallBody
        x: -50
        y: game.system.height / 2
        collisionGroup: 0
        shape: RectangleShape{
            width:  100
            height: game.system.height
        }
    }

    GameStaticBody{
        id: rightWallBody
        x: game.system.width+50
        y: game.system.height/2
        collisionGroup: 0
        shape: RectangleShape{
            width:  100
            height: game.system.height
        }
    }

    //background images
    GameSprite{ y: 400; texture: "media/parallax1.png"; }
    GameSprite{ y: 550; texture: "media/parallax2.png"; }
    GameSprite{ y: 650; texture: "media/parallax3.png"; }
    Cloud{ x: 100; y: 100; texture: 'media/cloud1.png'; speed: -50; }
    Cloud{ x: 300; y: 50; texture: 'media/cloud2.png'; speed: -30; }

    Cloud{ x: 650; y: 100; texture: 'media/cloud3.png'; speed: -50 }
    Cloud{ x: 700; y: 200; texture: 'media/cloud4.png'; speed: -40 }
    GameSprite{ y: 700; texture: "media/bushes.png"; }
    GameContainer{
        id: boxContainer

    }
    GameSprite{ y: 800; texture: "media/ground.png"; }

    Component{ id: crateComponent; Crate{} }
    Component{ id: soccerBallComponent; SoccerBall{} }
    Component{ id: footballComponent; Football{} }
    Component{ id: anvilComponent; Anvil{} }
    Component{ id: granadeComponent; Grenade{} }
    Component{ id: girderComponent; Girder{} }
    Component{ id: explosionComponent; Explosion{} }

}
