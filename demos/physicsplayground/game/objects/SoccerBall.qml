import Rhino2d 1.0

GameBody{
    id: ball
    rotation: Math.random()*360
    mass: 1
    bounciness: 0.75
    scale: 0.5
    shape: CircleShape{
        radius: 18
    }

    onCollided: {
        var vec = Qt.vector2d(linearVelocity.x, linearVelocity.y)
        game.audio.playSound("ballbounce", 0.5 - 10/vec.length(), false, 0.80 + Math.random()*0.4);
    }

    GameSprite{
        texture: "media/ball.png"
        anchors.centerIn: parent
    }
}
