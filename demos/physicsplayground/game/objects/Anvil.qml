import Rhino2d 1.0

GameBody{
    id: anvil
    mass: 3
    scale: 0.5
    shape: PolygonShape{
        points:[
            PolygonPoint{ x:-40; y:-16 },
            PolygonPoint{ x:-18; y:5 },
            PolygonPoint{ x:-13; y:18 },
            PolygonPoint{ x:13; y:18 },
            PolygonPoint{ x:18; y:5 },
            PolygonPoint{ x:40; y:-16 }
        ]

    }
    onCollided: {
        var vec = Qt.vector2d(linearVelocity.x, linearVelocity.y)
        game.audio.playSound("anvilhit", 0.5 - 50/vec.length(), false, 0.90 + Math.random()*0.3);
    }

    GameSprite{
        texture: "media/anvil.png"
        anchors.centerIn: parent
    }
}
