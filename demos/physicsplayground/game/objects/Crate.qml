import Rhino2d 1.0

GameBody{
    id: crateBody
    property var graphic:['media/crate1.png','media/crate2.png']
    //collideAgainst: 0
    //collisionGroup: 1
    mass: 1
    scale: 0.5
    bounciness: 0.2
    shape: RectangleShape{
        width: 100
        height: 100
    }

    onCollided: {
        var vec = Qt.vector2d(linearVelocity.x, linearVelocity.y)
        var rand = Math.floor(Math.random()*2+1)
        game.audio.playSound("woodhit"+rand, 0.5 - 10/vec.length(), false, 0.80 + Math.random()*0.4);
    }

    GameSprite{
        texture: graphic[Math.floor(Math.random()*2)]
        /*interactive: true
        onMouseDown: {
            event.accepted = true
        }
        onMouseMove: {
            if (event.wasHeld){
                crateBody.mass = 0
                crateBody.x = event.scenePos.x
                crateBody.y = event.scenePos.y
            }
        }

        onMouseUp: {
            crateBody.x = event.scenePos.x
            crateBody.y = event.scenePos.y
            crateBody.mass = 10
        }*/
    }
}
