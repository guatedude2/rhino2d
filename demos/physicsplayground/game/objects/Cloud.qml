import QtQuick 2.0
import Rhino2d 1.0

GameSprite{
    property real speed: 1
    onUpdate:{
        x += (speed * scene.cloudSpeedFactor * system.delta)
        if(x + width < 0) x = system.width;
    }
}
