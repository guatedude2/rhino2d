import Rhino2d 1.0

GameBody{
    id: grenade
    rotation: Math.random()*360
    angularVelocity: Math.random()*200-100
    mass: 1
    bounciness: 0.5
    scale: 0.5
    shape: CircleShape{
        radius: 13
    }

    property real lastVel:0
    onLinearVelocityChanged: {
        var vec = Qt.vector2d(linearVelocity.x, linearVelocity.y)
        if ((vec.length()-lastVel)> 100) explode()
        lastVel = vec.length()
    }

    function start(){
        //game.audio.playSound("ticktock",0.5, true, 0.90 + Math.random()*0.3);
        game.scene.addTimer(3000, explode, false)

    }

    function explode(){
        game.audio.stopSound("ticktock")
        addExplosionForce(1500, 200, Qt.point(0,0), GameBody.RaycastExplosion)
        scene.createExplosion(grenade.x, grenade.y)
        grenade.destroy()
        game.audio.playSound("explosion",0.8, false, 0.90 + Math.random()*0.3);
    }

    onCollided: {
        var vec = Qt.vector2d(linearVelocity.x, linearVelocity.y)
        game.audio.playSound("grenadehit", 0.5 - 10/vec.length(), false, 0.80 + Math.random()*0.4);
    }

    GameSprite{
        texture: "media/grenade.png"
        anchors.centerIn: parent
    }
}
