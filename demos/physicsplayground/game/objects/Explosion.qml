import Rhino2d 1.0


GameMovieClip{
    id: explosion
    animationSpeed: 0.25
    anchorPoint.x: 0.5
    anchorPoint.y: 0.5
    playing: true
    scale: 0.5
    loop: false
    textures: [
        "media/explosion1.png",
        "media/explosion2.png",
        "media/explosion3.png",
        "media/explosion4.png"
    ]
    onCompleted: {
        explosion.destroy()
    }

}
