
function loadAssets(game){
    // Sprites
    game.addAsset('media/cloud4.png');
    game.addAsset('media/cloud3.png');
    game.addAsset('media/cloud2.png');
    game.addAsset('media/cloud1.png');
    game.addAsset('media/ground.png');
    game.addAsset('media/bushes.png');
    game.addAsset('media/parallax3.png');
    game.addAsset('media/parallax2.png');
    game.addAsset('media/parallax1.png');

    game.addAsset('media/crate1.png');
    game.addAsset('media/crate2.png');
    game.addAsset('media/ball.png');
    game.addAsset('media/football.png');
    game.addAsset('media/grenade.png');
    game.addAsset('media/girder.png');
    game.addAsset('media/anvil.png');

    game.addAsset('media/explosion1.png');
    game.addAsset('media/explosion2.png');
    game.addAsset('media/explosion3.png');
    game.addAsset('media/explosion4.png');

    game.addSound("media/sounds/anvilhit.ogg", "anvilhit");
    game.addSound("media/sounds/ballbounce.ogg", "ballbounce");
    game.addSound("media/sounds/grenadehit.ogg", "grenadehit");
    game.addSound("media/sounds/explosion.ogg", "explosion");
    game.addSound("media/sounds/swish.ogg", "swish");
    game.addSound("media/sounds/ticktock.ogg", "ticktock");
    game.addSound("media/sounds/woodhit1.ogg", "woodhit1");
    game.addSound("media/sounds/woodhit2.ogg", "woodhit2");
    game.addSound("media/sounds/woodhit3.ogg", "woodhit3");

}
