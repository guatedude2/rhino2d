import QtQuick 2.2
import Rhino2d 1.0
import "objects"

GameScene{
    id: scene
    property bool ended: false
    property alias player: player
    property int gapTime: 1500
    property int gravity: 2000
    property int score: 0
    property real cloudSpeedFactor: 1
    property QtObject gameTimer
    backgroundColor: "#d0f4f7"
    world.gravity.y: 65
    //world.debugDraw: true

    onMouseDown: {
        if(ended) return;
        if(player.body.mass === 0) {
            logo.remove()
            player.body.mass = 1
            gameTimer = scene.addTimer(gapTime, spawnGap, true);
        }
        player.jump();
    }

    onSwipe: {
        console.log(direction, angle)
    }

    Connections{
        target: game.keyboard

    }

    onKeyDown: {                
        if (event.key==Qt.Key_P){
            if (game.system.paused){
                game.system.resume()                
            }else{
                game.system.pause()
            }
        }else if (event.key==Qt.Key_Back){
            game.system.messageBox("", "Are you sure you want exit?", System.Yes | System.No, System.No, System.Question)
            //TODO: confirm quit
            Qt.quit()
        }
        console.log(event.key, event.accepted)
    }
    Component.onCompleted: {
        game.audio.musicVolume = 0.2;
        game.audio.playMusic('music');
    }

    function spawnGap() {
        gapComponent.incubateObject(gapContainer)
    }
    function addScore() {
        score++;
        game.audio.playSound('score');
    }

    function showScore() {
        gameoverBox.visible = true

        var highScore = parseInt(game.storage.get('highScore')) || 0;
        if(score > highScore) game.storage.set('highScore', score);

        highScoreText.text = highScore

        if(score > 0) {
            var time = Math.min(100, (1 / score) * 1000);
            var scoreCounter = 0;
            addTimer(time, function() {
                scoreCounter++;
                scoreText.text = scoreCounter
                if(scoreCounter >= score) {
                    this.repeat = false;
                    if(score > highScore) {
                        game.audio.playSound('highscore');
                        newHighScore.visible = true
                    }
                    restartButton.visible = true
                }
            }, true);
        }else{
            restartButton.visible = true
        }
    }

    function gameOver() {
        var i;
        cloudSpeedFactor = 0.2;
        ended = true;
        scene.removeTimer(gameTimer, false)
        var objects = scene.children
        for (i = 0; i < objects.length; i++) {
            if(typeof(objects[i].speed)=="object") objects[i].speed.x = 0;
        }
        for (i = 0; i < world.bodies.length; i++) {
            if (!!world.bodies[i], world.bodies[i].linearVelocity)
                world.bodies[i].linearVelocity.x = 0
        }

        addTimer(500, showScore);

        game.audio.stopMusic();
        game.audio.playSound('explosion');
    }


    GameStaticBody{
        id: groundBody
        x: game.system.width / 2
        y: 850
        collisionGroup: 0
        shape: RectangleShape{
            width: game.system.width
            height: 100
        }
    }

    //paralax images
    GameTilingSprite{ y: 500; texture: "media/parallax1.png"; speed.x: -50; }
    GameTilingSprite{ y: 550; texture: "media/parallax2.png"; speed.x: -100; }
    GameTilingSprite{ y: 575; texture: "media/parallax3.png"; speed.x: -200; }
    Cloud{ x: 100; y: 100; texture: 'media/cloud1.png'; speed: -50; }
    Cloud{ x: 300; y: 50; texture: 'media/cloud2.png'; speed: -30; }

    Logo{ id: logo ; }

    Cloud{ x: 650; y: 100; texture: 'media/cloud3.png'; speed: -50 }
    Cloud{ x: 700; y: 200; texture: 'media/cloud4.png'; speed: -40 }
    GameTilingSprite{ y: 700; texture: "media/bushes.png"; speed.x: -250; }
    GameContainer{ id: gapContainer }
    GameTilingSprite{ y: 800; texture: "media/ground.png"; speed.x: -300; }

    Player{
        id: player;
    }

    GameSprite{
        id: gameoverBox
        visible: false
        texture: 'media/gameover.png'
        anchors.centerIn: parent
        GameBitmapText{
            id:scoreText
            style.font.family: "Pixel"
            x: 310
            y: 131
            text: "0"
        }
        GameBitmapText{
            id:highScoreText
            style.font.family: "Pixel"
            x: 310
            y: 195
            text: "0"
        }
        GameSprite{
            id: newHighScore
            y: 212
            x: 77
            visible: false
            texture: 'media/new.png'
        }

    }
    GameSprite{
        id: restartButton
        visible: false
        interactive: true
        texture: 'media/restart.png'
        anchors.centerIn: parent
        anchors.verticalCenterOffset: 250
        scale: 0
        onVisibleChanged: {
            console.log("CHECK VIs", visible)
            if (visible) restartButtonTween.start()
        }
        onMouseDown:{
            game.restartScene()
        }

        GameTween{
            id: restartButtonTween
            target: restartButton
            to: { "scale": 1 }
            duration: 200
            easing: GameTween.Easing.Back.Out
            repeat: 0
            yoyo: true
            running: false
        }
    }
    GameBitmapText{
        id: currentScoreText
        text: score
        style.font.family: "Pixel"
        x: game.system.width / 2 - scoreText.width / 2;
        y: 0
    }



//    GameSprite{
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: 48
//        texture: 'media/madewithpanda.png'
//    }

    Component{ id: gapComponent;
        Gap{
            world: scene.world
        }
    }


    //TODO: test tint and blending
//    GameSprite{
//        id: symbol
//        anchors.horizontalCenter: parent.horizontalCenter
//        rotation: -5.8
//        transformOrigin:"Bottom"
//        texture: "media/player1.png"
//        tint: "red"
//        blendMode: GameSprite.BlendModeColorBurn
//    }

}
