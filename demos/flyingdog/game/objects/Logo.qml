import QtQuick 2.0
import Rhino2d 1.0

GameClass {
    id: logoContainer
    anchors.horizontalCenter: parent.horizontalCenter;
    //position.y: -150

    function remove(){
        hideTween.start();
    }
    GameTween{
        id: hideTween
        target: logoContainer
        to: {"opacity": 0}
        duration: 1000
        repeat: 0
    }

    GameTween{
        target: logoContainer
        to: { "y": 200 }
        duration: 1500
        easing: GameTween.Easing.Back.Out
        running: true
    }

    GameSprite{
        id: logo1;
        anchors.horizontalCenter: parent.horizontalCenter;
        y: 0;
        texture: "media/logo1.png";
    }
    GameTween{
        target: logo1
        to: { "y": -20 }
        duration: 1000
        easing: GameTween.Easing.Quadratic.InOut
        repeat: -1
        yoyo: true
        running: true
    }

    GameSprite{
        id: logo2;
        anchors.horizontalCenter: parent.horizontalCenter;
        y: 80;
        texture: "media/logo2.png";
    }
    GameTween{
        target: logo2
        to: { "y": 100 }
        duration: 1000
        easing: GameTween.Easing.Quadratic.InOut
        repeat: -1
        yoyo: true
        running: true
    }
}
