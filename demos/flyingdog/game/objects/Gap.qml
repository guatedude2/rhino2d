import QtQuick 2.0
import Rhino2d 1.0

GameClass {
    id: gap
    property QtObject world
    property int groundTop: 800
    property int gapWidth: 132
    property int minY: 150
    property int maxY: 550
    property int gapHeight: 232
    property int speed: -300
    property int topHeight
    property int bottomHeight

    Component.onCompleted: {
        var y = Math.round(Math.randomize(minY, maxY));
        topHeight =  y - gapHeight / 2;
        bottomHeight = groundTop - topHeight - gapHeight;
    }

    GameBody{
        id: topBody
        y: topHeight / 2
        x: game.system.width + gap.gapWidth / 2
        linearVelocity.x: gap.speed
        collisionGroup: 0
        shape: RectangleShape{
            width: gap.gapWidth
            height: topHeight
        }

        GameSprite{
            id: topSprite
            texture:"media/bar.png"
            y:topBody.y+ topBody.height*0.5
            transform: Scale{
                yScale: -1
            }
        }
    }
    GameBody{
        id: goalBody
        x: topBody.x + width + scene.player.body.shape.width
        y: topHeight + gapHeight / 2
        collideAgainst: 1
        collisionGroup: 1
        sensor: true
        shape: RectangleShape{
            width: gap.gapWidth
            height: gap.gapHeight
        }
        onCollided: {
            enabled = false
            game.scene.addScore();
        }
    }

    GameBody{
        id: bottomBody
        y: topHeight + gapHeight + bottomHeight / 2
        x: topBody.x
        collisionGroup: 0
        shape: RectangleShape{
            width: gap.gapWidth
            height: bottomHeight
        }
        GameSprite{
            id: bottomSprite
            texture:"media/bar.png"
        }
    }

    onUpdate:{
        if(topBody.x + gap.gapWidth / 2 < 0) {
            gap.destroy()
        }
    }
}
