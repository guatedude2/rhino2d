import QtQuick 2.2
import Rhino2d 1.0

GameContainer{
    id: player
    property int jumpPower: -750//-550
    property alias body: body

    function jump() {
        if(body.y < 0 || game.scene.ended) return;
        body.linearVelocity.y  = jumpPower;
        game.audio.playSound('jump');
    }
    GameBody{
        id: body
        z: 100
        x: game.system.width / 2;
        y: 500;
        collideAgainst: 0
        collisionGroup: 1
        mass: 0
        fixedRotation: true
        shape: RectangleShape{
            width: 128+4-10
            height: 48-4-10
        }
        onCollided:{
            if(!game.scene.ended) {
                game.scene.gameOver();
                linearVelocity.y = -200;
                smokeEmitter.rate = 0;
            }
            linearVelocity.x = 0;
        }

        GameMovieClip{
            id: sprite
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 5
            animationSpeed: 0.1
            playing: true
            textures: [
                "media/player1.png",
                "media/player2.png"
            ]
        }
    }

    GameEmitter{
        id: smokeEmitter
        position.x: body.x - 60
        position.y: body.y - 10
        angle: Math.PI
        angleVar: 0.1
        endAlpha: 1
        life: 0.4
        lifeVar: 0.2
        count: 2
        speed: 400
        textures:[
            "media/particle.png"
        ]
    }


//    GameEmitter{
//        id: flyEmitter
//        position.x: body.x + 30
//        position.y: body.y - 30
//        life: 0
//        rate: 0
//        positionVar.x: 50
//        positionVar.y: 50
//        target.x: body.x + 30
//        target.y: body.y - 30
//        targetForce: 200
//        speed: 100
//        velocityLimit.x: 100
//        velocityLimit.y: 100
//        endAlpha: 1
//        textures:[
//            "media/particle2.png"
//        ]
//        onCreate: flyEmitter.emitParticles(5)
//    }
}
