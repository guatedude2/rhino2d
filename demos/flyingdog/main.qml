import QtQuick 2.2
import Rhino2d 1.0
import "game"
import "game/Assets.js" as Assets
import "config.js" as Config

GameWindow{
    id: game
    property alias sceneGame: sceneGame
    config.baseUrl: "."
    config.storageId: Config.storageId
    config.analyticsId: Config.analyticsId
    Component.onCompleted: {
        Assets.loadAssets(game)
        if (game.system.platform==System.iOS || game.system.platform==System.Android){
            game.system.aspect = System.AspectCrop
        }else{
            game.system.aspect = System.AspectFit
        }        
        game.start(sceneGame, 768, 1024)
    }

    function restartScene(){
        game.system.setScene(sceneGame);
    }

    Component{
        id: sceneGame;
        SceneGame{}
    }
}
